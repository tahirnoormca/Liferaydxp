/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.tahir.liferaypractice.service.persistence.impl;

import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;

import com.tahir.liferaypractice.model.TestServiceModule;
import com.tahir.liferaypractice.service.persistence.TestServiceModulePersistence;

import java.util.Set;

/**
 * @author Tahir
 * @generated
 */
public class TestServiceModuleFinderBaseImpl extends BasePersistenceImpl<TestServiceModule> {
	@Override
	public Set<String> getBadColumnNames() {
		return getTestServiceModulePersistence().getBadColumnNames();
	}

	/**
	 * Returns the test service module persistence.
	 *
	 * @return the test service module persistence
	 */
	public TestServiceModulePersistence getTestServiceModulePersistence() {
		return testServiceModulePersistence;
	}

	/**
	 * Sets the test service module persistence.
	 *
	 * @param testServiceModulePersistence the test service module persistence
	 */
	public void setTestServiceModulePersistence(
		TestServiceModulePersistence testServiceModulePersistence) {
		this.testServiceModulePersistence = testServiceModulePersistence;
	}

	@BeanReference(type = TestServiceModulePersistence.class)
	protected TestServiceModulePersistence testServiceModulePersistence;
}