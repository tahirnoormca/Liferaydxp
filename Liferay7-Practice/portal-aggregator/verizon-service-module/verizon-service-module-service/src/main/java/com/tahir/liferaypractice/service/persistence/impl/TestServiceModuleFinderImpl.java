package com.tahir.liferaypractice.service.persistence.impl;

import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.util.dao.orm.CustomSQLUtil;
import com.tahir.liferaypractice.model.TestServiceModule;
import com.tahir.liferaypractice.model.impl.TestServiceModuleImpl;

import java.util.List;

/**
 * Created by tana0616 on 2/23/2017.
 */
public class TestServiceModuleFinderImpl extends TestServiceModuleFinderBaseImpl  {
    public List<TestServiceModule> getTestServiceModuled() {
        Session session = null;
        try {
            session = openSession();
            String sql = CustomSQLUtil.get("TestSelectAllQuery");
            SQLQuery queryObject = session.createSQLQuery(sql);
            queryObject.setCacheable(false);
            queryObject.addEntity("Student", TestServiceModuleImpl.class);
            QueryPos qPos = QueryPos.getInstance(queryObject);
            //qPos.add(start);
           // qPos.add(end);
            return (List<TestServiceModule>)queryObject.list();
        //      return (List<Student>) QueryUtil.list(queryObject,getDialect(),start, end);
        // for pagination feature
            } catch (Exception e) {
                e.printStackTrace();
            }
            finally {
                closeSession(session);
            }
            return null;
    }


}


