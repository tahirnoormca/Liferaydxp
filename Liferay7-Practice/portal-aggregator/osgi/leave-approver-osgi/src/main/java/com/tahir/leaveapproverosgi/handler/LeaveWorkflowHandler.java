package com.tahir.leaveapproverosgi.handler;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.workflow.BaseWorkflowHandler;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.kernel.workflow.WorkflowHandler;
import com.tahir.leaveapproverdb.model.Leave;
import com.tahir.leaveapproverdb.service.LeaveLocalService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import java.io.Serializable;
import java.util.Locale;
import java.util.Map;

/**
 * Created by tana0616 on 7/17/2017.
 */
@Component(
        property = {"model.class.name=com.tahir.leaveapproverdb.model.Leave"},
        service = WorkflowHandler.class
)
public class LeaveWorkflowHandler extends BaseWorkflowHandler<Leave> {

   private LeaveLocalService leaveService;
    @Reference(unbind = "-")
    protected void setLeaveService(LeaveLocalService leaveService) {
        this.leaveService = leaveService;
    }

    @Override
    public String getClassName() {
        return Leave.class.getName();
    }
    @Override
    public String getType(Locale locale) {
        return "leave";
    }

    @Override
    public Leave updateStatus(int status, Map<String, Serializable> workflowContext) throws PortalException {

        long userId = GetterUtil.getLong((String)workflowContext.get(WorkflowConstants.CONTEXT_USER_ID));
        long leaveId = GetterUtil.getLong((String)workflowContext.get(WorkflowConstants.CONTEXT_ENTRY_CLASS_PK));
        ServiceContext serviceContext = (ServiceContext)workflowContext.get("serviceContext");
        Leave leave =leaveService.updateStatus(userId, leaveId, status, serviceContext);
        return leave;
    }

}
