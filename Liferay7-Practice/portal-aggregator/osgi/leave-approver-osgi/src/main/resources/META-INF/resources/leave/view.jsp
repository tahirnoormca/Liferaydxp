
<%@ page import="com.tahir.leaveapproverdb.service.LeaveLocalServiceUtil" %>
<%@ page import="com.liferay.portal.kernel.language.LanguageUtil" %>
<%@ page import="javax.portlet.PortletURL" %>
<%@ include file="init.jsp" %>
<%--<liferay-portlet:renderURL portletConfiguration="<%= true %>" varImpl="leaveItrUrl" />--%>

<portlet:renderURL var="addLeaveJSP">
    <portlet:param name="mvcPath" value="/leave/addLeave.jsp"/>
</portlet:renderURL>
<liferay-ui:user-display
        markupView="lexicon"
        showUserDetails="false"
        showUserName="true"
        userId="<%= themeDisplay.getRealUserId() %>"
        userName="<%= themeDisplay.getRealUser().getFullName() %>"
/>
<%
    PortletURL leaveItrUrl = renderResponse.createRenderURL();
%>


<div class="container-fluid-1280 main-content-body">
    <liferay-frontend:add-menu >
        <liferay-frontend:add-menu-item title='<%= LanguageUtil.get(request,
            "add-leave") %>' url="<%=addLeaveJSP%>" />
    </liferay-frontend:add-menu>

   <liferay-ui:search-container emptyResultsMessage="no-leaves-found" iteratorURL="<%=leaveItrUrl %>" >
        <liferay-ui:search-container-results results="<%= LeaveLocalServiceUtil.getLeaves(searchContainer.getStart(),
                        searchContainer.getEnd()) %>" >
        </liferay-ui:search-container-results>

        <liferay-ui:search-container-row className="com.tahir.leaveapproverdb.model.Leave" modelVar="leave" keyProperty="leaveId" >
            <portlet:renderURL var="rowURL">
                <portlet:param name="backURL" value="<%=currentURL %>" />
                <portlet:param name="leaveId" value="${leave.leaveId}" />
                <portlet:param name="mvcPath" value="/leave/leaveInfo.jsp"/>
            </portlet:renderURL>
            <liferay-ui:search-container-column-user userId="${leave.userId}" showDetails="false" name="User" />
            <liferay-ui:search-container-column-text property="userName" name="User Name" href="${rowURL}"/>
            <liferay-ui:search-container-column-text property="leaveName" name="Leave Name" href="${rowURL}"/>
            <liferay-ui:search-container-column-date property="startDate" name="Start Date"/>
            <liferay-ui:search-container-column-date property="endDate" name="End Date"/>
            <liferay-ui:search-container-column-status property="status" name="Status" />
        </liferay-ui:search-container-row>
        <liferay-ui:search-iterator />
    </liferay-ui:search-container>



</div>