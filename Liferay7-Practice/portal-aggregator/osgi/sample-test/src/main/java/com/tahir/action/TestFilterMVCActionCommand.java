package com.tahir.action;

import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import org.osgi.service.component.annotations.Component;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

/**
 * Created by tana0616 on 1/25/2017.
 */

@Component(
        property = {
                "javax.portlet.name=OSGITestPortlet",
                "mvc.command.name=testfilterAction"
        },
        service = MVCActionCommand.class
)
public class TestFilterMVCActionCommand extends BaseMVCActionCommand {

    @Override
    protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
        System.out.println("TestFilterMVCActionCommand");

    }
}
