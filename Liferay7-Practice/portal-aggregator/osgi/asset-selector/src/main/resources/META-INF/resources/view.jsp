<%@ page import="com.liferay.portal.kernel.util.GetterUtil" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ include file="init.jsp" %>
<portlet:resourceURL var="resourceURL" id="/selected/imageUrl"/>
<aui:button name="chooseImage" value="Choose"/>

<%

    List url=(List) request.getAttribute("itemSelectorURL");String itemSelectorURL=url.get(0).toString();
%>
<a hre="<%=itemSelectorURL.toString()%>">DOC</a>
<aui:script use="liferay-item-selector-dialog">

    $('#<portlet:namespace/>chooseImage').on('click',function(event) {
            var itemSelectorDialog = new A.LiferayItemSelectorDialog({
                    eventName: 'sampleTestSelectItem',
                        on: {
                                selectedItemChange: function(event) {
                                var selectedItem = event.newVal;
                                        if (selectedItem) {
                                        console.log("Object : "+JSON.stringify(selectedItem));
                                        var itemValue = JSON.parse(JSON.stringify(selectedItem));
                                        console.log(itemValue.value);
                                        itemSrc = itemValue.value;

    AUI().use('aui-io-request', function(A){
                                    A.io.request('<%=resourceURL.toString()%>', {
                                    method: 'post',
                                    data: {
                                    selectionReturnType:itemValue.returnType,
                                    selectionUrl: itemValue.value
                                    },
                                    on: {
                                    success: function() {
                                    console.log(this.get('responseData'));
                                    }
                                    }
                                    });

                                    });
    }
                                }
                            },
                        title: '<liferay-ui:message key="select-image"/>',
                        url: '<%= itemSelectorURL.toString() %>'
            });
            itemSelectorDialog.open();
    });
</aui:script>
cust-workflow