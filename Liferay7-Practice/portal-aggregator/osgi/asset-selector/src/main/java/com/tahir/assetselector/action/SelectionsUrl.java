package com.tahir.assetselector.action;

/**
 * Created by tana0616 on 7/14/2017.
 */

import java.io.IOException;
import java.io.PrintWriter;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import com.liferay.portal.kernel.util.ParamUtil;
import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;
@Component(
        property = {
                "javax.portlet.name=AssetSelectorPortlet",
                "mvc.command.name=/selected/imageUrl"
        },
        service = MVCResourceCommand.class
)
public class SelectionsUrl  implements MVCResourceCommand {
    private static Log _log = LogFactoryUtil.getLog(SelectionsUrl.class.getName());

    @Override
    public boolean serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
            throws PortletException {
        _log.info("Ajax");
        resourceResponse.setContentType("text/html");
        PrintWriter out;
        try {
            System.out.println("Return Type  : "+ParamUtil.getString(resourceRequest,"selectionReturnType"));
            System.out.println("URL  : "+ParamUtil.getString(resourceRequest,"selectionUrl"));
            out = resourceResponse.getWriter();
            out.println("AUI Ajax call is performed:--> "+ParamUtil.getString(resourceRequest,"selectionUrl"));
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }
}



