package com.tahir.assetselector.portlet;

import com.liferay.item.selector.ItemSelector;
import com.liferay.item.selector.ItemSelectorReturnType;
import com.liferay.item.selector.criteria.Base64ItemSelectorReturnType;
import com.liferay.item.selector.criteria.URLItemSelectorReturnType;
import com.liferay.item.selector.criteria.image.criterion.ImageItemSelectorCriterion;
import com.liferay.item.selector.criteria.url.criterion.URLItemSelectorCriterion;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactory;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import javax.portlet.*;
import javax.servlet.http.HttpServletRequest;

import com.liferay.portal.kernel.util.PortalUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component(
        immediate = true,
        property = {
                "javax.portlet.name=AssetSelectorPortlet",
                "com.liferay.portlet.display-category=category.sample",
                "com.liferay.portlet.instanceable=true",
                "javax.portlet.display-name=asset-selector Portlet",
                "javax.portlet.init-param.template-path=/",
                "javax.portlet.init-param.view-template=/view.jsp",
                "javax.portlet.resource-bundle=content.Language",
                "javax.portlet.security-role-ref=power-user,user",
                "com.liferay.portlet.requires-namespaced-parameters=false"
      },
        service = Portlet.class



)
public class AssetSelectorPortlet extends MVCPortlet {
    private ItemSelector _itemSelector;

    @Override
    public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
        List<ItemSelectorReturnType> itemSelectorReturnTypes = new ArrayList();
        itemSelectorReturnTypes.add(new URLItemSelectorReturnType());

       /* List<ItemSelectorReturnType> base64itemSelectorReturnTypes = new ArrayList();
        base64itemSelectorReturnTypes.add(new Base64ItemSelectorReturnType());*/

        //URLItemSelectorCriterion urlItemSelectorCriterion = new URLItemSelectorCriterion();

        ImageItemSelectorCriterion imageItemSelectorCriterion = new ImageItemSelectorCriterion();

        imageItemSelectorCriterion.setDesiredItemSelectorReturnTypes(itemSelectorReturnTypes);

        //imageItemSelectorCriterion.setDesiredItemSelectorReturnTypes(base64itemSelectorReturnTypes);

        RequestBackedPortletURLFactory requestBackedPortletURLFactory = RequestBackedPortletURLFactoryUtil.create(renderRequest);
        PortletURL itemSelectorURL = _itemSelector.getItemSelectorURL(requestBackedPortletURLFactory, "sampleTestSelectItem",imageItemSelectorCriterion);

        System.out.println("==============================================");
        System.out.println("itemSelectorURL : "+itemSelectorURL);
        System.out.println("==============================================");
        /*HttpServletRequest req = PortalUtil.getHttpServletRequest(renderRequest); req.setAttribute("itemSelectorURL",itemSelectorURL);*/

       renderRequest.setAttribute("itemSelectorURL", Arrays.asList(itemSelectorURL));
        super.doView(renderRequest, renderResponse);
    }

    @Reference(unbind = "-")
    public void setItemSelector(ItemSelector itemSelector) {
        _itemSelector = itemSelector;
    }
}