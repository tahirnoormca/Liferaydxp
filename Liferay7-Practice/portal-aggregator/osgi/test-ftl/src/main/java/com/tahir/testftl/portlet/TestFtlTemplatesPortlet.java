package com.tahir.testftl.portlet;


import com.liferay.util.bridges.freemarker.FreeMarkerPortlet;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

//import com.liferay.util.portlet.PortletProps;
import com.liferay.util.portlet.PortletProps;
import org.osgi.service.component.annotations.Component;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=test-ftl Portlet",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.ftl",
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class TestFtlTemplatesPortlet extends FreeMarkerPortlet {
	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		//PropsUtil.get("");
		/*try {
			String prop = PortletProps.get("tahir.article.id");
			System.out.println(prop);
		}catch (Exception e){
			System.out.println(e.getMessage());
		}*/
		readPropertiesFile();
		super.doView(renderRequest, renderResponse);
	}

	private void readPropertiesFile() {
		InputStream inputStream = null;
		try {
			Properties prop = new Properties();
			String propFileName = "portlet.properties";
			inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
			if (inputStream != null) {
				prop.load(inputStream);
				System.out.println(">>>>>>>>>>>>>>"+prop.getProperty("tahir.article.id"));

			} else {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}
		} catch (Exception e) {
			System.out.println("Exception: " + e);
		}

	}

}