/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.tahir.customworkflowbe.service.persistence.test;

import com.liferay.arquillian.extension.junit.bridge.junit.Arquillian;

import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.test.ReflectionTestUtil;
import com.liferay.portal.kernel.test.rule.AggregateTestRule;
import com.liferay.portal.kernel.test.rule.TransactionalTestRule;
import com.liferay.portal.kernel.test.util.RandomTestUtil;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.util.IntegerWrapper;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.OrderByComparatorFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Time;
import com.liferay.portal.test.rule.LiferayIntegrationTestRule;
import com.liferay.portal.test.rule.PersistenceTestRule;

import com.tahir.customworkflowbe.exception.NoSuchFeedbackException;
import com.tahir.customworkflowbe.model.Feedback;
import com.tahir.customworkflowbe.service.FeedbackLocalServiceUtil;
import com.tahir.customworkflowbe.service.persistence.FeedbackPersistence;
import com.tahir.customworkflowbe.service.persistence.FeedbackUtil;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

import org.junit.runner.RunWith;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * @generated
 */
@RunWith(Arquillian.class)
public class FeedbackPersistenceTest {
	@ClassRule
	@Rule
	public static final AggregateTestRule aggregateTestRule = new AggregateTestRule(new LiferayIntegrationTestRule(),
			PersistenceTestRule.INSTANCE,
			new TransactionalTestRule(Propagation.REQUIRED));

	@Before
	public void setUp() {
		_persistence = FeedbackUtil.getPersistence();

		Class<?> clazz = _persistence.getClass();

		_dynamicQueryClassLoader = clazz.getClassLoader();
	}

	@After
	public void tearDown() throws Exception {
		Iterator<Feedback> iterator = _feedbacks.iterator();

		while (iterator.hasNext()) {
			_persistence.remove(iterator.next());

			iterator.remove();
		}
	}

	@Test
	public void testCreate() throws Exception {
		long pk = RandomTestUtil.nextLong();

		Feedback feedback = _persistence.create(pk);

		Assert.assertNotNull(feedback);

		Assert.assertEquals(feedback.getPrimaryKey(), pk);
	}

	@Test
	public void testRemove() throws Exception {
		Feedback newFeedback = addFeedback();

		_persistence.remove(newFeedback);

		Feedback existingFeedback = _persistence.fetchByPrimaryKey(newFeedback.getPrimaryKey());

		Assert.assertNull(existingFeedback);
	}

	@Test
	public void testUpdateNew() throws Exception {
		addFeedback();
	}

	@Test
	public void testUpdateExisting() throws Exception {
		long pk = RandomTestUtil.nextLong();

		Feedback newFeedback = _persistence.create(pk);

		newFeedback.setUuid(RandomTestUtil.randomString());

		newFeedback.setFeedbackDate(RandomTestUtil.nextDate());

		newFeedback.setFeedbackText(RandomTestUtil.randomString());

		newFeedback.setFeedbackSubject(RandomTestUtil.randomString());

		newFeedback.setFeedBackStatus(RandomTestUtil.nextInt());

		newFeedback.setStatusByUserId(RandomTestUtil.nextLong());

		newFeedback.setStatusDate(RandomTestUtil.nextDate());

		newFeedback.setCompanyId(RandomTestUtil.nextLong());

		newFeedback.setGroupId(RandomTestUtil.nextLong());

		newFeedback.setUserId(RandomTestUtil.nextLong());

		_feedbacks.add(_persistence.update(newFeedback));

		Feedback existingFeedback = _persistence.findByPrimaryKey(newFeedback.getPrimaryKey());

		Assert.assertEquals(existingFeedback.getUuid(), newFeedback.getUuid());
		Assert.assertEquals(existingFeedback.getFeedbackId(),
			newFeedback.getFeedbackId());
		Assert.assertEquals(Time.getShortTimestamp(
				existingFeedback.getFeedbackDate()),
			Time.getShortTimestamp(newFeedback.getFeedbackDate()));
		Assert.assertEquals(existingFeedback.getFeedbackText(),
			newFeedback.getFeedbackText());
		Assert.assertEquals(existingFeedback.getFeedbackSubject(),
			newFeedback.getFeedbackSubject());
		Assert.assertEquals(existingFeedback.getFeedBackStatus(),
			newFeedback.getFeedBackStatus());
		Assert.assertEquals(existingFeedback.getStatusByUserId(),
			newFeedback.getStatusByUserId());
		Assert.assertEquals(Time.getShortTimestamp(
				existingFeedback.getStatusDate()),
			Time.getShortTimestamp(newFeedback.getStatusDate()));
		Assert.assertEquals(existingFeedback.getCompanyId(),
			newFeedback.getCompanyId());
		Assert.assertEquals(existingFeedback.getGroupId(),
			newFeedback.getGroupId());
		Assert.assertEquals(existingFeedback.getUserId(),
			newFeedback.getUserId());
	}

	@Test
	public void testCountByUuid() throws Exception {
		_persistence.countByUuid(StringPool.BLANK);

		_persistence.countByUuid(StringPool.NULL);

		_persistence.countByUuid((String)null);
	}

	@Test
	public void testCountByUUID_G() throws Exception {
		_persistence.countByUUID_G(StringPool.BLANK, RandomTestUtil.nextLong());

		_persistence.countByUUID_G(StringPool.NULL, 0L);

		_persistence.countByUUID_G((String)null, 0L);
	}

	@Test
	public void testCountByUuid_C() throws Exception {
		_persistence.countByUuid_C(StringPool.BLANK, RandomTestUtil.nextLong());

		_persistence.countByUuid_C(StringPool.NULL, 0L);

		_persistence.countByUuid_C((String)null, 0L);
	}

	@Test
	public void testCountByGroupId() throws Exception {
		_persistence.countByGroupId(RandomTestUtil.nextLong());

		_persistence.countByGroupId(0L);
	}

	@Test
	public void testCountByCompanyId() throws Exception {
		_persistence.countByCompanyId(RandomTestUtil.nextLong());

		_persistence.countByCompanyId(0L);
	}

	@Test
	public void testCountByfeedbackText() throws Exception {
		_persistence.countByfeedbackText(StringPool.BLANK);

		_persistence.countByfeedbackText(StringPool.NULL);

		_persistence.countByfeedbackText((String)null);
	}

	@Test
	public void testCountByG_S() throws Exception {
		_persistence.countByG_S(RandomTestUtil.nextLong(),
			RandomTestUtil.nextInt());

		_persistence.countByG_S(0L, 0);
	}

	@Test
	public void testFindByPrimaryKeyExisting() throws Exception {
		Feedback newFeedback = addFeedback();

		Feedback existingFeedback = _persistence.findByPrimaryKey(newFeedback.getPrimaryKey());

		Assert.assertEquals(existingFeedback, newFeedback);
	}

	@Test(expected = NoSuchFeedbackException.class)
	public void testFindByPrimaryKeyMissing() throws Exception {
		long pk = RandomTestUtil.nextLong();

		_persistence.findByPrimaryKey(pk);
	}

	@Test
	public void testFindAll() throws Exception {
		_persistence.findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			getOrderByComparator());
	}

	protected OrderByComparator<Feedback> getOrderByComparator() {
		return OrderByComparatorFactoryUtil.create("custwf_Feedback", "uuid",
			true, "feedbackId", true, "feedbackDate", true, "feedbackText",
			true, "feedbackSubject", true, "feedBackStatus", true,
			"statusByUserId", true, "statusDate", true, "companyId", true,
			"groupId", true, "userId", true);
	}

	@Test
	public void testFetchByPrimaryKeyExisting() throws Exception {
		Feedback newFeedback = addFeedback();

		Feedback existingFeedback = _persistence.fetchByPrimaryKey(newFeedback.getPrimaryKey());

		Assert.assertEquals(existingFeedback, newFeedback);
	}

	@Test
	public void testFetchByPrimaryKeyMissing() throws Exception {
		long pk = RandomTestUtil.nextLong();

		Feedback missingFeedback = _persistence.fetchByPrimaryKey(pk);

		Assert.assertNull(missingFeedback);
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereAllPrimaryKeysExist()
		throws Exception {
		Feedback newFeedback1 = addFeedback();
		Feedback newFeedback2 = addFeedback();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newFeedback1.getPrimaryKey());
		primaryKeys.add(newFeedback2.getPrimaryKey());

		Map<Serializable, Feedback> feedbacks = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(2, feedbacks.size());
		Assert.assertEquals(newFeedback1,
			feedbacks.get(newFeedback1.getPrimaryKey()));
		Assert.assertEquals(newFeedback2,
			feedbacks.get(newFeedback2.getPrimaryKey()));
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereNoPrimaryKeysExist()
		throws Exception {
		long pk1 = RandomTestUtil.nextLong();

		long pk2 = RandomTestUtil.nextLong();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(pk1);
		primaryKeys.add(pk2);

		Map<Serializable, Feedback> feedbacks = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertTrue(feedbacks.isEmpty());
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereSomePrimaryKeysExist()
		throws Exception {
		Feedback newFeedback = addFeedback();

		long pk = RandomTestUtil.nextLong();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newFeedback.getPrimaryKey());
		primaryKeys.add(pk);

		Map<Serializable, Feedback> feedbacks = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(1, feedbacks.size());
		Assert.assertEquals(newFeedback,
			feedbacks.get(newFeedback.getPrimaryKey()));
	}

	@Test
	public void testFetchByPrimaryKeysWithNoPrimaryKeys()
		throws Exception {
		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		Map<Serializable, Feedback> feedbacks = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertTrue(feedbacks.isEmpty());
	}

	@Test
	public void testFetchByPrimaryKeysWithOnePrimaryKey()
		throws Exception {
		Feedback newFeedback = addFeedback();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newFeedback.getPrimaryKey());

		Map<Serializable, Feedback> feedbacks = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(1, feedbacks.size());
		Assert.assertEquals(newFeedback,
			feedbacks.get(newFeedback.getPrimaryKey()));
	}

	@Test
	public void testActionableDynamicQuery() throws Exception {
		final IntegerWrapper count = new IntegerWrapper();

		ActionableDynamicQuery actionableDynamicQuery = FeedbackLocalServiceUtil.getActionableDynamicQuery();

		actionableDynamicQuery.setPerformActionMethod(new ActionableDynamicQuery.PerformActionMethod<Feedback>() {
				@Override
				public void performAction(Feedback feedback) {
					Assert.assertNotNull(feedback);

					count.increment();
				}
			});

		actionableDynamicQuery.performActions();

		Assert.assertEquals(count.getValue(), _persistence.countAll());
	}

	@Test
	public void testDynamicQueryByPrimaryKeyExisting()
		throws Exception {
		Feedback newFeedback = addFeedback();

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Feedback.class,
				_dynamicQueryClassLoader);

		dynamicQuery.add(RestrictionsFactoryUtil.eq("feedbackId",
				newFeedback.getFeedbackId()));

		List<Feedback> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(1, result.size());

		Feedback existingFeedback = result.get(0);

		Assert.assertEquals(existingFeedback, newFeedback);
	}

	@Test
	public void testDynamicQueryByPrimaryKeyMissing() throws Exception {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Feedback.class,
				_dynamicQueryClassLoader);

		dynamicQuery.add(RestrictionsFactoryUtil.eq("feedbackId",
				RandomTestUtil.nextLong()));

		List<Feedback> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(0, result.size());
	}

	@Test
	public void testDynamicQueryByProjectionExisting()
		throws Exception {
		Feedback newFeedback = addFeedback();

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Feedback.class,
				_dynamicQueryClassLoader);

		dynamicQuery.setProjection(ProjectionFactoryUtil.property("feedbackId"));

		Object newFeedbackId = newFeedback.getFeedbackId();

		dynamicQuery.add(RestrictionsFactoryUtil.in("feedbackId",
				new Object[] { newFeedbackId }));

		List<Object> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(1, result.size());

		Object existingFeedbackId = result.get(0);

		Assert.assertEquals(existingFeedbackId, newFeedbackId);
	}

	@Test
	public void testDynamicQueryByProjectionMissing() throws Exception {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Feedback.class,
				_dynamicQueryClassLoader);

		dynamicQuery.setProjection(ProjectionFactoryUtil.property("feedbackId"));

		dynamicQuery.add(RestrictionsFactoryUtil.in("feedbackId",
				new Object[] { RandomTestUtil.nextLong() }));

		List<Object> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(0, result.size());
	}

	@Test
	public void testResetOriginalValues() throws Exception {
		Feedback newFeedback = addFeedback();

		_persistence.clearCache();

		Feedback existingFeedback = _persistence.findByPrimaryKey(newFeedback.getPrimaryKey());

		Assert.assertTrue(Objects.equals(existingFeedback.getUuid(),
				ReflectionTestUtil.invoke(existingFeedback, "getOriginalUuid",
					new Class<?>[0])));
		Assert.assertEquals(Long.valueOf(existingFeedback.getGroupId()),
			ReflectionTestUtil.<Long>invoke(existingFeedback,
				"getOriginalGroupId", new Class<?>[0]));
	}

	protected Feedback addFeedback() throws Exception {
		long pk = RandomTestUtil.nextLong();

		Feedback feedback = _persistence.create(pk);

		feedback.setUuid(RandomTestUtil.randomString());

		feedback.setFeedbackDate(RandomTestUtil.nextDate());

		feedback.setFeedbackText(RandomTestUtil.randomString());

		feedback.setFeedbackSubject(RandomTestUtil.randomString());

		feedback.setFeedBackStatus(RandomTestUtil.nextInt());

		feedback.setStatusByUserId(RandomTestUtil.nextLong());

		feedback.setStatusDate(RandomTestUtil.nextDate());

		feedback.setCompanyId(RandomTestUtil.nextLong());

		feedback.setGroupId(RandomTestUtil.nextLong());

		feedback.setUserId(RandomTestUtil.nextLong());

		_feedbacks.add(_persistence.update(feedback));

		return feedback;
	}

	private List<Feedback> _feedbacks = new ArrayList<Feedback>();
	private FeedbackPersistence _persistence;
	private ClassLoader _dynamicQueryClassLoader;
}