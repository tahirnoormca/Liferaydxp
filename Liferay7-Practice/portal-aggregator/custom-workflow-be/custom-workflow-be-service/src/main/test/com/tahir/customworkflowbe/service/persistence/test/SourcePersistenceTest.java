/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.tahir.customworkflowbe.service.persistence.test;

import com.liferay.arquillian.extension.junit.bridge.junit.Arquillian;

import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.test.rule.AggregateTestRule;
import com.liferay.portal.kernel.test.rule.TransactionalTestRule;
import com.liferay.portal.kernel.test.util.RandomTestUtil;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.util.IntegerWrapper;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.OrderByComparatorFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Time;
import com.liferay.portal.test.rule.LiferayIntegrationTestRule;
import com.liferay.portal.test.rule.PersistenceTestRule;

import com.tahir.customworkflowbe.exception.NoSuchSourceException;
import com.tahir.customworkflowbe.model.Source;
import com.tahir.customworkflowbe.service.SourceLocalServiceUtil;
import com.tahir.customworkflowbe.service.persistence.SourcePersistence;
import com.tahir.customworkflowbe.service.persistence.SourceUtil;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

import org.junit.runner.RunWith;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @generated
 */
@RunWith(Arquillian.class)
public class SourcePersistenceTest {
	@ClassRule
	@Rule
	public static final AggregateTestRule aggregateTestRule = new AggregateTestRule(new LiferayIntegrationTestRule(),
			PersistenceTestRule.INSTANCE,
			new TransactionalTestRule(Propagation.REQUIRED));

	@Before
	public void setUp() {
		_persistence = SourceUtil.getPersistence();

		Class<?> clazz = _persistence.getClass();

		_dynamicQueryClassLoader = clazz.getClassLoader();
	}

	@After
	public void tearDown() throws Exception {
		Iterator<Source> iterator = _sources.iterator();

		while (iterator.hasNext()) {
			_persistence.remove(iterator.next());

			iterator.remove();
		}
	}

	@Test
	public void testCreate() throws Exception {
		long pk = RandomTestUtil.nextLong();

		Source source = _persistence.create(pk);

		Assert.assertNotNull(source);

		Assert.assertEquals(source.getPrimaryKey(), pk);
	}

	@Test
	public void testRemove() throws Exception {
		Source newSource = addSource();

		_persistence.remove(newSource);

		Source existingSource = _persistence.fetchByPrimaryKey(newSource.getPrimaryKey());

		Assert.assertNull(existingSource);
	}

	@Test
	public void testUpdateNew() throws Exception {
		addSource();
	}

	@Test
	public void testUpdateExisting() throws Exception {
		long pk = RandomTestUtil.nextLong();

		Source newSource = _persistence.create(pk);

		newSource.setGroupId(RandomTestUtil.nextLong());

		newSource.setCompanyId(RandomTestUtil.nextLong());

		newSource.setUserId(RandomTestUtil.nextLong());

		newSource.setUserName(RandomTestUtil.randomString());

		newSource.setCreateDate(RandomTestUtil.nextDate());

		newSource.setModifiedDate(RandomTestUtil.nextDate());

		newSource.setTitle(RandomTestUtil.randomString());

		newSource.setSource(RandomTestUtil.randomString());

		newSource.setColumnName(RandomTestUtil.randomString());

		newSource.setStatus(RandomTestUtil.randomBoolean());

		_sources.add(_persistence.update(newSource));

		Source existingSource = _persistence.findByPrimaryKey(newSource.getPrimaryKey());

		Assert.assertEquals(existingSource.getSourceId(),
			newSource.getSourceId());
		Assert.assertEquals(existingSource.getGroupId(), newSource.getGroupId());
		Assert.assertEquals(existingSource.getCompanyId(),
			newSource.getCompanyId());
		Assert.assertEquals(existingSource.getUserId(), newSource.getUserId());
		Assert.assertEquals(existingSource.getUserName(),
			newSource.getUserName());
		Assert.assertEquals(Time.getShortTimestamp(
				existingSource.getCreateDate()),
			Time.getShortTimestamp(newSource.getCreateDate()));
		Assert.assertEquals(Time.getShortTimestamp(
				existingSource.getModifiedDate()),
			Time.getShortTimestamp(newSource.getModifiedDate()));
		Assert.assertEquals(existingSource.getTitle(), newSource.getTitle());
		Assert.assertEquals(existingSource.getSource(), newSource.getSource());
		Assert.assertEquals(existingSource.getColumnName(),
			newSource.getColumnName());
		Assert.assertEquals(existingSource.getStatus(), newSource.getStatus());
	}

	@Test
	public void testCountByuserId() throws Exception {
		_persistence.countByuserId(RandomTestUtil.nextLong());

		_persistence.countByuserId(0L);
	}

	@Test
	public void testCountBytitle() throws Exception {
		_persistence.countBytitle(StringPool.BLANK);

		_persistence.countBytitle(StringPool.NULL);

		_persistence.countBytitle((String)null);
	}

	@Test
	public void testCountBystatus() throws Exception {
		_persistence.countBystatus(RandomTestUtil.randomBoolean());

		_persistence.countBystatus(RandomTestUtil.randomBoolean());
	}

	@Test
	public void testFindByPrimaryKeyExisting() throws Exception {
		Source newSource = addSource();

		Source existingSource = _persistence.findByPrimaryKey(newSource.getPrimaryKey());

		Assert.assertEquals(existingSource, newSource);
	}

	@Test(expected = NoSuchSourceException.class)
	public void testFindByPrimaryKeyMissing() throws Exception {
		long pk = RandomTestUtil.nextLong();

		_persistence.findByPrimaryKey(pk);
	}

	@Test
	public void testFindAll() throws Exception {
		_persistence.findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			getOrderByComparator());
	}

	protected OrderByComparator<Source> getOrderByComparator() {
		return OrderByComparatorFactoryUtil.create("custwf_Source", "sourceId",
			true, "groupId", true, "companyId", true, "userId", true,
			"userName", true, "createDate", true, "modifiedDate", true,
			"title", true, "source", true, "columnName", true, "status", true);
	}

	@Test
	public void testFetchByPrimaryKeyExisting() throws Exception {
		Source newSource = addSource();

		Source existingSource = _persistence.fetchByPrimaryKey(newSource.getPrimaryKey());

		Assert.assertEquals(existingSource, newSource);
	}

	@Test
	public void testFetchByPrimaryKeyMissing() throws Exception {
		long pk = RandomTestUtil.nextLong();

		Source missingSource = _persistence.fetchByPrimaryKey(pk);

		Assert.assertNull(missingSource);
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereAllPrimaryKeysExist()
		throws Exception {
		Source newSource1 = addSource();
		Source newSource2 = addSource();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newSource1.getPrimaryKey());
		primaryKeys.add(newSource2.getPrimaryKey());

		Map<Serializable, Source> sources = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(2, sources.size());
		Assert.assertEquals(newSource1, sources.get(newSource1.getPrimaryKey()));
		Assert.assertEquals(newSource2, sources.get(newSource2.getPrimaryKey()));
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereNoPrimaryKeysExist()
		throws Exception {
		long pk1 = RandomTestUtil.nextLong();

		long pk2 = RandomTestUtil.nextLong();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(pk1);
		primaryKeys.add(pk2);

		Map<Serializable, Source> sources = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertTrue(sources.isEmpty());
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereSomePrimaryKeysExist()
		throws Exception {
		Source newSource = addSource();

		long pk = RandomTestUtil.nextLong();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newSource.getPrimaryKey());
		primaryKeys.add(pk);

		Map<Serializable, Source> sources = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(1, sources.size());
		Assert.assertEquals(newSource, sources.get(newSource.getPrimaryKey()));
	}

	@Test
	public void testFetchByPrimaryKeysWithNoPrimaryKeys()
		throws Exception {
		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		Map<Serializable, Source> sources = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertTrue(sources.isEmpty());
	}

	@Test
	public void testFetchByPrimaryKeysWithOnePrimaryKey()
		throws Exception {
		Source newSource = addSource();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newSource.getPrimaryKey());

		Map<Serializable, Source> sources = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(1, sources.size());
		Assert.assertEquals(newSource, sources.get(newSource.getPrimaryKey()));
	}

	@Test
	public void testActionableDynamicQuery() throws Exception {
		final IntegerWrapper count = new IntegerWrapper();

		ActionableDynamicQuery actionableDynamicQuery = SourceLocalServiceUtil.getActionableDynamicQuery();

		actionableDynamicQuery.setPerformActionMethod(new ActionableDynamicQuery.PerformActionMethod<Source>() {
				@Override
				public void performAction(Source source) {
					Assert.assertNotNull(source);

					count.increment();
				}
			});

		actionableDynamicQuery.performActions();

		Assert.assertEquals(count.getValue(), _persistence.countAll());
	}

	@Test
	public void testDynamicQueryByPrimaryKeyExisting()
		throws Exception {
		Source newSource = addSource();

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Source.class,
				_dynamicQueryClassLoader);

		dynamicQuery.add(RestrictionsFactoryUtil.eq("sourceId",
				newSource.getSourceId()));

		List<Source> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(1, result.size());

		Source existingSource = result.get(0);

		Assert.assertEquals(existingSource, newSource);
	}

	@Test
	public void testDynamicQueryByPrimaryKeyMissing() throws Exception {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Source.class,
				_dynamicQueryClassLoader);

		dynamicQuery.add(RestrictionsFactoryUtil.eq("sourceId",
				RandomTestUtil.nextLong()));

		List<Source> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(0, result.size());
	}

	@Test
	public void testDynamicQueryByProjectionExisting()
		throws Exception {
		Source newSource = addSource();

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Source.class,
				_dynamicQueryClassLoader);

		dynamicQuery.setProjection(ProjectionFactoryUtil.property("sourceId"));

		Object newSourceId = newSource.getSourceId();

		dynamicQuery.add(RestrictionsFactoryUtil.in("sourceId",
				new Object[] { newSourceId }));

		List<Object> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(1, result.size());

		Object existingSourceId = result.get(0);

		Assert.assertEquals(existingSourceId, newSourceId);
	}

	@Test
	public void testDynamicQueryByProjectionMissing() throws Exception {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Source.class,
				_dynamicQueryClassLoader);

		dynamicQuery.setProjection(ProjectionFactoryUtil.property("sourceId"));

		dynamicQuery.add(RestrictionsFactoryUtil.in("sourceId",
				new Object[] { RandomTestUtil.nextLong() }));

		List<Object> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(0, result.size());
	}

	protected Source addSource() throws Exception {
		long pk = RandomTestUtil.nextLong();

		Source source = _persistence.create(pk);

		source.setGroupId(RandomTestUtil.nextLong());

		source.setCompanyId(RandomTestUtil.nextLong());

		source.setUserId(RandomTestUtil.nextLong());

		source.setUserName(RandomTestUtil.randomString());

		source.setCreateDate(RandomTestUtil.nextDate());

		source.setModifiedDate(RandomTestUtil.nextDate());

		source.setTitle(RandomTestUtil.randomString());

		source.setSource(RandomTestUtil.randomString());

		source.setColumnName(RandomTestUtil.randomString());

		source.setStatus(RandomTestUtil.randomBoolean());

		_sources.add(_persistence.update(source));

		return source;
	}

	private List<Source> _sources = new ArrayList<Source>();
	private SourcePersistence _persistence;
	private ClassLoader _dynamicQueryClassLoader;
}