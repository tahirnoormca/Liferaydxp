/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.tahir.customworkflowbe.service.persistence.test;

import com.liferay.arquillian.extension.junit.bridge.junit.Arquillian;

import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.test.rule.AggregateTestRule;
import com.liferay.portal.kernel.test.rule.TransactionalTestRule;
import com.liferay.portal.kernel.test.util.RandomTestUtil;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.util.IntegerWrapper;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.OrderByComparatorFactoryUtil;
import com.liferay.portal.kernel.util.Time;
import com.liferay.portal.test.rule.LiferayIntegrationTestRule;
import com.liferay.portal.test.rule.PersistenceTestRule;

import com.tahir.customworkflowbe.exception.NoSuchSiteformException;
import com.tahir.customworkflowbe.model.Siteform;
import com.tahir.customworkflowbe.service.SiteformLocalServiceUtil;
import com.tahir.customworkflowbe.service.persistence.SiteformPersistence;
import com.tahir.customworkflowbe.service.persistence.SiteformUtil;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

import org.junit.runner.RunWith;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @generated
 */
@RunWith(Arquillian.class)
public class SiteformPersistenceTest {
	@ClassRule
	@Rule
	public static final AggregateTestRule aggregateTestRule = new AggregateTestRule(new LiferayIntegrationTestRule(),
			PersistenceTestRule.INSTANCE,
			new TransactionalTestRule(Propagation.REQUIRED));

	@Before
	public void setUp() {
		_persistence = SiteformUtil.getPersistence();

		Class<?> clazz = _persistence.getClass();

		_dynamicQueryClassLoader = clazz.getClassLoader();
	}

	@After
	public void tearDown() throws Exception {
		Iterator<Siteform> iterator = _siteforms.iterator();

		while (iterator.hasNext()) {
			_persistence.remove(iterator.next());

			iterator.remove();
		}
	}

	@Test
	public void testCreate() throws Exception {
		long pk = RandomTestUtil.nextLong();

		Siteform siteform = _persistence.create(pk);

		Assert.assertNotNull(siteform);

		Assert.assertEquals(siteform.getPrimaryKey(), pk);
	}

	@Test
	public void testRemove() throws Exception {
		Siteform newSiteform = addSiteform();

		_persistence.remove(newSiteform);

		Siteform existingSiteform = _persistence.fetchByPrimaryKey(newSiteform.getPrimaryKey());

		Assert.assertNull(existingSiteform);
	}

	@Test
	public void testUpdateNew() throws Exception {
		addSiteform();
	}

	@Test
	public void testUpdateExisting() throws Exception {
		long pk = RandomTestUtil.nextLong();

		Siteform newSiteform = _persistence.create(pk);

		newSiteform.setSiteId(RandomTestUtil.nextLong());

		newSiteform.setSiteName(RandomTestUtil.randomString());

		newSiteform.setScreen1(RandomTestUtil.randomString());

		newSiteform.setScreen2(RandomTestUtil.randomString());

		newSiteform.setScreen3(RandomTestUtil.randomString());

		newSiteform.setScreen4(RandomTestUtil.randomString());

		newSiteform.setScreen5(RandomTestUtil.randomString());

		newSiteform.setScreen6(RandomTestUtil.randomString());

		newSiteform.setScreen7(RandomTestUtil.randomString());

		newSiteform.setScreen8(RandomTestUtil.randomString());

		newSiteform.setScreen9(RandomTestUtil.randomString());

		newSiteform.setScreen10(RandomTestUtil.randomString());

		newSiteform.setCreatedBy(RandomTestUtil.nextLong());

		newSiteform.setModifiedBy(RandomTestUtil.nextLong());

		newSiteform.setCreateDate(RandomTestUtil.nextDate());

		newSiteform.setModifiedDate(RandomTestUtil.nextDate());

		_siteforms.add(_persistence.update(newSiteform));

		Siteform existingSiteform = _persistence.findByPrimaryKey(newSiteform.getPrimaryKey());

		Assert.assertEquals(existingSiteform.getSiteFormId(),
			newSiteform.getSiteFormId());
		Assert.assertEquals(existingSiteform.getSiteId(),
			newSiteform.getSiteId());
		Assert.assertEquals(existingSiteform.getSiteName(),
			newSiteform.getSiteName());
		Assert.assertEquals(existingSiteform.getScreen1(),
			newSiteform.getScreen1());
		Assert.assertEquals(existingSiteform.getScreen2(),
			newSiteform.getScreen2());
		Assert.assertEquals(existingSiteform.getScreen3(),
			newSiteform.getScreen3());
		Assert.assertEquals(existingSiteform.getScreen4(),
			newSiteform.getScreen4());
		Assert.assertEquals(existingSiteform.getScreen5(),
			newSiteform.getScreen5());
		Assert.assertEquals(existingSiteform.getScreen6(),
			newSiteform.getScreen6());
		Assert.assertEquals(existingSiteform.getScreen7(),
			newSiteform.getScreen7());
		Assert.assertEquals(existingSiteform.getScreen8(),
			newSiteform.getScreen8());
		Assert.assertEquals(existingSiteform.getScreen9(),
			newSiteform.getScreen9());
		Assert.assertEquals(existingSiteform.getScreen10(),
			newSiteform.getScreen10());
		Assert.assertEquals(existingSiteform.getCreatedBy(),
			newSiteform.getCreatedBy());
		Assert.assertEquals(existingSiteform.getModifiedBy(),
			newSiteform.getModifiedBy());
		Assert.assertEquals(Time.getShortTimestamp(
				existingSiteform.getCreateDate()),
			Time.getShortTimestamp(newSiteform.getCreateDate()));
		Assert.assertEquals(Time.getShortTimestamp(
				existingSiteform.getModifiedDate()),
			Time.getShortTimestamp(newSiteform.getModifiedDate()));
	}

	@Test
	public void testFindByPrimaryKeyExisting() throws Exception {
		Siteform newSiteform = addSiteform();

		Siteform existingSiteform = _persistence.findByPrimaryKey(newSiteform.getPrimaryKey());

		Assert.assertEquals(existingSiteform, newSiteform);
	}

	@Test(expected = NoSuchSiteformException.class)
	public void testFindByPrimaryKeyMissing() throws Exception {
		long pk = RandomTestUtil.nextLong();

		_persistence.findByPrimaryKey(pk);
	}

	@Test
	public void testFindAll() throws Exception {
		_persistence.findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			getOrderByComparator());
	}

	protected OrderByComparator<Siteform> getOrderByComparator() {
		return OrderByComparatorFactoryUtil.create("custwf_Siteform",
			"siteFormId", true, "siteId", true, "siteName", true, "screen1",
			true, "screen2", true, "screen3", true, "screen4", true, "screen5",
			true, "screen6", true, "screen7", true, "screen8", true, "screen9",
			true, "screen10", true, "createdBy", true, "modifiedBy", true,
			"createDate", true, "modifiedDate", true);
	}

	@Test
	public void testFetchByPrimaryKeyExisting() throws Exception {
		Siteform newSiteform = addSiteform();

		Siteform existingSiteform = _persistence.fetchByPrimaryKey(newSiteform.getPrimaryKey());

		Assert.assertEquals(existingSiteform, newSiteform);
	}

	@Test
	public void testFetchByPrimaryKeyMissing() throws Exception {
		long pk = RandomTestUtil.nextLong();

		Siteform missingSiteform = _persistence.fetchByPrimaryKey(pk);

		Assert.assertNull(missingSiteform);
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereAllPrimaryKeysExist()
		throws Exception {
		Siteform newSiteform1 = addSiteform();
		Siteform newSiteform2 = addSiteform();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newSiteform1.getPrimaryKey());
		primaryKeys.add(newSiteform2.getPrimaryKey());

		Map<Serializable, Siteform> siteforms = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(2, siteforms.size());
		Assert.assertEquals(newSiteform1,
			siteforms.get(newSiteform1.getPrimaryKey()));
		Assert.assertEquals(newSiteform2,
			siteforms.get(newSiteform2.getPrimaryKey()));
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereNoPrimaryKeysExist()
		throws Exception {
		long pk1 = RandomTestUtil.nextLong();

		long pk2 = RandomTestUtil.nextLong();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(pk1);
		primaryKeys.add(pk2);

		Map<Serializable, Siteform> siteforms = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertTrue(siteforms.isEmpty());
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereSomePrimaryKeysExist()
		throws Exception {
		Siteform newSiteform = addSiteform();

		long pk = RandomTestUtil.nextLong();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newSiteform.getPrimaryKey());
		primaryKeys.add(pk);

		Map<Serializable, Siteform> siteforms = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(1, siteforms.size());
		Assert.assertEquals(newSiteform,
			siteforms.get(newSiteform.getPrimaryKey()));
	}

	@Test
	public void testFetchByPrimaryKeysWithNoPrimaryKeys()
		throws Exception {
		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		Map<Serializable, Siteform> siteforms = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertTrue(siteforms.isEmpty());
	}

	@Test
	public void testFetchByPrimaryKeysWithOnePrimaryKey()
		throws Exception {
		Siteform newSiteform = addSiteform();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newSiteform.getPrimaryKey());

		Map<Serializable, Siteform> siteforms = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(1, siteforms.size());
		Assert.assertEquals(newSiteform,
			siteforms.get(newSiteform.getPrimaryKey()));
	}

	@Test
	public void testActionableDynamicQuery() throws Exception {
		final IntegerWrapper count = new IntegerWrapper();

		ActionableDynamicQuery actionableDynamicQuery = SiteformLocalServiceUtil.getActionableDynamicQuery();

		actionableDynamicQuery.setPerformActionMethod(new ActionableDynamicQuery.PerformActionMethod<Siteform>() {
				@Override
				public void performAction(Siteform siteform) {
					Assert.assertNotNull(siteform);

					count.increment();
				}
			});

		actionableDynamicQuery.performActions();

		Assert.assertEquals(count.getValue(), _persistence.countAll());
	}

	@Test
	public void testDynamicQueryByPrimaryKeyExisting()
		throws Exception {
		Siteform newSiteform = addSiteform();

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Siteform.class,
				_dynamicQueryClassLoader);

		dynamicQuery.add(RestrictionsFactoryUtil.eq("siteFormId",
				newSiteform.getSiteFormId()));

		List<Siteform> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(1, result.size());

		Siteform existingSiteform = result.get(0);

		Assert.assertEquals(existingSiteform, newSiteform);
	}

	@Test
	public void testDynamicQueryByPrimaryKeyMissing() throws Exception {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Siteform.class,
				_dynamicQueryClassLoader);

		dynamicQuery.add(RestrictionsFactoryUtil.eq("siteFormId",
				RandomTestUtil.nextLong()));

		List<Siteform> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(0, result.size());
	}

	@Test
	public void testDynamicQueryByProjectionExisting()
		throws Exception {
		Siteform newSiteform = addSiteform();

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Siteform.class,
				_dynamicQueryClassLoader);

		dynamicQuery.setProjection(ProjectionFactoryUtil.property("siteFormId"));

		Object newSiteFormId = newSiteform.getSiteFormId();

		dynamicQuery.add(RestrictionsFactoryUtil.in("siteFormId",
				new Object[] { newSiteFormId }));

		List<Object> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(1, result.size());

		Object existingSiteFormId = result.get(0);

		Assert.assertEquals(existingSiteFormId, newSiteFormId);
	}

	@Test
	public void testDynamicQueryByProjectionMissing() throws Exception {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Siteform.class,
				_dynamicQueryClassLoader);

		dynamicQuery.setProjection(ProjectionFactoryUtil.property("siteFormId"));

		dynamicQuery.add(RestrictionsFactoryUtil.in("siteFormId",
				new Object[] { RandomTestUtil.nextLong() }));

		List<Object> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(0, result.size());
	}

	protected Siteform addSiteform() throws Exception {
		long pk = RandomTestUtil.nextLong();

		Siteform siteform = _persistence.create(pk);

		siteform.setSiteId(RandomTestUtil.nextLong());

		siteform.setSiteName(RandomTestUtil.randomString());

		siteform.setScreen1(RandomTestUtil.randomString());

		siteform.setScreen2(RandomTestUtil.randomString());

		siteform.setScreen3(RandomTestUtil.randomString());

		siteform.setScreen4(RandomTestUtil.randomString());

		siteform.setScreen5(RandomTestUtil.randomString());

		siteform.setScreen6(RandomTestUtil.randomString());

		siteform.setScreen7(RandomTestUtil.randomString());

		siteform.setScreen8(RandomTestUtil.randomString());

		siteform.setScreen9(RandomTestUtil.randomString());

		siteform.setScreen10(RandomTestUtil.randomString());

		siteform.setCreatedBy(RandomTestUtil.nextLong());

		siteform.setModifiedBy(RandomTestUtil.nextLong());

		siteform.setCreateDate(RandomTestUtil.nextDate());

		siteform.setModifiedDate(RandomTestUtil.nextDate());

		_siteforms.add(_persistence.update(siteform));

		return siteform;
	}

	private List<Siteform> _siteforms = new ArrayList<Siteform>();
	private SiteformPersistence _persistence;
	private ClassLoader _dynamicQueryClassLoader;
}