/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.tahir.customworkflowbe.service.base;

import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.dao.db.DB;
import com.liferay.portal.kernel.dao.db.DBManagerUtil;
import com.liferay.portal.kernel.dao.jdbc.SqlUpdate;
import com.liferay.portal.kernel.dao.jdbc.SqlUpdateFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.module.framework.service.IdentifiableOSGiService;
import com.liferay.portal.kernel.service.BaseServiceImpl;
import com.liferay.portal.kernel.service.persistence.ClassNamePersistence;
import com.liferay.portal.kernel.service.persistence.UserPersistence;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.spring.extender.service.ServiceReference;

import com.tahir.customworkflowbe.model.Feedback;
import com.tahir.customworkflowbe.service.FeedbackService;
import com.tahir.customworkflowbe.service.persistence.BookPersistence;
import com.tahir.customworkflowbe.service.persistence.FeedbackPersistence;
import com.tahir.customworkflowbe.service.persistence.SiteformPersistence;
import com.tahir.customworkflowbe.service.persistence.SourceFinder;
import com.tahir.customworkflowbe.service.persistence.SourcePersistence;

import javax.sql.DataSource;

/**
 * Provides the base implementation for the feedback remote service.
 *
 * <p>
 * This implementation exists only as a container for the default service methods generated by ServiceBuilder. All custom service methods should be put in {@link com.tahir.customworkflowbe.service.impl.FeedbackServiceImpl}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see com.tahir.customworkflowbe.service.impl.FeedbackServiceImpl
 * @see com.tahir.customworkflowbe.service.FeedbackServiceUtil
 * @generated
 */
public abstract class FeedbackServiceBaseImpl extends BaseServiceImpl
	implements FeedbackService, IdentifiableOSGiService {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link com.tahir.customworkflowbe.service.FeedbackServiceUtil} to access the feedback remote service.
	 */

	/**
	 * Returns the book local service.
	 *
	 * @return the book local service
	 */
	public com.tahir.customworkflowbe.service.BookLocalService getBookLocalService() {
		return bookLocalService;
	}

	/**
	 * Sets the book local service.
	 *
	 * @param bookLocalService the book local service
	 */
	public void setBookLocalService(
		com.tahir.customworkflowbe.service.BookLocalService bookLocalService) {
		this.bookLocalService = bookLocalService;
	}

	/**
	 * Returns the book remote service.
	 *
	 * @return the book remote service
	 */
	public com.tahir.customworkflowbe.service.BookService getBookService() {
		return bookService;
	}

	/**
	 * Sets the book remote service.
	 *
	 * @param bookService the book remote service
	 */
	public void setBookService(
		com.tahir.customworkflowbe.service.BookService bookService) {
		this.bookService = bookService;
	}

	/**
	 * Returns the book persistence.
	 *
	 * @return the book persistence
	 */
	public BookPersistence getBookPersistence() {
		return bookPersistence;
	}

	/**
	 * Sets the book persistence.
	 *
	 * @param bookPersistence the book persistence
	 */
	public void setBookPersistence(BookPersistence bookPersistence) {
		this.bookPersistence = bookPersistence;
	}

	/**
	 * Returns the feedback local service.
	 *
	 * @return the feedback local service
	 */
	public com.tahir.customworkflowbe.service.FeedbackLocalService getFeedbackLocalService() {
		return feedbackLocalService;
	}

	/**
	 * Sets the feedback local service.
	 *
	 * @param feedbackLocalService the feedback local service
	 */
	public void setFeedbackLocalService(
		com.tahir.customworkflowbe.service.FeedbackLocalService feedbackLocalService) {
		this.feedbackLocalService = feedbackLocalService;
	}

	/**
	 * Returns the feedback remote service.
	 *
	 * @return the feedback remote service
	 */
	public FeedbackService getFeedbackService() {
		return feedbackService;
	}

	/**
	 * Sets the feedback remote service.
	 *
	 * @param feedbackService the feedback remote service
	 */
	public void setFeedbackService(FeedbackService feedbackService) {
		this.feedbackService = feedbackService;
	}

	/**
	 * Returns the feedback persistence.
	 *
	 * @return the feedback persistence
	 */
	public FeedbackPersistence getFeedbackPersistence() {
		return feedbackPersistence;
	}

	/**
	 * Sets the feedback persistence.
	 *
	 * @param feedbackPersistence the feedback persistence
	 */
	public void setFeedbackPersistence(FeedbackPersistence feedbackPersistence) {
		this.feedbackPersistence = feedbackPersistence;
	}

	/**
	 * Returns the siteform local service.
	 *
	 * @return the siteform local service
	 */
	public com.tahir.customworkflowbe.service.SiteformLocalService getSiteformLocalService() {
		return siteformLocalService;
	}

	/**
	 * Sets the siteform local service.
	 *
	 * @param siteformLocalService the siteform local service
	 */
	public void setSiteformLocalService(
		com.tahir.customworkflowbe.service.SiteformLocalService siteformLocalService) {
		this.siteformLocalService = siteformLocalService;
	}

	/**
	 * Returns the siteform remote service.
	 *
	 * @return the siteform remote service
	 */
	public com.tahir.customworkflowbe.service.SiteformService getSiteformService() {
		return siteformService;
	}

	/**
	 * Sets the siteform remote service.
	 *
	 * @param siteformService the siteform remote service
	 */
	public void setSiteformService(
		com.tahir.customworkflowbe.service.SiteformService siteformService) {
		this.siteformService = siteformService;
	}

	/**
	 * Returns the siteform persistence.
	 *
	 * @return the siteform persistence
	 */
	public SiteformPersistence getSiteformPersistence() {
		return siteformPersistence;
	}

	/**
	 * Sets the siteform persistence.
	 *
	 * @param siteformPersistence the siteform persistence
	 */
	public void setSiteformPersistence(SiteformPersistence siteformPersistence) {
		this.siteformPersistence = siteformPersistence;
	}

	/**
	 * Returns the source local service.
	 *
	 * @return the source local service
	 */
	public com.tahir.customworkflowbe.service.SourceLocalService getSourceLocalService() {
		return sourceLocalService;
	}

	/**
	 * Sets the source local service.
	 *
	 * @param sourceLocalService the source local service
	 */
	public void setSourceLocalService(
		com.tahir.customworkflowbe.service.SourceLocalService sourceLocalService) {
		this.sourceLocalService = sourceLocalService;
	}

	/**
	 * Returns the source remote service.
	 *
	 * @return the source remote service
	 */
	public com.tahir.customworkflowbe.service.SourceService getSourceService() {
		return sourceService;
	}

	/**
	 * Sets the source remote service.
	 *
	 * @param sourceService the source remote service
	 */
	public void setSourceService(
		com.tahir.customworkflowbe.service.SourceService sourceService) {
		this.sourceService = sourceService;
	}

	/**
	 * Returns the source persistence.
	 *
	 * @return the source persistence
	 */
	public SourcePersistence getSourcePersistence() {
		return sourcePersistence;
	}

	/**
	 * Sets the source persistence.
	 *
	 * @param sourcePersistence the source persistence
	 */
	public void setSourcePersistence(SourcePersistence sourcePersistence) {
		this.sourcePersistence = sourcePersistence;
	}

	/**
	 * Returns the source finder.
	 *
	 * @return the source finder
	 */
	public SourceFinder getSourceFinder() {
		return sourceFinder;
	}

	/**
	 * Sets the source finder.
	 *
	 * @param sourceFinder the source finder
	 */
	public void setSourceFinder(SourceFinder sourceFinder) {
		this.sourceFinder = sourceFinder;
	}

	/**
	 * Returns the counter local service.
	 *
	 * @return the counter local service
	 */
	public com.liferay.counter.kernel.service.CounterLocalService getCounterLocalService() {
		return counterLocalService;
	}

	/**
	 * Sets the counter local service.
	 *
	 * @param counterLocalService the counter local service
	 */
	public void setCounterLocalService(
		com.liferay.counter.kernel.service.CounterLocalService counterLocalService) {
		this.counterLocalService = counterLocalService;
	}

	/**
	 * Returns the class name local service.
	 *
	 * @return the class name local service
	 */
	public com.liferay.portal.kernel.service.ClassNameLocalService getClassNameLocalService() {
		return classNameLocalService;
	}

	/**
	 * Sets the class name local service.
	 *
	 * @param classNameLocalService the class name local service
	 */
	public void setClassNameLocalService(
		com.liferay.portal.kernel.service.ClassNameLocalService classNameLocalService) {
		this.classNameLocalService = classNameLocalService;
	}

	/**
	 * Returns the class name remote service.
	 *
	 * @return the class name remote service
	 */
	public com.liferay.portal.kernel.service.ClassNameService getClassNameService() {
		return classNameService;
	}

	/**
	 * Sets the class name remote service.
	 *
	 * @param classNameService the class name remote service
	 */
	public void setClassNameService(
		com.liferay.portal.kernel.service.ClassNameService classNameService) {
		this.classNameService = classNameService;
	}

	/**
	 * Returns the class name persistence.
	 *
	 * @return the class name persistence
	 */
	public ClassNamePersistence getClassNamePersistence() {
		return classNamePersistence;
	}

	/**
	 * Sets the class name persistence.
	 *
	 * @param classNamePersistence the class name persistence
	 */
	public void setClassNamePersistence(
		ClassNamePersistence classNamePersistence) {
		this.classNamePersistence = classNamePersistence;
	}

	/**
	 * Returns the resource local service.
	 *
	 * @return the resource local service
	 */
	public com.liferay.portal.kernel.service.ResourceLocalService getResourceLocalService() {
		return resourceLocalService;
	}

	/**
	 * Sets the resource local service.
	 *
	 * @param resourceLocalService the resource local service
	 */
	public void setResourceLocalService(
		com.liferay.portal.kernel.service.ResourceLocalService resourceLocalService) {
		this.resourceLocalService = resourceLocalService;
	}

	/**
	 * Returns the user local service.
	 *
	 * @return the user local service
	 */
	public com.liferay.portal.kernel.service.UserLocalService getUserLocalService() {
		return userLocalService;
	}

	/**
	 * Sets the user local service.
	 *
	 * @param userLocalService the user local service
	 */
	public void setUserLocalService(
		com.liferay.portal.kernel.service.UserLocalService userLocalService) {
		this.userLocalService = userLocalService;
	}

	/**
	 * Returns the user remote service.
	 *
	 * @return the user remote service
	 */
	public com.liferay.portal.kernel.service.UserService getUserService() {
		return userService;
	}

	/**
	 * Sets the user remote service.
	 *
	 * @param userService the user remote service
	 */
	public void setUserService(
		com.liferay.portal.kernel.service.UserService userService) {
		this.userService = userService;
	}

	/**
	 * Returns the user persistence.
	 *
	 * @return the user persistence
	 */
	public UserPersistence getUserPersistence() {
		return userPersistence;
	}

	/**
	 * Sets the user persistence.
	 *
	 * @param userPersistence the user persistence
	 */
	public void setUserPersistence(UserPersistence userPersistence) {
		this.userPersistence = userPersistence;
	}

	public void afterPropertiesSet() {
	}

	public void destroy() {
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return FeedbackService.class.getName();
	}

	protected Class<?> getModelClass() {
		return Feedback.class;
	}

	protected String getModelClassName() {
		return Feedback.class.getName();
	}

	/**
	 * Performs a SQL query.
	 *
	 * @param sql the sql query
	 */
	protected void runSQL(String sql) {
		try {
			DataSource dataSource = feedbackPersistence.getDataSource();

			DB db = DBManagerUtil.getDB();

			sql = db.buildSQL(sql);
			sql = PortalUtil.transformSQL(sql);

			SqlUpdate sqlUpdate = SqlUpdateFactoryUtil.getSqlUpdate(dataSource,
					sql);

			sqlUpdate.update();
		}
		catch (Exception e) {
			throw new SystemException(e);
		}
	}

	@BeanReference(type = com.tahir.customworkflowbe.service.BookLocalService.class)
	protected com.tahir.customworkflowbe.service.BookLocalService bookLocalService;
	@BeanReference(type = com.tahir.customworkflowbe.service.BookService.class)
	protected com.tahir.customworkflowbe.service.BookService bookService;
	@BeanReference(type = BookPersistence.class)
	protected BookPersistence bookPersistence;
	@BeanReference(type = com.tahir.customworkflowbe.service.FeedbackLocalService.class)
	protected com.tahir.customworkflowbe.service.FeedbackLocalService feedbackLocalService;
	@BeanReference(type = FeedbackService.class)
	protected FeedbackService feedbackService;
	@BeanReference(type = FeedbackPersistence.class)
	protected FeedbackPersistence feedbackPersistence;
	@BeanReference(type = com.tahir.customworkflowbe.service.SiteformLocalService.class)
	protected com.tahir.customworkflowbe.service.SiteformLocalService siteformLocalService;
	@BeanReference(type = com.tahir.customworkflowbe.service.SiteformService.class)
	protected com.tahir.customworkflowbe.service.SiteformService siteformService;
	@BeanReference(type = SiteformPersistence.class)
	protected SiteformPersistence siteformPersistence;
	@BeanReference(type = com.tahir.customworkflowbe.service.SourceLocalService.class)
	protected com.tahir.customworkflowbe.service.SourceLocalService sourceLocalService;
	@BeanReference(type = com.tahir.customworkflowbe.service.SourceService.class)
	protected com.tahir.customworkflowbe.service.SourceService sourceService;
	@BeanReference(type = SourcePersistence.class)
	protected SourcePersistence sourcePersistence;
	@BeanReference(type = SourceFinder.class)
	protected SourceFinder sourceFinder;
	@ServiceReference(type = com.liferay.counter.kernel.service.CounterLocalService.class)
	protected com.liferay.counter.kernel.service.CounterLocalService counterLocalService;
	@ServiceReference(type = com.liferay.portal.kernel.service.ClassNameLocalService.class)
	protected com.liferay.portal.kernel.service.ClassNameLocalService classNameLocalService;
	@ServiceReference(type = com.liferay.portal.kernel.service.ClassNameService.class)
	protected com.liferay.portal.kernel.service.ClassNameService classNameService;
	@ServiceReference(type = ClassNamePersistence.class)
	protected ClassNamePersistence classNamePersistence;
	@ServiceReference(type = com.liferay.portal.kernel.service.ResourceLocalService.class)
	protected com.liferay.portal.kernel.service.ResourceLocalService resourceLocalService;
	@ServiceReference(type = com.liferay.portal.kernel.service.UserLocalService.class)
	protected com.liferay.portal.kernel.service.UserLocalService userLocalService;
	@ServiceReference(type = com.liferay.portal.kernel.service.UserService.class)
	protected com.liferay.portal.kernel.service.UserService userService;
	@ServiceReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
}