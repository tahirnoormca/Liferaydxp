/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.tahir.customworkflowbe.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.tahir.customworkflowbe.model.Siteform;
import com.tahir.customworkflowbe.service.SiteformLocalServiceUtil;

/**
 * The extended model base implementation for the Siteform service. Represents a row in the &quot;custwf_Siteform&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This class exists only as a container for the default extended model level methods generated by ServiceBuilder. Helper methods and all application logic should be put in {@link SiteformImpl}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see SiteformImpl
 * @see Siteform
 * @generated
 */
@ProviderType
public abstract class SiteformBaseImpl extends SiteformModelImpl
	implements Siteform {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. All methods that expect a siteform model instance should use the {@link Siteform} interface instead.
	 */
	@Override
	public void persist() {
		if (this.isNew()) {
			SiteformLocalServiceUtil.addSiteform(this);
		}
		else {
			SiteformLocalServiceUtil.updateSiteform(this);
		}
	}
}