/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.tahir.customworkflowbe.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import com.tahir.customworkflowbe.model.Source;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Source in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Source
 * @generated
 */
@ProviderType
public class SourceCacheModel implements CacheModel<Source>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof SourceCacheModel)) {
			return false;
		}

		SourceCacheModel sourceCacheModel = (SourceCacheModel)obj;

		if (sourceId == sourceCacheModel.sourceId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, sourceId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{sourceId=");
		sb.append(sourceId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", title=");
		sb.append(title);
		sb.append(", source=");
		sb.append(source);
		sb.append(", columnName=");
		sb.append(columnName);
		sb.append(", status=");
		sb.append(status);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Source toEntityModel() {
		SourceImpl sourceImpl = new SourceImpl();

		sourceImpl.setSourceId(sourceId);
		sourceImpl.setGroupId(groupId);
		sourceImpl.setCompanyId(companyId);
		sourceImpl.setUserId(userId);

		if (userName == null) {
			sourceImpl.setUserName(StringPool.BLANK);
		}
		else {
			sourceImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			sourceImpl.setCreateDate(null);
		}
		else {
			sourceImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			sourceImpl.setModifiedDate(null);
		}
		else {
			sourceImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (title == null) {
			sourceImpl.setTitle(StringPool.BLANK);
		}
		else {
			sourceImpl.setTitle(title);
		}

		if (source == null) {
			sourceImpl.setSource(StringPool.BLANK);
		}
		else {
			sourceImpl.setSource(source);
		}

		if (columnName == null) {
			sourceImpl.setColumnName(StringPool.BLANK);
		}
		else {
			sourceImpl.setColumnName(columnName);
		}

		sourceImpl.setStatus(status);

		sourceImpl.resetOriginalValues();

		return sourceImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		sourceId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		title = objectInput.readUTF();
		source = objectInput.readUTF();
		columnName = objectInput.readUTF();

		status = objectInput.readBoolean();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(sourceId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (title == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(title);
		}

		if (source == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(source);
		}

		if (columnName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(columnName);
		}

		objectOutput.writeBoolean(status);
	}

	public long sourceId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String title;
	public String source;
	public String columnName;
	public boolean status;
}