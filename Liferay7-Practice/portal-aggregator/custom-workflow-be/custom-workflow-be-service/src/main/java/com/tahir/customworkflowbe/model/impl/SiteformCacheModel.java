/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.tahir.customworkflowbe.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import com.tahir.customworkflowbe.model.Siteform;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Siteform in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Siteform
 * @generated
 */
@ProviderType
public class SiteformCacheModel implements CacheModel<Siteform>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof SiteformCacheModel)) {
			return false;
		}

		SiteformCacheModel siteformCacheModel = (SiteformCacheModel)obj;

		if (siteFormId == siteformCacheModel.siteFormId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, siteFormId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(35);

		sb.append("{siteFormId=");
		sb.append(siteFormId);
		sb.append(", siteId=");
		sb.append(siteId);
		sb.append(", siteName=");
		sb.append(siteName);
		sb.append(", screen1=");
		sb.append(screen1);
		sb.append(", screen2=");
		sb.append(screen2);
		sb.append(", screen3=");
		sb.append(screen3);
		sb.append(", screen4=");
		sb.append(screen4);
		sb.append(", screen5=");
		sb.append(screen5);
		sb.append(", screen6=");
		sb.append(screen6);
		sb.append(", screen7=");
		sb.append(screen7);
		sb.append(", screen8=");
		sb.append(screen8);
		sb.append(", screen9=");
		sb.append(screen9);
		sb.append(", screen10=");
		sb.append(screen10);
		sb.append(", createdBy=");
		sb.append(createdBy);
		sb.append(", modifiedBy=");
		sb.append(modifiedBy);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Siteform toEntityModel() {
		SiteformImpl siteformImpl = new SiteformImpl();

		siteformImpl.setSiteFormId(siteFormId);
		siteformImpl.setSiteId(siteId);

		if (siteName == null) {
			siteformImpl.setSiteName(StringPool.BLANK);
		}
		else {
			siteformImpl.setSiteName(siteName);
		}

		if (screen1 == null) {
			siteformImpl.setScreen1(StringPool.BLANK);
		}
		else {
			siteformImpl.setScreen1(screen1);
		}

		if (screen2 == null) {
			siteformImpl.setScreen2(StringPool.BLANK);
		}
		else {
			siteformImpl.setScreen2(screen2);
		}

		if (screen3 == null) {
			siteformImpl.setScreen3(StringPool.BLANK);
		}
		else {
			siteformImpl.setScreen3(screen3);
		}

		if (screen4 == null) {
			siteformImpl.setScreen4(StringPool.BLANK);
		}
		else {
			siteformImpl.setScreen4(screen4);
		}

		if (screen5 == null) {
			siteformImpl.setScreen5(StringPool.BLANK);
		}
		else {
			siteformImpl.setScreen5(screen5);
		}

		if (screen6 == null) {
			siteformImpl.setScreen6(StringPool.BLANK);
		}
		else {
			siteformImpl.setScreen6(screen6);
		}

		if (screen7 == null) {
			siteformImpl.setScreen7(StringPool.BLANK);
		}
		else {
			siteformImpl.setScreen7(screen7);
		}

		if (screen8 == null) {
			siteformImpl.setScreen8(StringPool.BLANK);
		}
		else {
			siteformImpl.setScreen8(screen8);
		}

		if (screen9 == null) {
			siteformImpl.setScreen9(StringPool.BLANK);
		}
		else {
			siteformImpl.setScreen9(screen9);
		}

		if (screen10 == null) {
			siteformImpl.setScreen10(StringPool.BLANK);
		}
		else {
			siteformImpl.setScreen10(screen10);
		}

		siteformImpl.setCreatedBy(createdBy);
		siteformImpl.setModifiedBy(modifiedBy);

		if (createDate == Long.MIN_VALUE) {
			siteformImpl.setCreateDate(null);
		}
		else {
			siteformImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			siteformImpl.setModifiedDate(null);
		}
		else {
			siteformImpl.setModifiedDate(new Date(modifiedDate));
		}

		siteformImpl.resetOriginalValues();

		return siteformImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		siteFormId = objectInput.readLong();

		siteId = objectInput.readLong();
		siteName = objectInput.readUTF();
		screen1 = objectInput.readUTF();
		screen2 = objectInput.readUTF();
		screen3 = objectInput.readUTF();
		screen4 = objectInput.readUTF();
		screen5 = objectInput.readUTF();
		screen6 = objectInput.readUTF();
		screen7 = objectInput.readUTF();
		screen8 = objectInput.readUTF();
		screen9 = objectInput.readUTF();
		screen10 = objectInput.readUTF();

		createdBy = objectInput.readLong();

		modifiedBy = objectInput.readLong();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(siteFormId);

		objectOutput.writeLong(siteId);

		if (siteName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(siteName);
		}

		if (screen1 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(screen1);
		}

		if (screen2 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(screen2);
		}

		if (screen3 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(screen3);
		}

		if (screen4 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(screen4);
		}

		if (screen5 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(screen5);
		}

		if (screen6 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(screen6);
		}

		if (screen7 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(screen7);
		}

		if (screen8 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(screen8);
		}

		if (screen9 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(screen9);
		}

		if (screen10 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(screen10);
		}

		objectOutput.writeLong(createdBy);

		objectOutput.writeLong(modifiedBy);
		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);
	}

	public long siteFormId;
	public long siteId;
	public String siteName;
	public String screen1;
	public String screen2;
	public String screen3;
	public String screen4;
	public String screen5;
	public String screen6;
	public String screen7;
	public String screen8;
	public String screen9;
	public String screen10;
	public long createdBy;
	public long modifiedBy;
	public long createDate;
	public long modifiedDate;
}