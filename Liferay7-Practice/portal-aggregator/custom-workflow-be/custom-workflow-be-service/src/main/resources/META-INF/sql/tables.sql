create table custwf_Book (
	uuid_ VARCHAR(75) null,
	bookID LONG not null primary key,
	bookName VARCHAR(75) null,
	description VARCHAR(75) null,
	authorName VARCHAR(75) null,
	isbn INTEGER,
	price INTEGER
);

create table custwf_Feedback (
	uuid_ VARCHAR(75) null,
	feedbackId LONG not null primary key,
	feedbackDate DATE null,
	feedbackText VARCHAR(75) null,
	feedbackSubject VARCHAR(75) null,
	feedBackStatus INTEGER,
	statusByUserId LONG,
	statusDate DATE null,
	companyId LONG,
	groupId LONG,
	userId LONG
);

create table custwf_Siteform (
	siteFormId LONG not null primary key,
	siteId LONG,
	siteName VARCHAR(75) null,
	screen1 VARCHAR(75) null,
	screen2 VARCHAR(75) null,
	screen3 VARCHAR(75) null,
	screen4 VARCHAR(75) null,
	screen5 VARCHAR(75) null,
	screen6 VARCHAR(75) null,
	screen7 VARCHAR(75) null,
	screen8 VARCHAR(75) null,
	screen9 VARCHAR(75) null,
	screen10 VARCHAR(75) null,
	createdBy LONG,
	modifiedBy LONG,
	createDate DATE null,
	modifiedDate DATE null
);

create table custwf_Source (
	sourceId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	title VARCHAR(75) null,
	source VARCHAR(75) null,
	columnName VARCHAR(75) null,
	status BOOLEAN
);