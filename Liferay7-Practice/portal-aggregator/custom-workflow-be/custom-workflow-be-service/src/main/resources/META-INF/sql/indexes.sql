create index IX_22BBB128 on custwf_Book (uuid_[$COLUMN_LENGTH:75$]);

create index IX_D60D8500 on custwf_Feedback (companyId);
create index IX_19A527EE on custwf_Feedback (feedbackText[$COLUMN_LENGTH:75$]);
create index IX_9493332D on custwf_Feedback (groupId, feedBackStatus);
create index IX_8F4DE1C on custwf_Feedback (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_AF163B9E on custwf_Feedback (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_E93F8964 on custwf_Source (status);
create index IX_2AAEF0CA on custwf_Source (title[$COLUMN_LENGTH:75$]);
create index IX_8ABEA2B8 on custwf_Source (userId);