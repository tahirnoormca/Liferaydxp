/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.tahir.customworkflowbe.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import com.tahir.customworkflowbe.exception.NoSuchSiteformException;
import com.tahir.customworkflowbe.model.Siteform;

/**
 * The persistence interface for the siteform service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see com.tahir.customworkflowbe.service.persistence.impl.SiteformPersistenceImpl
 * @see SiteformUtil
 * @generated
 */
@ProviderType
public interface SiteformPersistence extends BasePersistence<Siteform> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link SiteformUtil} to access the siteform persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the siteform in the entity cache if it is enabled.
	*
	* @param siteform the siteform
	*/
	public void cacheResult(Siteform siteform);

	/**
	* Caches the siteforms in the entity cache if it is enabled.
	*
	* @param siteforms the siteforms
	*/
	public void cacheResult(java.util.List<Siteform> siteforms);

	/**
	* Creates a new siteform with the primary key. Does not add the siteform to the database.
	*
	* @param siteFormId the primary key for the new siteform
	* @return the new siteform
	*/
	public Siteform create(long siteFormId);

	/**
	* Removes the siteform with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param siteFormId the primary key of the siteform
	* @return the siteform that was removed
	* @throws NoSuchSiteformException if a siteform with the primary key could not be found
	*/
	public Siteform remove(long siteFormId) throws NoSuchSiteformException;

	public Siteform updateImpl(Siteform siteform);

	/**
	* Returns the siteform with the primary key or throws a {@link NoSuchSiteformException} if it could not be found.
	*
	* @param siteFormId the primary key of the siteform
	* @return the siteform
	* @throws NoSuchSiteformException if a siteform with the primary key could not be found
	*/
	public Siteform findByPrimaryKey(long siteFormId)
		throws NoSuchSiteformException;

	/**
	* Returns the siteform with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param siteFormId the primary key of the siteform
	* @return the siteform, or <code>null</code> if a siteform with the primary key could not be found
	*/
	public Siteform fetchByPrimaryKey(long siteFormId);

	@Override
	public java.util.Map<java.io.Serializable, Siteform> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the siteforms.
	*
	* @return the siteforms
	*/
	public java.util.List<Siteform> findAll();

	/**
	* Returns a range of all the siteforms.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SiteformModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of siteforms
	* @param end the upper bound of the range of siteforms (not inclusive)
	* @return the range of siteforms
	*/
	public java.util.List<Siteform> findAll(int start, int end);

	/**
	* Returns an ordered range of all the siteforms.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SiteformModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of siteforms
	* @param end the upper bound of the range of siteforms (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of siteforms
	*/
	public java.util.List<Siteform> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Siteform> orderByComparator);

	/**
	* Returns an ordered range of all the siteforms.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SiteformModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of siteforms
	* @param end the upper bound of the range of siteforms (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of siteforms
	*/
	public java.util.List<Siteform> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Siteform> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the siteforms from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of siteforms.
	*
	* @return the number of siteforms
	*/
	public int countAll();
}