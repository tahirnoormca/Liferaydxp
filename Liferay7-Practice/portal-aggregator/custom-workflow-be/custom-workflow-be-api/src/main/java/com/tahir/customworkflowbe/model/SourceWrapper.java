/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.tahir.customworkflowbe.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Source}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Source
 * @generated
 */
@ProviderType
public class SourceWrapper implements Source, ModelWrapper<Source> {
	public SourceWrapper(Source source) {
		_source = source;
	}

	@Override
	public Class<?> getModelClass() {
		return Source.class;
	}

	@Override
	public String getModelClassName() {
		return Source.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("sourceId", getSourceId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("title", getTitle());
		attributes.put("source", getSource());
		attributes.put("columnName", getColumnName());
		attributes.put("status", getStatus());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long sourceId = (Long)attributes.get("sourceId");

		if (sourceId != null) {
			setSourceId(sourceId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String title = (String)attributes.get("title");

		if (title != null) {
			setTitle(title);
		}

		String source = (String)attributes.get("source");

		if (source != null) {
			setSource(source);
		}

		String columnName = (String)attributes.get("columnName");

		if (columnName != null) {
			setColumnName(columnName);
		}

		Boolean status = (Boolean)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}
	}

	@Override
	public Source toEscapedModel() {
		return new SourceWrapper(_source.toEscapedModel());
	}

	@Override
	public Source toUnescapedModel() {
		return new SourceWrapper(_source.toUnescapedModel());
	}

	/**
	* Returns the status of this source.
	*
	* @return the status of this source
	*/
	@Override
	public boolean getStatus() {
		return _source.getStatus();
	}

	@Override
	public boolean isCachedModel() {
		return _source.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _source.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _source.isNew();
	}

	/**
	* Returns <code>true</code> if this source is status.
	*
	* @return <code>true</code> if this source is status; <code>false</code> otherwise
	*/
	@Override
	public boolean isStatus() {
		return _source.isStatus();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _source.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<Source> toCacheModel() {
		return _source.toCacheModel();
	}

	@Override
	public int compareTo(Source source) {
		return _source.compareTo(source);
	}

	@Override
	public int hashCode() {
		return _source.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _source.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new SourceWrapper((Source)_source.clone());
	}

	/**
	* Returns the column name of this source.
	*
	* @return the column name of this source
	*/
	@Override
	public java.lang.String getColumnName() {
		return _source.getColumnName();
	}

	/**
	* Returns the source of this source.
	*
	* @return the source of this source
	*/
	@Override
	public java.lang.String getSource() {
		return _source.getSource();
	}

	/**
	* Returns the title of this source.
	*
	* @return the title of this source
	*/
	@Override
	public java.lang.String getTitle() {
		return _source.getTitle();
	}

	/**
	* Returns the user name of this source.
	*
	* @return the user name of this source
	*/
	@Override
	public java.lang.String getUserName() {
		return _source.getUserName();
	}

	/**
	* Returns the user uuid of this source.
	*
	* @return the user uuid of this source
	*/
	@Override
	public java.lang.String getUserUuid() {
		return _source.getUserUuid();
	}

	@Override
	public java.lang.String toString() {
		return _source.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _source.toXmlString();
	}

	/**
	* Returns the create date of this source.
	*
	* @return the create date of this source
	*/
	@Override
	public Date getCreateDate() {
		return _source.getCreateDate();
	}

	/**
	* Returns the modified date of this source.
	*
	* @return the modified date of this source
	*/
	@Override
	public Date getModifiedDate() {
		return _source.getModifiedDate();
	}

	/**
	* Returns the company ID of this source.
	*
	* @return the company ID of this source
	*/
	@Override
	public long getCompanyId() {
		return _source.getCompanyId();
	}

	/**
	* Returns the group ID of this source.
	*
	* @return the group ID of this source
	*/
	@Override
	public long getGroupId() {
		return _source.getGroupId();
	}

	/**
	* Returns the primary key of this source.
	*
	* @return the primary key of this source
	*/
	@Override
	public long getPrimaryKey() {
		return _source.getPrimaryKey();
	}

	/**
	* Returns the source ID of this source.
	*
	* @return the source ID of this source
	*/
	@Override
	public long getSourceId() {
		return _source.getSourceId();
	}

	/**
	* Returns the user ID of this source.
	*
	* @return the user ID of this source
	*/
	@Override
	public long getUserId() {
		return _source.getUserId();
	}

	@Override
	public void persist() {
		_source.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_source.setCachedModel(cachedModel);
	}

	/**
	* Sets the column name of this source.
	*
	* @param columnName the column name of this source
	*/
	@Override
	public void setColumnName(java.lang.String columnName) {
		_source.setColumnName(columnName);
	}

	/**
	* Sets the company ID of this source.
	*
	* @param companyId the company ID of this source
	*/
	@Override
	public void setCompanyId(long companyId) {
		_source.setCompanyId(companyId);
	}

	/**
	* Sets the create date of this source.
	*
	* @param createDate the create date of this source
	*/
	@Override
	public void setCreateDate(Date createDate) {
		_source.setCreateDate(createDate);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_source.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_source.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_source.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the group ID of this source.
	*
	* @param groupId the group ID of this source
	*/
	@Override
	public void setGroupId(long groupId) {
		_source.setGroupId(groupId);
	}

	/**
	* Sets the modified date of this source.
	*
	* @param modifiedDate the modified date of this source
	*/
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_source.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_source.setNew(n);
	}

	/**
	* Sets the primary key of this source.
	*
	* @param primaryKey the primary key of this source
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_source.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_source.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the source of this source.
	*
	* @param source the source of this source
	*/
	@Override
	public void setSource(java.lang.String source) {
		_source.setSource(source);
	}

	/**
	* Sets the source ID of this source.
	*
	* @param sourceId the source ID of this source
	*/
	@Override
	public void setSourceId(long sourceId) {
		_source.setSourceId(sourceId);
	}

	/**
	* Sets whether this source is status.
	*
	* @param status the status of this source
	*/
	@Override
	public void setStatus(boolean status) {
		_source.setStatus(status);
	}

	/**
	* Sets the title of this source.
	*
	* @param title the title of this source
	*/
	@Override
	public void setTitle(java.lang.String title) {
		_source.setTitle(title);
	}

	/**
	* Sets the user ID of this source.
	*
	* @param userId the user ID of this source
	*/
	@Override
	public void setUserId(long userId) {
		_source.setUserId(userId);
	}

	/**
	* Sets the user name of this source.
	*
	* @param userName the user name of this source
	*/
	@Override
	public void setUserName(java.lang.String userName) {
		_source.setUserName(userName);
	}

	/**
	* Sets the user uuid of this source.
	*
	* @param userUuid the user uuid of this source
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_source.setUserUuid(userUuid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof SourceWrapper)) {
			return false;
		}

		SourceWrapper sourceWrapper = (SourceWrapper)obj;

		if (Objects.equals(_source, sourceWrapper._source)) {
			return true;
		}

		return false;
	}

	@Override
	public Source getWrappedModel() {
		return _source;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _source.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _source.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_source.resetOriginalValues();
	}

	private final Source _source;
}