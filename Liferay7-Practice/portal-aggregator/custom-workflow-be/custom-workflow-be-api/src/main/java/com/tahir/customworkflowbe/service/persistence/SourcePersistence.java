/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.tahir.customworkflowbe.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import com.tahir.customworkflowbe.exception.NoSuchSourceException;
import com.tahir.customworkflowbe.model.Source;

/**
 * The persistence interface for the source service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see com.tahir.customworkflowbe.service.persistence.impl.SourcePersistenceImpl
 * @see SourceUtil
 * @generated
 */
@ProviderType
public interface SourcePersistence extends BasePersistence<Source> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link SourceUtil} to access the source persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the sources where userId = &#63;.
	*
	* @param userId the user ID
	* @return the matching sources
	*/
	public java.util.List<Source> findByuserId(long userId);

	/**
	* Returns a range of all the sources where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of sources
	* @param end the upper bound of the range of sources (not inclusive)
	* @return the range of matching sources
	*/
	public java.util.List<Source> findByuserId(long userId, int start, int end);

	/**
	* Returns an ordered range of all the sources where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of sources
	* @param end the upper bound of the range of sources (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching sources
	*/
	public java.util.List<Source> findByuserId(long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Source> orderByComparator);

	/**
	* Returns an ordered range of all the sources where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of sources
	* @param end the upper bound of the range of sources (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching sources
	*/
	public java.util.List<Source> findByuserId(long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Source> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first source in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching source
	* @throws NoSuchSourceException if a matching source could not be found
	*/
	public Source findByuserId_First(long userId,
		com.liferay.portal.kernel.util.OrderByComparator<Source> orderByComparator)
		throws NoSuchSourceException;

	/**
	* Returns the first source in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching source, or <code>null</code> if a matching source could not be found
	*/
	public Source fetchByuserId_First(long userId,
		com.liferay.portal.kernel.util.OrderByComparator<Source> orderByComparator);

	/**
	* Returns the last source in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching source
	* @throws NoSuchSourceException if a matching source could not be found
	*/
	public Source findByuserId_Last(long userId,
		com.liferay.portal.kernel.util.OrderByComparator<Source> orderByComparator)
		throws NoSuchSourceException;

	/**
	* Returns the last source in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching source, or <code>null</code> if a matching source could not be found
	*/
	public Source fetchByuserId_Last(long userId,
		com.liferay.portal.kernel.util.OrderByComparator<Source> orderByComparator);

	/**
	* Returns the sources before and after the current source in the ordered set where userId = &#63;.
	*
	* @param sourceId the primary key of the current source
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next source
	* @throws NoSuchSourceException if a source with the primary key could not be found
	*/
	public Source[] findByuserId_PrevAndNext(long sourceId, long userId,
		com.liferay.portal.kernel.util.OrderByComparator<Source> orderByComparator)
		throws NoSuchSourceException;

	/**
	* Removes all the sources where userId = &#63; from the database.
	*
	* @param userId the user ID
	*/
	public void removeByuserId(long userId);

	/**
	* Returns the number of sources where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching sources
	*/
	public int countByuserId(long userId);

	/**
	* Returns all the sources where title = &#63;.
	*
	* @param title the title
	* @return the matching sources
	*/
	public java.util.List<Source> findBytitle(java.lang.String title);

	/**
	* Returns a range of all the sources where title = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param title the title
	* @param start the lower bound of the range of sources
	* @param end the upper bound of the range of sources (not inclusive)
	* @return the range of matching sources
	*/
	public java.util.List<Source> findBytitle(java.lang.String title,
		int start, int end);

	/**
	* Returns an ordered range of all the sources where title = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param title the title
	* @param start the lower bound of the range of sources
	* @param end the upper bound of the range of sources (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching sources
	*/
	public java.util.List<Source> findBytitle(java.lang.String title,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Source> orderByComparator);

	/**
	* Returns an ordered range of all the sources where title = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param title the title
	* @param start the lower bound of the range of sources
	* @param end the upper bound of the range of sources (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching sources
	*/
	public java.util.List<Source> findBytitle(java.lang.String title,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Source> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first source in the ordered set where title = &#63;.
	*
	* @param title the title
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching source
	* @throws NoSuchSourceException if a matching source could not be found
	*/
	public Source findBytitle_First(java.lang.String title,
		com.liferay.portal.kernel.util.OrderByComparator<Source> orderByComparator)
		throws NoSuchSourceException;

	/**
	* Returns the first source in the ordered set where title = &#63;.
	*
	* @param title the title
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching source, or <code>null</code> if a matching source could not be found
	*/
	public Source fetchBytitle_First(java.lang.String title,
		com.liferay.portal.kernel.util.OrderByComparator<Source> orderByComparator);

	/**
	* Returns the last source in the ordered set where title = &#63;.
	*
	* @param title the title
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching source
	* @throws NoSuchSourceException if a matching source could not be found
	*/
	public Source findBytitle_Last(java.lang.String title,
		com.liferay.portal.kernel.util.OrderByComparator<Source> orderByComparator)
		throws NoSuchSourceException;

	/**
	* Returns the last source in the ordered set where title = &#63;.
	*
	* @param title the title
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching source, or <code>null</code> if a matching source could not be found
	*/
	public Source fetchBytitle_Last(java.lang.String title,
		com.liferay.portal.kernel.util.OrderByComparator<Source> orderByComparator);

	/**
	* Returns the sources before and after the current source in the ordered set where title = &#63;.
	*
	* @param sourceId the primary key of the current source
	* @param title the title
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next source
	* @throws NoSuchSourceException if a source with the primary key could not be found
	*/
	public Source[] findBytitle_PrevAndNext(long sourceId,
		java.lang.String title,
		com.liferay.portal.kernel.util.OrderByComparator<Source> orderByComparator)
		throws NoSuchSourceException;

	/**
	* Removes all the sources where title = &#63; from the database.
	*
	* @param title the title
	*/
	public void removeBytitle(java.lang.String title);

	/**
	* Returns the number of sources where title = &#63;.
	*
	* @param title the title
	* @return the number of matching sources
	*/
	public int countBytitle(java.lang.String title);

	/**
	* Returns all the sources where status = &#63;.
	*
	* @param status the status
	* @return the matching sources
	*/
	public java.util.List<Source> findBystatus(boolean status);

	/**
	* Returns a range of all the sources where status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param status the status
	* @param start the lower bound of the range of sources
	* @param end the upper bound of the range of sources (not inclusive)
	* @return the range of matching sources
	*/
	public java.util.List<Source> findBystatus(boolean status, int start,
		int end);

	/**
	* Returns an ordered range of all the sources where status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param status the status
	* @param start the lower bound of the range of sources
	* @param end the upper bound of the range of sources (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching sources
	*/
	public java.util.List<Source> findBystatus(boolean status, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Source> orderByComparator);

	/**
	* Returns an ordered range of all the sources where status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param status the status
	* @param start the lower bound of the range of sources
	* @param end the upper bound of the range of sources (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching sources
	*/
	public java.util.List<Source> findBystatus(boolean status, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Source> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first source in the ordered set where status = &#63;.
	*
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching source
	* @throws NoSuchSourceException if a matching source could not be found
	*/
	public Source findBystatus_First(boolean status,
		com.liferay.portal.kernel.util.OrderByComparator<Source> orderByComparator)
		throws NoSuchSourceException;

	/**
	* Returns the first source in the ordered set where status = &#63;.
	*
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching source, or <code>null</code> if a matching source could not be found
	*/
	public Source fetchBystatus_First(boolean status,
		com.liferay.portal.kernel.util.OrderByComparator<Source> orderByComparator);

	/**
	* Returns the last source in the ordered set where status = &#63;.
	*
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching source
	* @throws NoSuchSourceException if a matching source could not be found
	*/
	public Source findBystatus_Last(boolean status,
		com.liferay.portal.kernel.util.OrderByComparator<Source> orderByComparator)
		throws NoSuchSourceException;

	/**
	* Returns the last source in the ordered set where status = &#63;.
	*
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching source, or <code>null</code> if a matching source could not be found
	*/
	public Source fetchBystatus_Last(boolean status,
		com.liferay.portal.kernel.util.OrderByComparator<Source> orderByComparator);

	/**
	* Returns the sources before and after the current source in the ordered set where status = &#63;.
	*
	* @param sourceId the primary key of the current source
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next source
	* @throws NoSuchSourceException if a source with the primary key could not be found
	*/
	public Source[] findBystatus_PrevAndNext(long sourceId, boolean status,
		com.liferay.portal.kernel.util.OrderByComparator<Source> orderByComparator)
		throws NoSuchSourceException;

	/**
	* Removes all the sources where status = &#63; from the database.
	*
	* @param status the status
	*/
	public void removeBystatus(boolean status);

	/**
	* Returns the number of sources where status = &#63;.
	*
	* @param status the status
	* @return the number of matching sources
	*/
	public int countBystatus(boolean status);

	/**
	* Caches the source in the entity cache if it is enabled.
	*
	* @param source the source
	*/
	public void cacheResult(Source source);

	/**
	* Caches the sources in the entity cache if it is enabled.
	*
	* @param sources the sources
	*/
	public void cacheResult(java.util.List<Source> sources);

	/**
	* Creates a new source with the primary key. Does not add the source to the database.
	*
	* @param sourceId the primary key for the new source
	* @return the new source
	*/
	public Source create(long sourceId);

	/**
	* Removes the source with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param sourceId the primary key of the source
	* @return the source that was removed
	* @throws NoSuchSourceException if a source with the primary key could not be found
	*/
	public Source remove(long sourceId) throws NoSuchSourceException;

	public Source updateImpl(Source source);

	/**
	* Returns the source with the primary key or throws a {@link NoSuchSourceException} if it could not be found.
	*
	* @param sourceId the primary key of the source
	* @return the source
	* @throws NoSuchSourceException if a source with the primary key could not be found
	*/
	public Source findByPrimaryKey(long sourceId) throws NoSuchSourceException;

	/**
	* Returns the source with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param sourceId the primary key of the source
	* @return the source, or <code>null</code> if a source with the primary key could not be found
	*/
	public Source fetchByPrimaryKey(long sourceId);

	@Override
	public java.util.Map<java.io.Serializable, Source> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the sources.
	*
	* @return the sources
	*/
	public java.util.List<Source> findAll();

	/**
	* Returns a range of all the sources.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of sources
	* @param end the upper bound of the range of sources (not inclusive)
	* @return the range of sources
	*/
	public java.util.List<Source> findAll(int start, int end);

	/**
	* Returns an ordered range of all the sources.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of sources
	* @param end the upper bound of the range of sources (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of sources
	*/
	public java.util.List<Source> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Source> orderByComparator);

	/**
	* Returns an ordered range of all the sources.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of sources
	* @param end the upper bound of the range of sources (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of sources
	*/
	public java.util.List<Source> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Source> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the sources from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of sources.
	*
	* @return the number of sources
	*/
	public int countAll();
}