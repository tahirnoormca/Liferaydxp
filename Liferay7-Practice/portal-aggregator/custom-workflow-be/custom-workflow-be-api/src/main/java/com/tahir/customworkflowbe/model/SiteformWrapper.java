/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.tahir.customworkflowbe.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Siteform}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Siteform
 * @generated
 */
@ProviderType
public class SiteformWrapper implements Siteform, ModelWrapper<Siteform> {
	public SiteformWrapper(Siteform siteform) {
		_siteform = siteform;
	}

	@Override
	public Class<?> getModelClass() {
		return Siteform.class;
	}

	@Override
	public String getModelClassName() {
		return Siteform.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("siteFormId", getSiteFormId());
		attributes.put("siteId", getSiteId());
		attributes.put("siteName", getSiteName());
		attributes.put("screen1", getScreen1());
		attributes.put("screen2", getScreen2());
		attributes.put("screen3", getScreen3());
		attributes.put("screen4", getScreen4());
		attributes.put("screen5", getScreen5());
		attributes.put("screen6", getScreen6());
		attributes.put("screen7", getScreen7());
		attributes.put("screen8", getScreen8());
		attributes.put("screen9", getScreen9());
		attributes.put("screen10", getScreen10());
		attributes.put("createdBy", getCreatedBy());
		attributes.put("modifiedBy", getModifiedBy());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long siteFormId = (Long)attributes.get("siteFormId");

		if (siteFormId != null) {
			setSiteFormId(siteFormId);
		}

		Long siteId = (Long)attributes.get("siteId");

		if (siteId != null) {
			setSiteId(siteId);
		}

		String siteName = (String)attributes.get("siteName");

		if (siteName != null) {
			setSiteName(siteName);
		}

		String screen1 = (String)attributes.get("screen1");

		if (screen1 != null) {
			setScreen1(screen1);
		}

		String screen2 = (String)attributes.get("screen2");

		if (screen2 != null) {
			setScreen2(screen2);
		}

		String screen3 = (String)attributes.get("screen3");

		if (screen3 != null) {
			setScreen3(screen3);
		}

		String screen4 = (String)attributes.get("screen4");

		if (screen4 != null) {
			setScreen4(screen4);
		}

		String screen5 = (String)attributes.get("screen5");

		if (screen5 != null) {
			setScreen5(screen5);
		}

		String screen6 = (String)attributes.get("screen6");

		if (screen6 != null) {
			setScreen6(screen6);
		}

		String screen7 = (String)attributes.get("screen7");

		if (screen7 != null) {
			setScreen7(screen7);
		}

		String screen8 = (String)attributes.get("screen8");

		if (screen8 != null) {
			setScreen8(screen8);
		}

		String screen9 = (String)attributes.get("screen9");

		if (screen9 != null) {
			setScreen9(screen9);
		}

		String screen10 = (String)attributes.get("screen10");

		if (screen10 != null) {
			setScreen10(screen10);
		}

		Long createdBy = (Long)attributes.get("createdBy");

		if (createdBy != null) {
			setCreatedBy(createdBy);
		}

		Long modifiedBy = (Long)attributes.get("modifiedBy");

		if (modifiedBy != null) {
			setModifiedBy(modifiedBy);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}
	}

	@Override
	public Siteform toEscapedModel() {
		return new SiteformWrapper(_siteform.toEscapedModel());
	}

	@Override
	public Siteform toUnescapedModel() {
		return new SiteformWrapper(_siteform.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _siteform.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _siteform.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _siteform.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _siteform.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<Siteform> toCacheModel() {
		return _siteform.toCacheModel();
	}

	@Override
	public int compareTo(Siteform siteform) {
		return _siteform.compareTo(siteform);
	}

	@Override
	public int hashCode() {
		return _siteform.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _siteform.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new SiteformWrapper((Siteform)_siteform.clone());
	}

	/**
	* Returns the screen1 of this siteform.
	*
	* @return the screen1 of this siteform
	*/
	@Override
	public java.lang.String getScreen1() {
		return _siteform.getScreen1();
	}

	/**
	* Returns the screen10 of this siteform.
	*
	* @return the screen10 of this siteform
	*/
	@Override
	public java.lang.String getScreen10() {
		return _siteform.getScreen10();
	}

	/**
	* Returns the screen2 of this siteform.
	*
	* @return the screen2 of this siteform
	*/
	@Override
	public java.lang.String getScreen2() {
		return _siteform.getScreen2();
	}

	/**
	* Returns the screen3 of this siteform.
	*
	* @return the screen3 of this siteform
	*/
	@Override
	public java.lang.String getScreen3() {
		return _siteform.getScreen3();
	}

	/**
	* Returns the screen4 of this siteform.
	*
	* @return the screen4 of this siteform
	*/
	@Override
	public java.lang.String getScreen4() {
		return _siteform.getScreen4();
	}

	/**
	* Returns the screen5 of this siteform.
	*
	* @return the screen5 of this siteform
	*/
	@Override
	public java.lang.String getScreen5() {
		return _siteform.getScreen5();
	}

	/**
	* Returns the screen6 of this siteform.
	*
	* @return the screen6 of this siteform
	*/
	@Override
	public java.lang.String getScreen6() {
		return _siteform.getScreen6();
	}

	/**
	* Returns the screen7 of this siteform.
	*
	* @return the screen7 of this siteform
	*/
	@Override
	public java.lang.String getScreen7() {
		return _siteform.getScreen7();
	}

	/**
	* Returns the screen8 of this siteform.
	*
	* @return the screen8 of this siteform
	*/
	@Override
	public java.lang.String getScreen8() {
		return _siteform.getScreen8();
	}

	/**
	* Returns the screen9 of this siteform.
	*
	* @return the screen9 of this siteform
	*/
	@Override
	public java.lang.String getScreen9() {
		return _siteform.getScreen9();
	}

	/**
	* Returns the site name of this siteform.
	*
	* @return the site name of this siteform
	*/
	@Override
	public java.lang.String getSiteName() {
		return _siteform.getSiteName();
	}

	@Override
	public java.lang.String toString() {
		return _siteform.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _siteform.toXmlString();
	}

	/**
	* Returns the create date of this siteform.
	*
	* @return the create date of this siteform
	*/
	@Override
	public Date getCreateDate() {
		return _siteform.getCreateDate();
	}

	/**
	* Returns the modified date of this siteform.
	*
	* @return the modified date of this siteform
	*/
	@Override
	public Date getModifiedDate() {
		return _siteform.getModifiedDate();
	}

	/**
	* Returns the created by of this siteform.
	*
	* @return the created by of this siteform
	*/
	@Override
	public long getCreatedBy() {
		return _siteform.getCreatedBy();
	}

	/**
	* Returns the modified by of this siteform.
	*
	* @return the modified by of this siteform
	*/
	@Override
	public long getModifiedBy() {
		return _siteform.getModifiedBy();
	}

	/**
	* Returns the primary key of this siteform.
	*
	* @return the primary key of this siteform
	*/
	@Override
	public long getPrimaryKey() {
		return _siteform.getPrimaryKey();
	}

	/**
	* Returns the site form ID of this siteform.
	*
	* @return the site form ID of this siteform
	*/
	@Override
	public long getSiteFormId() {
		return _siteform.getSiteFormId();
	}

	/**
	* Returns the site ID of this siteform.
	*
	* @return the site ID of this siteform
	*/
	@Override
	public long getSiteId() {
		return _siteform.getSiteId();
	}

	@Override
	public void persist() {
		_siteform.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_siteform.setCachedModel(cachedModel);
	}

	/**
	* Sets the create date of this siteform.
	*
	* @param createDate the create date of this siteform
	*/
	@Override
	public void setCreateDate(Date createDate) {
		_siteform.setCreateDate(createDate);
	}

	/**
	* Sets the created by of this siteform.
	*
	* @param createdBy the created by of this siteform
	*/
	@Override
	public void setCreatedBy(long createdBy) {
		_siteform.setCreatedBy(createdBy);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_siteform.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_siteform.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_siteform.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the modified by of this siteform.
	*
	* @param modifiedBy the modified by of this siteform
	*/
	@Override
	public void setModifiedBy(long modifiedBy) {
		_siteform.setModifiedBy(modifiedBy);
	}

	/**
	* Sets the modified date of this siteform.
	*
	* @param modifiedDate the modified date of this siteform
	*/
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_siteform.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_siteform.setNew(n);
	}

	/**
	* Sets the primary key of this siteform.
	*
	* @param primaryKey the primary key of this siteform
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_siteform.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_siteform.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the screen1 of this siteform.
	*
	* @param screen1 the screen1 of this siteform
	*/
	@Override
	public void setScreen1(java.lang.String screen1) {
		_siteform.setScreen1(screen1);
	}

	/**
	* Sets the screen10 of this siteform.
	*
	* @param screen10 the screen10 of this siteform
	*/
	@Override
	public void setScreen10(java.lang.String screen10) {
		_siteform.setScreen10(screen10);
	}

	/**
	* Sets the screen2 of this siteform.
	*
	* @param screen2 the screen2 of this siteform
	*/
	@Override
	public void setScreen2(java.lang.String screen2) {
		_siteform.setScreen2(screen2);
	}

	/**
	* Sets the screen3 of this siteform.
	*
	* @param screen3 the screen3 of this siteform
	*/
	@Override
	public void setScreen3(java.lang.String screen3) {
		_siteform.setScreen3(screen3);
	}

	/**
	* Sets the screen4 of this siteform.
	*
	* @param screen4 the screen4 of this siteform
	*/
	@Override
	public void setScreen4(java.lang.String screen4) {
		_siteform.setScreen4(screen4);
	}

	/**
	* Sets the screen5 of this siteform.
	*
	* @param screen5 the screen5 of this siteform
	*/
	@Override
	public void setScreen5(java.lang.String screen5) {
		_siteform.setScreen5(screen5);
	}

	/**
	* Sets the screen6 of this siteform.
	*
	* @param screen6 the screen6 of this siteform
	*/
	@Override
	public void setScreen6(java.lang.String screen6) {
		_siteform.setScreen6(screen6);
	}

	/**
	* Sets the screen7 of this siteform.
	*
	* @param screen7 the screen7 of this siteform
	*/
	@Override
	public void setScreen7(java.lang.String screen7) {
		_siteform.setScreen7(screen7);
	}

	/**
	* Sets the screen8 of this siteform.
	*
	* @param screen8 the screen8 of this siteform
	*/
	@Override
	public void setScreen8(java.lang.String screen8) {
		_siteform.setScreen8(screen8);
	}

	/**
	* Sets the screen9 of this siteform.
	*
	* @param screen9 the screen9 of this siteform
	*/
	@Override
	public void setScreen9(java.lang.String screen9) {
		_siteform.setScreen9(screen9);
	}

	/**
	* Sets the site form ID of this siteform.
	*
	* @param siteFormId the site form ID of this siteform
	*/
	@Override
	public void setSiteFormId(long siteFormId) {
		_siteform.setSiteFormId(siteFormId);
	}

	/**
	* Sets the site ID of this siteform.
	*
	* @param siteId the site ID of this siteform
	*/
	@Override
	public void setSiteId(long siteId) {
		_siteform.setSiteId(siteId);
	}

	/**
	* Sets the site name of this siteform.
	*
	* @param siteName the site name of this siteform
	*/
	@Override
	public void setSiteName(java.lang.String siteName) {
		_siteform.setSiteName(siteName);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof SiteformWrapper)) {
			return false;
		}

		SiteformWrapper siteformWrapper = (SiteformWrapper)obj;

		if (Objects.equals(_siteform, siteformWrapper._siteform)) {
			return true;
		}

		return false;
	}

	@Override
	public Siteform getWrappedModel() {
		return _siteform;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _siteform.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _siteform.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_siteform.resetOriginalValues();
	}

	private final Siteform _siteform;
}