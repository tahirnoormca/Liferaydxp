/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.tahir.customworkflowbe.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.kernel.model.BaseModel;
import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.model.GroupedModel;
import com.liferay.portal.kernel.model.ShardedModel;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;

/**
 * The base model interface for the Source service. Represents a row in the &quot;custwf_Source&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation {@link com.tahir.customworkflowbe.model.impl.SourceModelImpl} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link com.tahir.customworkflowbe.model.impl.SourceImpl}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Source
 * @see com.tahir.customworkflowbe.model.impl.SourceImpl
 * @see com.tahir.customworkflowbe.model.impl.SourceModelImpl
 * @generated
 */
@ProviderType
public interface SourceModel extends BaseModel<Source>, GroupedModel,
	ShardedModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a source model instance should use the {@link Source} interface instead.
	 */

	/**
	 * Returns the primary key of this source.
	 *
	 * @return the primary key of this source
	 */
	public long getPrimaryKey();

	/**
	 * Sets the primary key of this source.
	 *
	 * @param primaryKey the primary key of this source
	 */
	public void setPrimaryKey(long primaryKey);

	/**
	 * Returns the source ID of this source.
	 *
	 * @return the source ID of this source
	 */
	public long getSourceId();

	/**
	 * Sets the source ID of this source.
	 *
	 * @param sourceId the source ID of this source
	 */
	public void setSourceId(long sourceId);

	/**
	 * Returns the group ID of this source.
	 *
	 * @return the group ID of this source
	 */
	@Override
	public long getGroupId();

	/**
	 * Sets the group ID of this source.
	 *
	 * @param groupId the group ID of this source
	 */
	@Override
	public void setGroupId(long groupId);

	/**
	 * Returns the company ID of this source.
	 *
	 * @return the company ID of this source
	 */
	@Override
	public long getCompanyId();

	/**
	 * Sets the company ID of this source.
	 *
	 * @param companyId the company ID of this source
	 */
	@Override
	public void setCompanyId(long companyId);

	/**
	 * Returns the user ID of this source.
	 *
	 * @return the user ID of this source
	 */
	@Override
	public long getUserId();

	/**
	 * Sets the user ID of this source.
	 *
	 * @param userId the user ID of this source
	 */
	@Override
	public void setUserId(long userId);

	/**
	 * Returns the user uuid of this source.
	 *
	 * @return the user uuid of this source
	 */
	@Override
	public String getUserUuid();

	/**
	 * Sets the user uuid of this source.
	 *
	 * @param userUuid the user uuid of this source
	 */
	@Override
	public void setUserUuid(String userUuid);

	/**
	 * Returns the user name of this source.
	 *
	 * @return the user name of this source
	 */
	@AutoEscape
	@Override
	public String getUserName();

	/**
	 * Sets the user name of this source.
	 *
	 * @param userName the user name of this source
	 */
	@Override
	public void setUserName(String userName);

	/**
	 * Returns the create date of this source.
	 *
	 * @return the create date of this source
	 */
	@Override
	public Date getCreateDate();

	/**
	 * Sets the create date of this source.
	 *
	 * @param createDate the create date of this source
	 */
	@Override
	public void setCreateDate(Date createDate);

	/**
	 * Returns the modified date of this source.
	 *
	 * @return the modified date of this source
	 */
	@Override
	public Date getModifiedDate();

	/**
	 * Sets the modified date of this source.
	 *
	 * @param modifiedDate the modified date of this source
	 */
	@Override
	public void setModifiedDate(Date modifiedDate);

	/**
	 * Returns the title of this source.
	 *
	 * @return the title of this source
	 */
	@AutoEscape
	public String getTitle();

	/**
	 * Sets the title of this source.
	 *
	 * @param title the title of this source
	 */
	public void setTitle(String title);

	/**
	 * Returns the source of this source.
	 *
	 * @return the source of this source
	 */
	@AutoEscape
	public String getSource();

	/**
	 * Sets the source of this source.
	 *
	 * @param source the source of this source
	 */
	public void setSource(String source);

	/**
	 * Returns the column name of this source.
	 *
	 * @return the column name of this source
	 */
	@AutoEscape
	public String getColumnName();

	/**
	 * Sets the column name of this source.
	 *
	 * @param columnName the column name of this source
	 */
	public void setColumnName(String columnName);

	/**
	 * Returns the status of this source.
	 *
	 * @return the status of this source
	 */
	public boolean getStatus();

	/**
	 * Returns <code>true</code> if this source is status.
	 *
	 * @return <code>true</code> if this source is status; <code>false</code> otherwise
	 */
	public boolean isStatus();

	/**
	 * Sets whether this source is status.
	 *
	 * @param status the status of this source
	 */
	public void setStatus(boolean status);

	@Override
	public boolean isNew();

	@Override
	public void setNew(boolean n);

	@Override
	public boolean isCachedModel();

	@Override
	public void setCachedModel(boolean cachedModel);

	@Override
	public boolean isEscapedModel();

	@Override
	public Serializable getPrimaryKeyObj();

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj);

	@Override
	public ExpandoBridge getExpandoBridge();

	@Override
	public void setExpandoBridgeAttributes(BaseModel<?> baseModel);

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge);

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext);

	@Override
	public Object clone();

	@Override
	public int compareTo(Source source);

	@Override
	public int hashCode();

	@Override
	public CacheModel<Source> toCacheModel();

	@Override
	public Source toEscapedModel();

	@Override
	public Source toUnescapedModel();

	@Override
	public String toString();

	@Override
	public String toXmlString();
}