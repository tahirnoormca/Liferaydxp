/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.tahir.customworkflowbe.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.tahir.customworkflowbe.model.Siteform;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the siteform service. This utility wraps {@link com.tahir.customworkflowbe.service.persistence.impl.SiteformPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see SiteformPersistence
 * @see com.tahir.customworkflowbe.service.persistence.impl.SiteformPersistenceImpl
 * @generated
 */
@ProviderType
public class SiteformUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Siteform siteform) {
		getPersistence().clearCache(siteform);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Siteform> findWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Siteform> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Siteform> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Siteform> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Siteform update(Siteform siteform) {
		return getPersistence().update(siteform);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Siteform update(Siteform siteform,
		ServiceContext serviceContext) {
		return getPersistence().update(siteform, serviceContext);
	}

	/**
	* Caches the siteform in the entity cache if it is enabled.
	*
	* @param siteform the siteform
	*/
	public static void cacheResult(Siteform siteform) {
		getPersistence().cacheResult(siteform);
	}

	/**
	* Caches the siteforms in the entity cache if it is enabled.
	*
	* @param siteforms the siteforms
	*/
	public static void cacheResult(List<Siteform> siteforms) {
		getPersistence().cacheResult(siteforms);
	}

	/**
	* Creates a new siteform with the primary key. Does not add the siteform to the database.
	*
	* @param siteFormId the primary key for the new siteform
	* @return the new siteform
	*/
	public static Siteform create(long siteFormId) {
		return getPersistence().create(siteFormId);
	}

	/**
	* Removes the siteform with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param siteFormId the primary key of the siteform
	* @return the siteform that was removed
	* @throws NoSuchSiteformException if a siteform with the primary key could not be found
	*/
	public static Siteform remove(long siteFormId)
		throws com.tahir.customworkflowbe.exception.NoSuchSiteformException {
		return getPersistence().remove(siteFormId);
	}

	public static Siteform updateImpl(Siteform siteform) {
		return getPersistence().updateImpl(siteform);
	}

	/**
	* Returns the siteform with the primary key or throws a {@link NoSuchSiteformException} if it could not be found.
	*
	* @param siteFormId the primary key of the siteform
	* @return the siteform
	* @throws NoSuchSiteformException if a siteform with the primary key could not be found
	*/
	public static Siteform findByPrimaryKey(long siteFormId)
		throws com.tahir.customworkflowbe.exception.NoSuchSiteformException {
		return getPersistence().findByPrimaryKey(siteFormId);
	}

	/**
	* Returns the siteform with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param siteFormId the primary key of the siteform
	* @return the siteform, or <code>null</code> if a siteform with the primary key could not be found
	*/
	public static Siteform fetchByPrimaryKey(long siteFormId) {
		return getPersistence().fetchByPrimaryKey(siteFormId);
	}

	public static java.util.Map<java.io.Serializable, Siteform> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the siteforms.
	*
	* @return the siteforms
	*/
	public static List<Siteform> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the siteforms.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SiteformModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of siteforms
	* @param end the upper bound of the range of siteforms (not inclusive)
	* @return the range of siteforms
	*/
	public static List<Siteform> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the siteforms.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SiteformModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of siteforms
	* @param end the upper bound of the range of siteforms (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of siteforms
	*/
	public static List<Siteform> findAll(int start, int end,
		OrderByComparator<Siteform> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the siteforms.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SiteformModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of siteforms
	* @param end the upper bound of the range of siteforms (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of siteforms
	*/
	public static List<Siteform> findAll(int start, int end,
		OrderByComparator<Siteform> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the siteforms from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of siteforms.
	*
	* @return the number of siteforms
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static SiteformPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<SiteformPersistence, SiteformPersistence> _serviceTracker =
		ServiceTrackerFactory.open(SiteformPersistence.class);
}