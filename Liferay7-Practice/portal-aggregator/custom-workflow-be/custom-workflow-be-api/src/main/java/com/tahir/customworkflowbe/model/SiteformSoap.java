/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.tahir.customworkflowbe.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.tahir.customworkflowbe.service.http.SiteformServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see com.tahir.customworkflowbe.service.http.SiteformServiceSoap
 * @generated
 */
@ProviderType
public class SiteformSoap implements Serializable {
	public static SiteformSoap toSoapModel(Siteform model) {
		SiteformSoap soapModel = new SiteformSoap();

		soapModel.setSiteFormId(model.getSiteFormId());
		soapModel.setSiteId(model.getSiteId());
		soapModel.setSiteName(model.getSiteName());
		soapModel.setScreen1(model.getScreen1());
		soapModel.setScreen2(model.getScreen2());
		soapModel.setScreen3(model.getScreen3());
		soapModel.setScreen4(model.getScreen4());
		soapModel.setScreen5(model.getScreen5());
		soapModel.setScreen6(model.getScreen6());
		soapModel.setScreen7(model.getScreen7());
		soapModel.setScreen8(model.getScreen8());
		soapModel.setScreen9(model.getScreen9());
		soapModel.setScreen10(model.getScreen10());
		soapModel.setCreatedBy(model.getCreatedBy());
		soapModel.setModifiedBy(model.getModifiedBy());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());

		return soapModel;
	}

	public static SiteformSoap[] toSoapModels(Siteform[] models) {
		SiteformSoap[] soapModels = new SiteformSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static SiteformSoap[][] toSoapModels(Siteform[][] models) {
		SiteformSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new SiteformSoap[models.length][models[0].length];
		}
		else {
			soapModels = new SiteformSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static SiteformSoap[] toSoapModels(List<Siteform> models) {
		List<SiteformSoap> soapModels = new ArrayList<SiteformSoap>(models.size());

		for (Siteform model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new SiteformSoap[soapModels.size()]);
	}

	public SiteformSoap() {
	}

	public long getPrimaryKey() {
		return _siteFormId;
	}

	public void setPrimaryKey(long pk) {
		setSiteFormId(pk);
	}

	public long getSiteFormId() {
		return _siteFormId;
	}

	public void setSiteFormId(long siteFormId) {
		_siteFormId = siteFormId;
	}

	public long getSiteId() {
		return _siteId;
	}

	public void setSiteId(long siteId) {
		_siteId = siteId;
	}

	public String getSiteName() {
		return _siteName;
	}

	public void setSiteName(String siteName) {
		_siteName = siteName;
	}

	public String getScreen1() {
		return _screen1;
	}

	public void setScreen1(String screen1) {
		_screen1 = screen1;
	}

	public String getScreen2() {
		return _screen2;
	}

	public void setScreen2(String screen2) {
		_screen2 = screen2;
	}

	public String getScreen3() {
		return _screen3;
	}

	public void setScreen3(String screen3) {
		_screen3 = screen3;
	}

	public String getScreen4() {
		return _screen4;
	}

	public void setScreen4(String screen4) {
		_screen4 = screen4;
	}

	public String getScreen5() {
		return _screen5;
	}

	public void setScreen5(String screen5) {
		_screen5 = screen5;
	}

	public String getScreen6() {
		return _screen6;
	}

	public void setScreen6(String screen6) {
		_screen6 = screen6;
	}

	public String getScreen7() {
		return _screen7;
	}

	public void setScreen7(String screen7) {
		_screen7 = screen7;
	}

	public String getScreen8() {
		return _screen8;
	}

	public void setScreen8(String screen8) {
		_screen8 = screen8;
	}

	public String getScreen9() {
		return _screen9;
	}

	public void setScreen9(String screen9) {
		_screen9 = screen9;
	}

	public String getScreen10() {
		return _screen10;
	}

	public void setScreen10(String screen10) {
		_screen10 = screen10;
	}

	public long getCreatedBy() {
		return _createdBy;
	}

	public void setCreatedBy(long createdBy) {
		_createdBy = createdBy;
	}

	public long getModifiedBy() {
		return _modifiedBy;
	}

	public void setModifiedBy(long modifiedBy) {
		_modifiedBy = modifiedBy;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	private long _siteFormId;
	private long _siteId;
	private String _siteName;
	private String _screen1;
	private String _screen2;
	private String _screen3;
	private String _screen4;
	private String _screen5;
	private String _screen6;
	private String _screen7;
	private String _screen8;
	private String _screen9;
	private String _screen10;
	private long _createdBy;
	private long _modifiedBy;
	private Date _createDate;
	private Date _modifiedDate;
}