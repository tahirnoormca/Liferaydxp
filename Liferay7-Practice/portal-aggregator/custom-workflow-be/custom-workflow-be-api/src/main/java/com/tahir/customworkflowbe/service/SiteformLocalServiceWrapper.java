/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.tahir.customworkflowbe.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link SiteformLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see SiteformLocalService
 * @generated
 */
@ProviderType
public class SiteformLocalServiceWrapper implements SiteformLocalService,
	ServiceWrapper<SiteformLocalService> {
	public SiteformLocalServiceWrapper(
		SiteformLocalService siteformLocalService) {
		_siteformLocalService = siteformLocalService;
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _siteformLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _siteformLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _siteformLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _siteformLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _siteformLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Adds the siteform to the database. Also notifies the appropriate model listeners.
	*
	* @param siteform the siteform
	* @return the siteform that was added
	*/
	@Override
	public com.tahir.customworkflowbe.model.Siteform addSiteform(
		com.tahir.customworkflowbe.model.Siteform siteform) {
		return _siteformLocalService.addSiteform(siteform);
	}

	/**
	* Creates a new siteform with the primary key. Does not add the siteform to the database.
	*
	* @param siteFormId the primary key for the new siteform
	* @return the new siteform
	*/
	@Override
	public com.tahir.customworkflowbe.model.Siteform createSiteform(
		long siteFormId) {
		return _siteformLocalService.createSiteform(siteFormId);
	}

	/**
	* Deletes the siteform from the database. Also notifies the appropriate model listeners.
	*
	* @param siteform the siteform
	* @return the siteform that was removed
	*/
	@Override
	public com.tahir.customworkflowbe.model.Siteform deleteSiteform(
		com.tahir.customworkflowbe.model.Siteform siteform) {
		return _siteformLocalService.deleteSiteform(siteform);
	}

	/**
	* Deletes the siteform with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param siteFormId the primary key of the siteform
	* @return the siteform that was removed
	* @throws PortalException if a siteform with the primary key could not be found
	*/
	@Override
	public com.tahir.customworkflowbe.model.Siteform deleteSiteform(
		long siteFormId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _siteformLocalService.deleteSiteform(siteFormId);
	}

	@Override
	public com.tahir.customworkflowbe.model.Siteform fetchSiteform(
		long siteFormId) {
		return _siteformLocalService.fetchSiteform(siteFormId);
	}

	/**
	* Returns the siteform with the primary key.
	*
	* @param siteFormId the primary key of the siteform
	* @return the siteform
	* @throws PortalException if a siteform with the primary key could not be found
	*/
	@Override
	public com.tahir.customworkflowbe.model.Siteform getSiteform(
		long siteFormId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _siteformLocalService.getSiteform(siteFormId);
	}

	/**
	* Updates the siteform in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param siteform the siteform
	* @return the siteform that was updated
	*/
	@Override
	public com.tahir.customworkflowbe.model.Siteform updateSiteform(
		com.tahir.customworkflowbe.model.Siteform siteform) {
		return _siteformLocalService.updateSiteform(siteform);
	}

	/**
	* Returns the number of siteforms.
	*
	* @return the number of siteforms
	*/
	@Override
	public int getSiteformsCount() {
		return _siteformLocalService.getSiteformsCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _siteformLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _siteformLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.tahir.customworkflowbe.model.impl.SiteformModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _siteformLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.tahir.customworkflowbe.model.impl.SiteformModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _siteformLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns a range of all the siteforms.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.tahir.customworkflowbe.model.impl.SiteformModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of siteforms
	* @param end the upper bound of the range of siteforms (not inclusive)
	* @return the range of siteforms
	*/
	@Override
	public java.util.List<com.tahir.customworkflowbe.model.Siteform> getSiteforms(
		int start, int end) {
		return _siteformLocalService.getSiteforms(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _siteformLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _siteformLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public SiteformLocalService getWrappedService() {
		return _siteformLocalService;
	}

	@Override
	public void setWrappedService(SiteformLocalService siteformLocalService) {
		_siteformLocalService = siteformLocalService;
	}

	private SiteformLocalService _siteformLocalService;
}