/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.tahir.customworkflowbe.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link SourceService}.
 *
 * @author Brian Wing Shun Chan
 * @see SourceService
 * @generated
 */
@ProviderType
public class SourceServiceWrapper implements SourceService,
	ServiceWrapper<SourceService> {
	public SourceServiceWrapper(SourceService sourceService) {
		_sourceService = sourceService;
	}

	@Override
	public com.tahir.customworkflowbe.model.Source getSourceDetails(
		long sourceId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _sourceService.getSourceDetails(sourceId);
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _sourceService.getOSGiServiceIdentifier();
	}

	@Override
	public SourceService getWrappedService() {
		return _sourceService;
	}

	@Override
	public void setWrappedService(SourceService sourceService) {
		_sourceService = sourceService;
	}

	private SourceService _sourceService;
}