package com.tahir.custworkflow;

import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

import com.liferay.portal.kernel.service.WorkflowDefinitionLinkLocalService;



public class ServiceWorkflowTracker extends ServiceTracker<WorkflowDefinitionLinkLocalService, WorkflowDefinitionLinkLocalService>{

	public ServiceWorkflowTracker(Object host)
	{
		super(FrameworkUtil.getBundle(host.getClass()).getBundleContext(),WorkflowDefinitionLinkLocalService.class,null);
	}
}
