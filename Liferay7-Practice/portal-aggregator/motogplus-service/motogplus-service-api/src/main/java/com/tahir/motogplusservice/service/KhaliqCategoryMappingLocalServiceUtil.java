/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.tahir.motogplusservice.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for KhaliqCategoryMapping. This utility wraps
 * {@link com.tahir.motogplusservice.service.impl.KhaliqCategoryMappingLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author TahirNoor
 * @see KhaliqCategoryMappingLocalService
 * @see com.tahir.motogplusservice.service.base.KhaliqCategoryMappingLocalServiceBaseImpl
 * @see com.tahir.motogplusservice.service.impl.KhaliqCategoryMappingLocalServiceImpl
 * @generated
 */
@ProviderType
public class KhaliqCategoryMappingLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.tahir.motogplusservice.service.impl.KhaliqCategoryMappingLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */
	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Adds the khaliq category mapping to the database. Also notifies the appropriate model listeners.
	*
	* @param khaliqCategoryMapping the khaliq category mapping
	* @return the khaliq category mapping that was added
	*/
	public static com.tahir.motogplusservice.model.KhaliqCategoryMapping addKhaliqCategoryMapping(
		com.tahir.motogplusservice.model.KhaliqCategoryMapping khaliqCategoryMapping) {
		return getService().addKhaliqCategoryMapping(khaliqCategoryMapping);
	}

	/**
	* Creates a new khaliq category mapping with the primary key. Does not add the khaliq category mapping to the database.
	*
	* @param khaliqCategoryMappingId the primary key for the new khaliq category mapping
	* @return the new khaliq category mapping
	*/
	public static com.tahir.motogplusservice.model.KhaliqCategoryMapping createKhaliqCategoryMapping(
		long khaliqCategoryMappingId) {
		return getService().createKhaliqCategoryMapping(khaliqCategoryMappingId);
	}

	/**
	* Deletes the khaliq category mapping from the database. Also notifies the appropriate model listeners.
	*
	* @param khaliqCategoryMapping the khaliq category mapping
	* @return the khaliq category mapping that was removed
	*/
	public static com.tahir.motogplusservice.model.KhaliqCategoryMapping deleteKhaliqCategoryMapping(
		com.tahir.motogplusservice.model.KhaliqCategoryMapping khaliqCategoryMapping) {
		return getService().deleteKhaliqCategoryMapping(khaliqCategoryMapping);
	}

	/**
	* Deletes the khaliq category mapping with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param khaliqCategoryMappingId the primary key of the khaliq category mapping
	* @return the khaliq category mapping that was removed
	* @throws PortalException if a khaliq category mapping with the primary key could not be found
	*/
	public static com.tahir.motogplusservice.model.KhaliqCategoryMapping deleteKhaliqCategoryMapping(
		long khaliqCategoryMappingId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteKhaliqCategoryMapping(khaliqCategoryMappingId);
	}

	public static com.tahir.motogplusservice.model.KhaliqCategoryMapping fetchKhaliqCategoryMapping(
		long khaliqCategoryMappingId) {
		return getService().fetchKhaliqCategoryMapping(khaliqCategoryMappingId);
	}

	/**
	* Returns the khaliq category mapping with the primary key.
	*
	* @param khaliqCategoryMappingId the primary key of the khaliq category mapping
	* @return the khaliq category mapping
	* @throws PortalException if a khaliq category mapping with the primary key could not be found
	*/
	public static com.tahir.motogplusservice.model.KhaliqCategoryMapping getKhaliqCategoryMapping(
		long khaliqCategoryMappingId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getKhaliqCategoryMapping(khaliqCategoryMappingId);
	}

	/**
	* Updates the khaliq category mapping in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param khaliqCategoryMapping the khaliq category mapping
	* @return the khaliq category mapping that was updated
	*/
	public static com.tahir.motogplusservice.model.KhaliqCategoryMapping updateKhaliqCategoryMapping(
		com.tahir.motogplusservice.model.KhaliqCategoryMapping khaliqCategoryMapping) {
		return getService().updateKhaliqCategoryMapping(khaliqCategoryMapping);
	}

	/**
	* Returns the number of khaliq category mappings.
	*
	* @return the number of khaliq category mappings
	*/
	public static int getKhaliqCategoryMappingsCount() {
		return getService().getKhaliqCategoryMappingsCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.tahir.motogplusservice.model.impl.KhaliqCategoryMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.tahir.motogplusservice.model.impl.KhaliqCategoryMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns a range of all the khaliq category mappings.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.tahir.motogplusservice.model.impl.KhaliqCategoryMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of khaliq category mappings
	* @param end the upper bound of the range of khaliq category mappings (not inclusive)
	* @return the range of khaliq category mappings
	*/
	public static java.util.List<com.tahir.motogplusservice.model.KhaliqCategoryMapping> getKhaliqCategoryMappings(
		int start, int end) {
		return getService().getKhaliqCategoryMappings(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static KhaliqCategoryMappingLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<KhaliqCategoryMappingLocalService, KhaliqCategoryMappingLocalService> _serviceTracker =
		ServiceTrackerFactory.open(KhaliqCategoryMappingLocalService.class);
}