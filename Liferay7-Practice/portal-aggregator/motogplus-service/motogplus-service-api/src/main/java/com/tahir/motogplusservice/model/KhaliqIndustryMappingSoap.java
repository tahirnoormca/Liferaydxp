/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.tahir.motogplusservice.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.tahir.motogplusservice.service.http.KhaliqIndustryMappingServiceSoap}.
 *
 * @author TahirNoor
 * @see com.tahir.motogplusservice.service.http.KhaliqIndustryMappingServiceSoap
 * @generated
 */
@ProviderType
public class KhaliqIndustryMappingSoap implements Serializable {
	public static KhaliqIndustryMappingSoap toSoapModel(
		KhaliqIndustryMapping model) {
		KhaliqIndustryMappingSoap soapModel = new KhaliqIndustryMappingSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setKhaliqIndustryMappingId(model.getKhaliqIndustryMappingId());
		soapModel.setKhaliqId(model.getKhaliqId());
		soapModel.setIndustryId(model.getIndustryId());

		return soapModel;
	}

	public static KhaliqIndustryMappingSoap[] toSoapModels(
		KhaliqIndustryMapping[] models) {
		KhaliqIndustryMappingSoap[] soapModels = new KhaliqIndustryMappingSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static KhaliqIndustryMappingSoap[][] toSoapModels(
		KhaliqIndustryMapping[][] models) {
		KhaliqIndustryMappingSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new KhaliqIndustryMappingSoap[models.length][models[0].length];
		}
		else {
			soapModels = new KhaliqIndustryMappingSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static KhaliqIndustryMappingSoap[] toSoapModels(
		List<KhaliqIndustryMapping> models) {
		List<KhaliqIndustryMappingSoap> soapModels = new ArrayList<KhaliqIndustryMappingSoap>(models.size());

		for (KhaliqIndustryMapping model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new KhaliqIndustryMappingSoap[soapModels.size()]);
	}

	public KhaliqIndustryMappingSoap() {
	}

	public long getPrimaryKey() {
		return _khaliqIndustryMappingId;
	}

	public void setPrimaryKey(long pk) {
		setKhaliqIndustryMappingId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getKhaliqIndustryMappingId() {
		return _khaliqIndustryMappingId;
	}

	public void setKhaliqIndustryMappingId(long khaliqIndustryMappingId) {
		_khaliqIndustryMappingId = khaliqIndustryMappingId;
	}

	public long getKhaliqId() {
		return _khaliqId;
	}

	public void setKhaliqId(long khaliqId) {
		_khaliqId = khaliqId;
	}

	public long getIndustryId() {
		return _industryId;
	}

	public void setIndustryId(long industryId) {
		_industryId = industryId;
	}

	private String _uuid;
	private long _khaliqIndustryMappingId;
	private long _khaliqId;
	private long _industryId;
}