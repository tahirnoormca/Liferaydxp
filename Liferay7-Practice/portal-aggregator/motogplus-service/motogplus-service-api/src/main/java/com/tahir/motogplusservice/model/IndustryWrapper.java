/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.tahir.motogplusservice.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Industry}.
 * </p>
 *
 * @author TahirNoor
 * @see Industry
 * @generated
 */
@ProviderType
public class IndustryWrapper implements Industry, ModelWrapper<Industry> {
	public IndustryWrapper(Industry industry) {
		_industry = industry;
	}

	@Override
	public Class<?> getModelClass() {
		return Industry.class;
	}

	@Override
	public String getModelClassName() {
		return Industry.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("industryId", getIndustryId());
		attributes.put("industryName", getIndustryName());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long industryId = (Long)attributes.get("industryId");

		if (industryId != null) {
			setIndustryId(industryId);
		}

		String industryName = (String)attributes.get("industryName");

		if (industryName != null) {
			setIndustryName(industryName);
		}
	}

	@Override
	public Industry toEscapedModel() {
		return new IndustryWrapper(_industry.toEscapedModel());
	}

	@Override
	public Industry toUnescapedModel() {
		return new IndustryWrapper(_industry.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _industry.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _industry.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _industry.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _industry.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<Industry> toCacheModel() {
		return _industry.toCacheModel();
	}

	@Override
	public int compareTo(Industry industry) {
		return _industry.compareTo(industry);
	}

	@Override
	public int hashCode() {
		return _industry.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _industry.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new IndustryWrapper((Industry)_industry.clone());
	}

	/**
	* Returns the industry name of this industry.
	*
	* @return the industry name of this industry
	*/
	@Override
	public java.lang.String getIndustryName() {
		return _industry.getIndustryName();
	}

	/**
	* Returns the uuid of this industry.
	*
	* @return the uuid of this industry
	*/
	@Override
	public java.lang.String getUuid() {
		return _industry.getUuid();
	}

	@Override
	public java.lang.String toString() {
		return _industry.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _industry.toXmlString();
	}

	/**
	* Returns the industry ID of this industry.
	*
	* @return the industry ID of this industry
	*/
	@Override
	public long getIndustryId() {
		return _industry.getIndustryId();
	}

	/**
	* Returns the primary key of this industry.
	*
	* @return the primary key of this industry
	*/
	@Override
	public long getPrimaryKey() {
		return _industry.getPrimaryKey();
	}

	@Override
	public void persist() {
		_industry.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_industry.setCachedModel(cachedModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_industry.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_industry.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_industry.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the industry ID of this industry.
	*
	* @param industryId the industry ID of this industry
	*/
	@Override
	public void setIndustryId(long industryId) {
		_industry.setIndustryId(industryId);
	}

	/**
	* Sets the industry name of this industry.
	*
	* @param industryName the industry name of this industry
	*/
	@Override
	public void setIndustryName(java.lang.String industryName) {
		_industry.setIndustryName(industryName);
	}

	@Override
	public void setNew(boolean n) {
		_industry.setNew(n);
	}

	/**
	* Sets the primary key of this industry.
	*
	* @param primaryKey the primary key of this industry
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_industry.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_industry.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the uuid of this industry.
	*
	* @param uuid the uuid of this industry
	*/
	@Override
	public void setUuid(java.lang.String uuid) {
		_industry.setUuid(uuid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof IndustryWrapper)) {
			return false;
		}

		IndustryWrapper industryWrapper = (IndustryWrapper)obj;

		if (Objects.equals(_industry, industryWrapper._industry)) {
			return true;
		}

		return false;
	}

	@Override
	public Industry getWrappedModel() {
		return _industry;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _industry.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _industry.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_industry.resetOriginalValues();
	}

	private final Industry _industry;
}