/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.tahir.motogplusservice.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.tahir.motogplusservice.service.http.KhaliqCategoryMappingServiceSoap}.
 *
 * @author TahirNoor
 * @see com.tahir.motogplusservice.service.http.KhaliqCategoryMappingServiceSoap
 * @generated
 */
@ProviderType
public class KhaliqCategoryMappingSoap implements Serializable {
	public static KhaliqCategoryMappingSoap toSoapModel(
		KhaliqCategoryMapping model) {
		KhaliqCategoryMappingSoap soapModel = new KhaliqCategoryMappingSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setKhaliqCategoryMappingId(model.getKhaliqCategoryMappingId());
		soapModel.setKhaliqId(model.getKhaliqId());
		soapModel.setCategoryId(model.getCategoryId());

		return soapModel;
	}

	public static KhaliqCategoryMappingSoap[] toSoapModels(
		KhaliqCategoryMapping[] models) {
		KhaliqCategoryMappingSoap[] soapModels = new KhaliqCategoryMappingSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static KhaliqCategoryMappingSoap[][] toSoapModels(
		KhaliqCategoryMapping[][] models) {
		KhaliqCategoryMappingSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new KhaliqCategoryMappingSoap[models.length][models[0].length];
		}
		else {
			soapModels = new KhaliqCategoryMappingSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static KhaliqCategoryMappingSoap[] toSoapModels(
		List<KhaliqCategoryMapping> models) {
		List<KhaliqCategoryMappingSoap> soapModels = new ArrayList<KhaliqCategoryMappingSoap>(models.size());

		for (KhaliqCategoryMapping model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new KhaliqCategoryMappingSoap[soapModels.size()]);
	}

	public KhaliqCategoryMappingSoap() {
	}

	public long getPrimaryKey() {
		return _khaliqCategoryMappingId;
	}

	public void setPrimaryKey(long pk) {
		setKhaliqCategoryMappingId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getKhaliqCategoryMappingId() {
		return _khaliqCategoryMappingId;
	}

	public void setKhaliqCategoryMappingId(long khaliqCategoryMappingId) {
		_khaliqCategoryMappingId = khaliqCategoryMappingId;
	}

	public long getKhaliqId() {
		return _khaliqId;
	}

	public void setKhaliqId(long khaliqId) {
		_khaliqId = khaliqId;
	}

	public long getCategoryId() {
		return _categoryId;
	}

	public void setCategoryId(long categoryId) {
		_categoryId = categoryId;
	}

	private String _uuid;
	private long _khaliqCategoryMappingId;
	private long _khaliqId;
	private long _categoryId;
}