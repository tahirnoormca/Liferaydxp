/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.tahir.motogplusservice.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link KhaliqCategoryMapping}.
 * </p>
 *
 * @author TahirNoor
 * @see KhaliqCategoryMapping
 * @generated
 */
@ProviderType
public class KhaliqCategoryMappingWrapper implements KhaliqCategoryMapping,
	ModelWrapper<KhaliqCategoryMapping> {
	public KhaliqCategoryMappingWrapper(
		KhaliqCategoryMapping khaliqCategoryMapping) {
		_khaliqCategoryMapping = khaliqCategoryMapping;
	}

	@Override
	public Class<?> getModelClass() {
		return KhaliqCategoryMapping.class;
	}

	@Override
	public String getModelClassName() {
		return KhaliqCategoryMapping.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("khaliqCategoryMappingId", getKhaliqCategoryMappingId());
		attributes.put("khaliqId", getKhaliqId());
		attributes.put("categoryId", getCategoryId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long khaliqCategoryMappingId = (Long)attributes.get(
				"khaliqCategoryMappingId");

		if (khaliqCategoryMappingId != null) {
			setKhaliqCategoryMappingId(khaliqCategoryMappingId);
		}

		Long khaliqId = (Long)attributes.get("khaliqId");

		if (khaliqId != null) {
			setKhaliqId(khaliqId);
		}

		Long categoryId = (Long)attributes.get("categoryId");

		if (categoryId != null) {
			setCategoryId(categoryId);
		}
	}

	@Override
	public KhaliqCategoryMapping toEscapedModel() {
		return new KhaliqCategoryMappingWrapper(_khaliqCategoryMapping.toEscapedModel());
	}

	@Override
	public KhaliqCategoryMapping toUnescapedModel() {
		return new KhaliqCategoryMappingWrapper(_khaliqCategoryMapping.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _khaliqCategoryMapping.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _khaliqCategoryMapping.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _khaliqCategoryMapping.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _khaliqCategoryMapping.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<KhaliqCategoryMapping> toCacheModel() {
		return _khaliqCategoryMapping.toCacheModel();
	}

	@Override
	public int compareTo(KhaliqCategoryMapping khaliqCategoryMapping) {
		return _khaliqCategoryMapping.compareTo(khaliqCategoryMapping);
	}

	@Override
	public int hashCode() {
		return _khaliqCategoryMapping.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _khaliqCategoryMapping.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new KhaliqCategoryMappingWrapper((KhaliqCategoryMapping)_khaliqCategoryMapping.clone());
	}

	/**
	* Returns the uuid of this khaliq category mapping.
	*
	* @return the uuid of this khaliq category mapping
	*/
	@Override
	public java.lang.String getUuid() {
		return _khaliqCategoryMapping.getUuid();
	}

	@Override
	public java.lang.String toString() {
		return _khaliqCategoryMapping.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _khaliqCategoryMapping.toXmlString();
	}

	/**
	* Returns the category ID of this khaliq category mapping.
	*
	* @return the category ID of this khaliq category mapping
	*/
	@Override
	public long getCategoryId() {
		return _khaliqCategoryMapping.getCategoryId();
	}

	/**
	* Returns the khaliq category mapping ID of this khaliq category mapping.
	*
	* @return the khaliq category mapping ID of this khaliq category mapping
	*/
	@Override
	public long getKhaliqCategoryMappingId() {
		return _khaliqCategoryMapping.getKhaliqCategoryMappingId();
	}

	/**
	* Returns the khaliq ID of this khaliq category mapping.
	*
	* @return the khaliq ID of this khaliq category mapping
	*/
	@Override
	public long getKhaliqId() {
		return _khaliqCategoryMapping.getKhaliqId();
	}

	/**
	* Returns the primary key of this khaliq category mapping.
	*
	* @return the primary key of this khaliq category mapping
	*/
	@Override
	public long getPrimaryKey() {
		return _khaliqCategoryMapping.getPrimaryKey();
	}

	@Override
	public void persist() {
		_khaliqCategoryMapping.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_khaliqCategoryMapping.setCachedModel(cachedModel);
	}

	/**
	* Sets the category ID of this khaliq category mapping.
	*
	* @param categoryId the category ID of this khaliq category mapping
	*/
	@Override
	public void setCategoryId(long categoryId) {
		_khaliqCategoryMapping.setCategoryId(categoryId);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_khaliqCategoryMapping.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_khaliqCategoryMapping.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_khaliqCategoryMapping.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the khaliq category mapping ID of this khaliq category mapping.
	*
	* @param khaliqCategoryMappingId the khaliq category mapping ID of this khaliq category mapping
	*/
	@Override
	public void setKhaliqCategoryMappingId(long khaliqCategoryMappingId) {
		_khaliqCategoryMapping.setKhaliqCategoryMappingId(khaliqCategoryMappingId);
	}

	/**
	* Sets the khaliq ID of this khaliq category mapping.
	*
	* @param khaliqId the khaliq ID of this khaliq category mapping
	*/
	@Override
	public void setKhaliqId(long khaliqId) {
		_khaliqCategoryMapping.setKhaliqId(khaliqId);
	}

	@Override
	public void setNew(boolean n) {
		_khaliqCategoryMapping.setNew(n);
	}

	/**
	* Sets the primary key of this khaliq category mapping.
	*
	* @param primaryKey the primary key of this khaliq category mapping
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_khaliqCategoryMapping.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_khaliqCategoryMapping.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the uuid of this khaliq category mapping.
	*
	* @param uuid the uuid of this khaliq category mapping
	*/
	@Override
	public void setUuid(java.lang.String uuid) {
		_khaliqCategoryMapping.setUuid(uuid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof KhaliqCategoryMappingWrapper)) {
			return false;
		}

		KhaliqCategoryMappingWrapper khaliqCategoryMappingWrapper = (KhaliqCategoryMappingWrapper)obj;

		if (Objects.equals(_khaliqCategoryMapping,
					khaliqCategoryMappingWrapper._khaliqCategoryMapping)) {
			return true;
		}

		return false;
	}

	@Override
	public KhaliqCategoryMapping getWrappedModel() {
		return _khaliqCategoryMapping;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _khaliqCategoryMapping.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _khaliqCategoryMapping.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_khaliqCategoryMapping.resetOriginalValues();
	}

	private final KhaliqCategoryMapping _khaliqCategoryMapping;
}