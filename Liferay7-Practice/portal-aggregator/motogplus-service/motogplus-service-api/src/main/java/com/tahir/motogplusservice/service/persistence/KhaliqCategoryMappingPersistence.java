/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.tahir.motogplusservice.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import com.tahir.motogplusservice.exception.NoSuchKhaliqCategoryMappingException;
import com.tahir.motogplusservice.model.KhaliqCategoryMapping;

/**
 * The persistence interface for the khaliq category mapping service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author TahirNoor
 * @see com.tahir.motogplusservice.service.persistence.impl.KhaliqCategoryMappingPersistenceImpl
 * @see KhaliqCategoryMappingUtil
 * @generated
 */
@ProviderType
public interface KhaliqCategoryMappingPersistence extends BasePersistence<KhaliqCategoryMapping> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link KhaliqCategoryMappingUtil} to access the khaliq category mapping persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the khaliq category mappings where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching khaliq category mappings
	*/
	public java.util.List<KhaliqCategoryMapping> findByUuid(
		java.lang.String uuid);

	/**
	* Returns a range of all the khaliq category mappings where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link KhaliqCategoryMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of khaliq category mappings
	* @param end the upper bound of the range of khaliq category mappings (not inclusive)
	* @return the range of matching khaliq category mappings
	*/
	public java.util.List<KhaliqCategoryMapping> findByUuid(
		java.lang.String uuid, int start, int end);

	/**
	* Returns an ordered range of all the khaliq category mappings where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link KhaliqCategoryMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of khaliq category mappings
	* @param end the upper bound of the range of khaliq category mappings (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching khaliq category mappings
	*/
	public java.util.List<KhaliqCategoryMapping> findByUuid(
		java.lang.String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<KhaliqCategoryMapping> orderByComparator);

	/**
	* Returns an ordered range of all the khaliq category mappings where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link KhaliqCategoryMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of khaliq category mappings
	* @param end the upper bound of the range of khaliq category mappings (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching khaliq category mappings
	*/
	public java.util.List<KhaliqCategoryMapping> findByUuid(
		java.lang.String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<KhaliqCategoryMapping> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first khaliq category mapping in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching khaliq category mapping
	* @throws NoSuchKhaliqCategoryMappingException if a matching khaliq category mapping could not be found
	*/
	public KhaliqCategoryMapping findByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<KhaliqCategoryMapping> orderByComparator)
		throws NoSuchKhaliqCategoryMappingException;

	/**
	* Returns the first khaliq category mapping in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching khaliq category mapping, or <code>null</code> if a matching khaliq category mapping could not be found
	*/
	public KhaliqCategoryMapping fetchByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<KhaliqCategoryMapping> orderByComparator);

	/**
	* Returns the last khaliq category mapping in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching khaliq category mapping
	* @throws NoSuchKhaliqCategoryMappingException if a matching khaliq category mapping could not be found
	*/
	public KhaliqCategoryMapping findByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<KhaliqCategoryMapping> orderByComparator)
		throws NoSuchKhaliqCategoryMappingException;

	/**
	* Returns the last khaliq category mapping in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching khaliq category mapping, or <code>null</code> if a matching khaliq category mapping could not be found
	*/
	public KhaliqCategoryMapping fetchByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<KhaliqCategoryMapping> orderByComparator);

	/**
	* Returns the khaliq category mappings before and after the current khaliq category mapping in the ordered set where uuid = &#63;.
	*
	* @param khaliqCategoryMappingId the primary key of the current khaliq category mapping
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next khaliq category mapping
	* @throws NoSuchKhaliqCategoryMappingException if a khaliq category mapping with the primary key could not be found
	*/
	public KhaliqCategoryMapping[] findByUuid_PrevAndNext(
		long khaliqCategoryMappingId, java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<KhaliqCategoryMapping> orderByComparator)
		throws NoSuchKhaliqCategoryMappingException;

	/**
	* Removes all the khaliq category mappings where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public void removeByUuid(java.lang.String uuid);

	/**
	* Returns the number of khaliq category mappings where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching khaliq category mappings
	*/
	public int countByUuid(java.lang.String uuid);

	/**
	* Caches the khaliq category mapping in the entity cache if it is enabled.
	*
	* @param khaliqCategoryMapping the khaliq category mapping
	*/
	public void cacheResult(KhaliqCategoryMapping khaliqCategoryMapping);

	/**
	* Caches the khaliq category mappings in the entity cache if it is enabled.
	*
	* @param khaliqCategoryMappings the khaliq category mappings
	*/
	public void cacheResult(
		java.util.List<KhaliqCategoryMapping> khaliqCategoryMappings);

	/**
	* Creates a new khaliq category mapping with the primary key. Does not add the khaliq category mapping to the database.
	*
	* @param khaliqCategoryMappingId the primary key for the new khaliq category mapping
	* @return the new khaliq category mapping
	*/
	public KhaliqCategoryMapping create(long khaliqCategoryMappingId);

	/**
	* Removes the khaliq category mapping with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param khaliqCategoryMappingId the primary key of the khaliq category mapping
	* @return the khaliq category mapping that was removed
	* @throws NoSuchKhaliqCategoryMappingException if a khaliq category mapping with the primary key could not be found
	*/
	public KhaliqCategoryMapping remove(long khaliqCategoryMappingId)
		throws NoSuchKhaliqCategoryMappingException;

	public KhaliqCategoryMapping updateImpl(
		KhaliqCategoryMapping khaliqCategoryMapping);

	/**
	* Returns the khaliq category mapping with the primary key or throws a {@link NoSuchKhaliqCategoryMappingException} if it could not be found.
	*
	* @param khaliqCategoryMappingId the primary key of the khaliq category mapping
	* @return the khaliq category mapping
	* @throws NoSuchKhaliqCategoryMappingException if a khaliq category mapping with the primary key could not be found
	*/
	public KhaliqCategoryMapping findByPrimaryKey(long khaliqCategoryMappingId)
		throws NoSuchKhaliqCategoryMappingException;

	/**
	* Returns the khaliq category mapping with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param khaliqCategoryMappingId the primary key of the khaliq category mapping
	* @return the khaliq category mapping, or <code>null</code> if a khaliq category mapping with the primary key could not be found
	*/
	public KhaliqCategoryMapping fetchByPrimaryKey(long khaliqCategoryMappingId);

	@Override
	public java.util.Map<java.io.Serializable, KhaliqCategoryMapping> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the khaliq category mappings.
	*
	* @return the khaliq category mappings
	*/
	public java.util.List<KhaliqCategoryMapping> findAll();

	/**
	* Returns a range of all the khaliq category mappings.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link KhaliqCategoryMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of khaliq category mappings
	* @param end the upper bound of the range of khaliq category mappings (not inclusive)
	* @return the range of khaliq category mappings
	*/
	public java.util.List<KhaliqCategoryMapping> findAll(int start, int end);

	/**
	* Returns an ordered range of all the khaliq category mappings.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link KhaliqCategoryMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of khaliq category mappings
	* @param end the upper bound of the range of khaliq category mappings (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of khaliq category mappings
	*/
	public java.util.List<KhaliqCategoryMapping> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<KhaliqCategoryMapping> orderByComparator);

	/**
	* Returns an ordered range of all the khaliq category mappings.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link KhaliqCategoryMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of khaliq category mappings
	* @param end the upper bound of the range of khaliq category mappings (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of khaliq category mappings
	*/
	public java.util.List<KhaliqCategoryMapping> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<KhaliqCategoryMapping> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the khaliq category mappings from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of khaliq category mappings.
	*
	* @return the number of khaliq category mappings
	*/
	public int countAll();

	@Override
	public java.util.Set<java.lang.String> getBadColumnNames();
}