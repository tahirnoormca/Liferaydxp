/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.tahir.motogplusservice.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import com.tahir.motogplusservice.exception.NoSuchKhaliqIndustryMappingException;
import com.tahir.motogplusservice.model.KhaliqIndustryMapping;

/**
 * The persistence interface for the khaliq industry mapping service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author TahirNoor
 * @see com.tahir.motogplusservice.service.persistence.impl.KhaliqIndustryMappingPersistenceImpl
 * @see KhaliqIndustryMappingUtil
 * @generated
 */
@ProviderType
public interface KhaliqIndustryMappingPersistence extends BasePersistence<KhaliqIndustryMapping> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link KhaliqIndustryMappingUtil} to access the khaliq industry mapping persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the khaliq industry mappings where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching khaliq industry mappings
	*/
	public java.util.List<KhaliqIndustryMapping> findByUuid(
		java.lang.String uuid);

	/**
	* Returns a range of all the khaliq industry mappings where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link KhaliqIndustryMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of khaliq industry mappings
	* @param end the upper bound of the range of khaliq industry mappings (not inclusive)
	* @return the range of matching khaliq industry mappings
	*/
	public java.util.List<KhaliqIndustryMapping> findByUuid(
		java.lang.String uuid, int start, int end);

	/**
	* Returns an ordered range of all the khaliq industry mappings where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link KhaliqIndustryMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of khaliq industry mappings
	* @param end the upper bound of the range of khaliq industry mappings (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching khaliq industry mappings
	*/
	public java.util.List<KhaliqIndustryMapping> findByUuid(
		java.lang.String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<KhaliqIndustryMapping> orderByComparator);

	/**
	* Returns an ordered range of all the khaliq industry mappings where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link KhaliqIndustryMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of khaliq industry mappings
	* @param end the upper bound of the range of khaliq industry mappings (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching khaliq industry mappings
	*/
	public java.util.List<KhaliqIndustryMapping> findByUuid(
		java.lang.String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<KhaliqIndustryMapping> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first khaliq industry mapping in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching khaliq industry mapping
	* @throws NoSuchKhaliqIndustryMappingException if a matching khaliq industry mapping could not be found
	*/
	public KhaliqIndustryMapping findByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<KhaliqIndustryMapping> orderByComparator)
		throws NoSuchKhaliqIndustryMappingException;

	/**
	* Returns the first khaliq industry mapping in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching khaliq industry mapping, or <code>null</code> if a matching khaliq industry mapping could not be found
	*/
	public KhaliqIndustryMapping fetchByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<KhaliqIndustryMapping> orderByComparator);

	/**
	* Returns the last khaliq industry mapping in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching khaliq industry mapping
	* @throws NoSuchKhaliqIndustryMappingException if a matching khaliq industry mapping could not be found
	*/
	public KhaliqIndustryMapping findByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<KhaliqIndustryMapping> orderByComparator)
		throws NoSuchKhaliqIndustryMappingException;

	/**
	* Returns the last khaliq industry mapping in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching khaliq industry mapping, or <code>null</code> if a matching khaliq industry mapping could not be found
	*/
	public KhaliqIndustryMapping fetchByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<KhaliqIndustryMapping> orderByComparator);

	/**
	* Returns the khaliq industry mappings before and after the current khaliq industry mapping in the ordered set where uuid = &#63;.
	*
	* @param khaliqIndustryMappingId the primary key of the current khaliq industry mapping
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next khaliq industry mapping
	* @throws NoSuchKhaliqIndustryMappingException if a khaliq industry mapping with the primary key could not be found
	*/
	public KhaliqIndustryMapping[] findByUuid_PrevAndNext(
		long khaliqIndustryMappingId, java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<KhaliqIndustryMapping> orderByComparator)
		throws NoSuchKhaliqIndustryMappingException;

	/**
	* Removes all the khaliq industry mappings where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public void removeByUuid(java.lang.String uuid);

	/**
	* Returns the number of khaliq industry mappings where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching khaliq industry mappings
	*/
	public int countByUuid(java.lang.String uuid);

	/**
	* Caches the khaliq industry mapping in the entity cache if it is enabled.
	*
	* @param khaliqIndustryMapping the khaliq industry mapping
	*/
	public void cacheResult(KhaliqIndustryMapping khaliqIndustryMapping);

	/**
	* Caches the khaliq industry mappings in the entity cache if it is enabled.
	*
	* @param khaliqIndustryMappings the khaliq industry mappings
	*/
	public void cacheResult(
		java.util.List<KhaliqIndustryMapping> khaliqIndustryMappings);

	/**
	* Creates a new khaliq industry mapping with the primary key. Does not add the khaliq industry mapping to the database.
	*
	* @param khaliqIndustryMappingId the primary key for the new khaliq industry mapping
	* @return the new khaliq industry mapping
	*/
	public KhaliqIndustryMapping create(long khaliqIndustryMappingId);

	/**
	* Removes the khaliq industry mapping with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param khaliqIndustryMappingId the primary key of the khaliq industry mapping
	* @return the khaliq industry mapping that was removed
	* @throws NoSuchKhaliqIndustryMappingException if a khaliq industry mapping with the primary key could not be found
	*/
	public KhaliqIndustryMapping remove(long khaliqIndustryMappingId)
		throws NoSuchKhaliqIndustryMappingException;

	public KhaliqIndustryMapping updateImpl(
		KhaliqIndustryMapping khaliqIndustryMapping);

	/**
	* Returns the khaliq industry mapping with the primary key or throws a {@link NoSuchKhaliqIndustryMappingException} if it could not be found.
	*
	* @param khaliqIndustryMappingId the primary key of the khaliq industry mapping
	* @return the khaliq industry mapping
	* @throws NoSuchKhaliqIndustryMappingException if a khaliq industry mapping with the primary key could not be found
	*/
	public KhaliqIndustryMapping findByPrimaryKey(long khaliqIndustryMappingId)
		throws NoSuchKhaliqIndustryMappingException;

	/**
	* Returns the khaliq industry mapping with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param khaliqIndustryMappingId the primary key of the khaliq industry mapping
	* @return the khaliq industry mapping, or <code>null</code> if a khaliq industry mapping with the primary key could not be found
	*/
	public KhaliqIndustryMapping fetchByPrimaryKey(long khaliqIndustryMappingId);

	@Override
	public java.util.Map<java.io.Serializable, KhaliqIndustryMapping> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the khaliq industry mappings.
	*
	* @return the khaliq industry mappings
	*/
	public java.util.List<KhaliqIndustryMapping> findAll();

	/**
	* Returns a range of all the khaliq industry mappings.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link KhaliqIndustryMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of khaliq industry mappings
	* @param end the upper bound of the range of khaliq industry mappings (not inclusive)
	* @return the range of khaliq industry mappings
	*/
	public java.util.List<KhaliqIndustryMapping> findAll(int start, int end);

	/**
	* Returns an ordered range of all the khaliq industry mappings.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link KhaliqIndustryMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of khaliq industry mappings
	* @param end the upper bound of the range of khaliq industry mappings (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of khaliq industry mappings
	*/
	public java.util.List<KhaliqIndustryMapping> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<KhaliqIndustryMapping> orderByComparator);

	/**
	* Returns an ordered range of all the khaliq industry mappings.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link KhaliqIndustryMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of khaliq industry mappings
	* @param end the upper bound of the range of khaliq industry mappings (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of khaliq industry mappings
	*/
	public java.util.List<KhaliqIndustryMapping> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<KhaliqIndustryMapping> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the khaliq industry mappings from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of khaliq industry mappings.
	*
	* @return the number of khaliq industry mappings
	*/
	public int countAll();

	@Override
	public java.util.Set<java.lang.String> getBadColumnNames();
}