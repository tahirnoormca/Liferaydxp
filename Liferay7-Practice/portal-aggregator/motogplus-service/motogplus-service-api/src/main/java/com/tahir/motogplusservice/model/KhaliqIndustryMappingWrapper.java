/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.tahir.motogplusservice.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link KhaliqIndustryMapping}.
 * </p>
 *
 * @author TahirNoor
 * @see KhaliqIndustryMapping
 * @generated
 */
@ProviderType
public class KhaliqIndustryMappingWrapper implements KhaliqIndustryMapping,
	ModelWrapper<KhaliqIndustryMapping> {
	public KhaliqIndustryMappingWrapper(
		KhaliqIndustryMapping khaliqIndustryMapping) {
		_khaliqIndustryMapping = khaliqIndustryMapping;
	}

	@Override
	public Class<?> getModelClass() {
		return KhaliqIndustryMapping.class;
	}

	@Override
	public String getModelClassName() {
		return KhaliqIndustryMapping.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("khaliqIndustryMappingId", getKhaliqIndustryMappingId());
		attributes.put("khaliqId", getKhaliqId());
		attributes.put("industryId", getIndustryId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long khaliqIndustryMappingId = (Long)attributes.get(
				"khaliqIndustryMappingId");

		if (khaliqIndustryMappingId != null) {
			setKhaliqIndustryMappingId(khaliqIndustryMappingId);
		}

		Long khaliqId = (Long)attributes.get("khaliqId");

		if (khaliqId != null) {
			setKhaliqId(khaliqId);
		}

		Long industryId = (Long)attributes.get("industryId");

		if (industryId != null) {
			setIndustryId(industryId);
		}
	}

	@Override
	public KhaliqIndustryMapping toEscapedModel() {
		return new KhaliqIndustryMappingWrapper(_khaliqIndustryMapping.toEscapedModel());
	}

	@Override
	public KhaliqIndustryMapping toUnescapedModel() {
		return new KhaliqIndustryMappingWrapper(_khaliqIndustryMapping.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _khaliqIndustryMapping.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _khaliqIndustryMapping.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _khaliqIndustryMapping.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _khaliqIndustryMapping.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<KhaliqIndustryMapping> toCacheModel() {
		return _khaliqIndustryMapping.toCacheModel();
	}

	@Override
	public int compareTo(KhaliqIndustryMapping khaliqIndustryMapping) {
		return _khaliqIndustryMapping.compareTo(khaliqIndustryMapping);
	}

	@Override
	public int hashCode() {
		return _khaliqIndustryMapping.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _khaliqIndustryMapping.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new KhaliqIndustryMappingWrapper((KhaliqIndustryMapping)_khaliqIndustryMapping.clone());
	}

	/**
	* Returns the uuid of this khaliq industry mapping.
	*
	* @return the uuid of this khaliq industry mapping
	*/
	@Override
	public java.lang.String getUuid() {
		return _khaliqIndustryMapping.getUuid();
	}

	@Override
	public java.lang.String toString() {
		return _khaliqIndustryMapping.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _khaliqIndustryMapping.toXmlString();
	}

	/**
	* Returns the industry ID of this khaliq industry mapping.
	*
	* @return the industry ID of this khaliq industry mapping
	*/
	@Override
	public long getIndustryId() {
		return _khaliqIndustryMapping.getIndustryId();
	}

	/**
	* Returns the khaliq ID of this khaliq industry mapping.
	*
	* @return the khaliq ID of this khaliq industry mapping
	*/
	@Override
	public long getKhaliqId() {
		return _khaliqIndustryMapping.getKhaliqId();
	}

	/**
	* Returns the khaliq industry mapping ID of this khaliq industry mapping.
	*
	* @return the khaliq industry mapping ID of this khaliq industry mapping
	*/
	@Override
	public long getKhaliqIndustryMappingId() {
		return _khaliqIndustryMapping.getKhaliqIndustryMappingId();
	}

	/**
	* Returns the primary key of this khaliq industry mapping.
	*
	* @return the primary key of this khaliq industry mapping
	*/
	@Override
	public long getPrimaryKey() {
		return _khaliqIndustryMapping.getPrimaryKey();
	}

	@Override
	public void persist() {
		_khaliqIndustryMapping.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_khaliqIndustryMapping.setCachedModel(cachedModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_khaliqIndustryMapping.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_khaliqIndustryMapping.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_khaliqIndustryMapping.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the industry ID of this khaliq industry mapping.
	*
	* @param industryId the industry ID of this khaliq industry mapping
	*/
	@Override
	public void setIndustryId(long industryId) {
		_khaliqIndustryMapping.setIndustryId(industryId);
	}

	/**
	* Sets the khaliq ID of this khaliq industry mapping.
	*
	* @param khaliqId the khaliq ID of this khaliq industry mapping
	*/
	@Override
	public void setKhaliqId(long khaliqId) {
		_khaliqIndustryMapping.setKhaliqId(khaliqId);
	}

	/**
	* Sets the khaliq industry mapping ID of this khaliq industry mapping.
	*
	* @param khaliqIndustryMappingId the khaliq industry mapping ID of this khaliq industry mapping
	*/
	@Override
	public void setKhaliqIndustryMappingId(long khaliqIndustryMappingId) {
		_khaliqIndustryMapping.setKhaliqIndustryMappingId(khaliqIndustryMappingId);
	}

	@Override
	public void setNew(boolean n) {
		_khaliqIndustryMapping.setNew(n);
	}

	/**
	* Sets the primary key of this khaliq industry mapping.
	*
	* @param primaryKey the primary key of this khaliq industry mapping
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_khaliqIndustryMapping.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_khaliqIndustryMapping.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the uuid of this khaliq industry mapping.
	*
	* @param uuid the uuid of this khaliq industry mapping
	*/
	@Override
	public void setUuid(java.lang.String uuid) {
		_khaliqIndustryMapping.setUuid(uuid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof KhaliqIndustryMappingWrapper)) {
			return false;
		}

		KhaliqIndustryMappingWrapper khaliqIndustryMappingWrapper = (KhaliqIndustryMappingWrapper)obj;

		if (Objects.equals(_khaliqIndustryMapping,
					khaliqIndustryMappingWrapper._khaliqIndustryMapping)) {
			return true;
		}

		return false;
	}

	@Override
	public KhaliqIndustryMapping getWrappedModel() {
		return _khaliqIndustryMapping;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _khaliqIndustryMapping.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _khaliqIndustryMapping.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_khaliqIndustryMapping.resetOriginalValues();
	}

	private final KhaliqIndustryMapping _khaliqIndustryMapping;
}