/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.tahir.motogplusservice.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.tahir.motogplusservice.model.KhaliqCategoryMapping;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the khaliq category mapping service. This utility wraps {@link com.tahir.motogplusservice.service.persistence.impl.KhaliqCategoryMappingPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author TahirNoor
 * @see KhaliqCategoryMappingPersistence
 * @see com.tahir.motogplusservice.service.persistence.impl.KhaliqCategoryMappingPersistenceImpl
 * @generated
 */
@ProviderType
public class KhaliqCategoryMappingUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(KhaliqCategoryMapping khaliqCategoryMapping) {
		getPersistence().clearCache(khaliqCategoryMapping);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<KhaliqCategoryMapping> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<KhaliqCategoryMapping> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<KhaliqCategoryMapping> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<KhaliqCategoryMapping> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static KhaliqCategoryMapping update(
		KhaliqCategoryMapping khaliqCategoryMapping) {
		return getPersistence().update(khaliqCategoryMapping);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static KhaliqCategoryMapping update(
		KhaliqCategoryMapping khaliqCategoryMapping,
		ServiceContext serviceContext) {
		return getPersistence().update(khaliqCategoryMapping, serviceContext);
	}

	/**
	* Returns all the khaliq category mappings where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching khaliq category mappings
	*/
	public static List<KhaliqCategoryMapping> findByUuid(java.lang.String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	* Returns a range of all the khaliq category mappings where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link KhaliqCategoryMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of khaliq category mappings
	* @param end the upper bound of the range of khaliq category mappings (not inclusive)
	* @return the range of matching khaliq category mappings
	*/
	public static List<KhaliqCategoryMapping> findByUuid(
		java.lang.String uuid, int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	* Returns an ordered range of all the khaliq category mappings where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link KhaliqCategoryMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of khaliq category mappings
	* @param end the upper bound of the range of khaliq category mappings (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching khaliq category mappings
	*/
	public static List<KhaliqCategoryMapping> findByUuid(
		java.lang.String uuid, int start, int end,
		OrderByComparator<KhaliqCategoryMapping> orderByComparator) {
		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the khaliq category mappings where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link KhaliqCategoryMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of khaliq category mappings
	* @param end the upper bound of the range of khaliq category mappings (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching khaliq category mappings
	*/
	public static List<KhaliqCategoryMapping> findByUuid(
		java.lang.String uuid, int start, int end,
		OrderByComparator<KhaliqCategoryMapping> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid(uuid, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first khaliq category mapping in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching khaliq category mapping
	* @throws NoSuchKhaliqCategoryMappingException if a matching khaliq category mapping could not be found
	*/
	public static KhaliqCategoryMapping findByUuid_First(
		java.lang.String uuid,
		OrderByComparator<KhaliqCategoryMapping> orderByComparator)
		throws com.tahir.motogplusservice.exception.NoSuchKhaliqCategoryMappingException {
		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the first khaliq category mapping in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching khaliq category mapping, or <code>null</code> if a matching khaliq category mapping could not be found
	*/
	public static KhaliqCategoryMapping fetchByUuid_First(
		java.lang.String uuid,
		OrderByComparator<KhaliqCategoryMapping> orderByComparator) {
		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the last khaliq category mapping in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching khaliq category mapping
	* @throws NoSuchKhaliqCategoryMappingException if a matching khaliq category mapping could not be found
	*/
	public static KhaliqCategoryMapping findByUuid_Last(java.lang.String uuid,
		OrderByComparator<KhaliqCategoryMapping> orderByComparator)
		throws com.tahir.motogplusservice.exception.NoSuchKhaliqCategoryMappingException {
		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the last khaliq category mapping in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching khaliq category mapping, or <code>null</code> if a matching khaliq category mapping could not be found
	*/
	public static KhaliqCategoryMapping fetchByUuid_Last(
		java.lang.String uuid,
		OrderByComparator<KhaliqCategoryMapping> orderByComparator) {
		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the khaliq category mappings before and after the current khaliq category mapping in the ordered set where uuid = &#63;.
	*
	* @param khaliqCategoryMappingId the primary key of the current khaliq category mapping
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next khaliq category mapping
	* @throws NoSuchKhaliqCategoryMappingException if a khaliq category mapping with the primary key could not be found
	*/
	public static KhaliqCategoryMapping[] findByUuid_PrevAndNext(
		long khaliqCategoryMappingId, java.lang.String uuid,
		OrderByComparator<KhaliqCategoryMapping> orderByComparator)
		throws com.tahir.motogplusservice.exception.NoSuchKhaliqCategoryMappingException {
		return getPersistence()
				   .findByUuid_PrevAndNext(khaliqCategoryMappingId, uuid,
			orderByComparator);
	}

	/**
	* Removes all the khaliq category mappings where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public static void removeByUuid(java.lang.String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	* Returns the number of khaliq category mappings where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching khaliq category mappings
	*/
	public static int countByUuid(java.lang.String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	* Caches the khaliq category mapping in the entity cache if it is enabled.
	*
	* @param khaliqCategoryMapping the khaliq category mapping
	*/
	public static void cacheResult(KhaliqCategoryMapping khaliqCategoryMapping) {
		getPersistence().cacheResult(khaliqCategoryMapping);
	}

	/**
	* Caches the khaliq category mappings in the entity cache if it is enabled.
	*
	* @param khaliqCategoryMappings the khaliq category mappings
	*/
	public static void cacheResult(
		List<KhaliqCategoryMapping> khaliqCategoryMappings) {
		getPersistence().cacheResult(khaliqCategoryMappings);
	}

	/**
	* Creates a new khaliq category mapping with the primary key. Does not add the khaliq category mapping to the database.
	*
	* @param khaliqCategoryMappingId the primary key for the new khaliq category mapping
	* @return the new khaliq category mapping
	*/
	public static KhaliqCategoryMapping create(long khaliqCategoryMappingId) {
		return getPersistence().create(khaliqCategoryMappingId);
	}

	/**
	* Removes the khaliq category mapping with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param khaliqCategoryMappingId the primary key of the khaliq category mapping
	* @return the khaliq category mapping that was removed
	* @throws NoSuchKhaliqCategoryMappingException if a khaliq category mapping with the primary key could not be found
	*/
	public static KhaliqCategoryMapping remove(long khaliqCategoryMappingId)
		throws com.tahir.motogplusservice.exception.NoSuchKhaliqCategoryMappingException {
		return getPersistence().remove(khaliqCategoryMappingId);
	}

	public static KhaliqCategoryMapping updateImpl(
		KhaliqCategoryMapping khaliqCategoryMapping) {
		return getPersistence().updateImpl(khaliqCategoryMapping);
	}

	/**
	* Returns the khaliq category mapping with the primary key or throws a {@link NoSuchKhaliqCategoryMappingException} if it could not be found.
	*
	* @param khaliqCategoryMappingId the primary key of the khaliq category mapping
	* @return the khaliq category mapping
	* @throws NoSuchKhaliqCategoryMappingException if a khaliq category mapping with the primary key could not be found
	*/
	public static KhaliqCategoryMapping findByPrimaryKey(
		long khaliqCategoryMappingId)
		throws com.tahir.motogplusservice.exception.NoSuchKhaliqCategoryMappingException {
		return getPersistence().findByPrimaryKey(khaliqCategoryMappingId);
	}

	/**
	* Returns the khaliq category mapping with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param khaliqCategoryMappingId the primary key of the khaliq category mapping
	* @return the khaliq category mapping, or <code>null</code> if a khaliq category mapping with the primary key could not be found
	*/
	public static KhaliqCategoryMapping fetchByPrimaryKey(
		long khaliqCategoryMappingId) {
		return getPersistence().fetchByPrimaryKey(khaliqCategoryMappingId);
	}

	public static java.util.Map<java.io.Serializable, KhaliqCategoryMapping> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the khaliq category mappings.
	*
	* @return the khaliq category mappings
	*/
	public static List<KhaliqCategoryMapping> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the khaliq category mappings.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link KhaliqCategoryMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of khaliq category mappings
	* @param end the upper bound of the range of khaliq category mappings (not inclusive)
	* @return the range of khaliq category mappings
	*/
	public static List<KhaliqCategoryMapping> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the khaliq category mappings.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link KhaliqCategoryMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of khaliq category mappings
	* @param end the upper bound of the range of khaliq category mappings (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of khaliq category mappings
	*/
	public static List<KhaliqCategoryMapping> findAll(int start, int end,
		OrderByComparator<KhaliqCategoryMapping> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the khaliq category mappings.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link KhaliqCategoryMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of khaliq category mappings
	* @param end the upper bound of the range of khaliq category mappings (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of khaliq category mappings
	*/
	public static List<KhaliqCategoryMapping> findAll(int start, int end,
		OrderByComparator<KhaliqCategoryMapping> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the khaliq category mappings from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of khaliq category mappings.
	*
	* @return the number of khaliq category mappings
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static java.util.Set<java.lang.String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static KhaliqCategoryMappingPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<KhaliqCategoryMappingPersistence, KhaliqCategoryMappingPersistence> _serviceTracker =
		ServiceTrackerFactory.open(KhaliqCategoryMappingPersistence.class);
}