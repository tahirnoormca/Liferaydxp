/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.tahir.motogplusservice.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

/**
 * The extended model interface for the KhaliqCategoryMapping service. Represents a row in the &quot;moto_KhaliqCategoryMapping&quot; database table, with each column mapped to a property of this class.
 *
 * @author TahirNoor
 * @see KhaliqCategoryMappingModel
 * @see com.tahir.motogplusservice.model.impl.KhaliqCategoryMappingImpl
 * @see com.tahir.motogplusservice.model.impl.KhaliqCategoryMappingModelImpl
 * @generated
 */
@ImplementationClassName("com.tahir.motogplusservice.model.impl.KhaliqCategoryMappingImpl")
@ProviderType
public interface KhaliqCategoryMapping extends KhaliqCategoryMappingModel,
	PersistedModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to {@link com.tahir.motogplusservice.model.impl.KhaliqCategoryMappingImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<KhaliqCategoryMapping, Long> KHALIQ_CATEGORY_MAPPING_ID_ACCESSOR =
		new Accessor<KhaliqCategoryMapping, Long>() {
			@Override
			public Long get(KhaliqCategoryMapping khaliqCategoryMapping) {
				return khaliqCategoryMapping.getKhaliqCategoryMappingId();
			}

			@Override
			public Class<Long> getAttributeClass() {
				return Long.class;
			}

			@Override
			public Class<KhaliqCategoryMapping> getTypeClass() {
				return KhaliqCategoryMapping.class;
			}
		};
}