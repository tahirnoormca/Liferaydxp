/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.tahir.motogplusservice.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link KhaliqIndustryMappingLocalService}.
 *
 * @author TahirNoor
 * @see KhaliqIndustryMappingLocalService
 * @generated
 */
@ProviderType
public class KhaliqIndustryMappingLocalServiceWrapper
	implements KhaliqIndustryMappingLocalService,
		ServiceWrapper<KhaliqIndustryMappingLocalService> {
	public KhaliqIndustryMappingLocalServiceWrapper(
		KhaliqIndustryMappingLocalService khaliqIndustryMappingLocalService) {
		_khaliqIndustryMappingLocalService = khaliqIndustryMappingLocalService;
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _khaliqIndustryMappingLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _khaliqIndustryMappingLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _khaliqIndustryMappingLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _khaliqIndustryMappingLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _khaliqIndustryMappingLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Adds the khaliq industry mapping to the database. Also notifies the appropriate model listeners.
	*
	* @param khaliqIndustryMapping the khaliq industry mapping
	* @return the khaliq industry mapping that was added
	*/
	@Override
	public com.tahir.motogplusservice.model.KhaliqIndustryMapping addKhaliqIndustryMapping(
		com.tahir.motogplusservice.model.KhaliqIndustryMapping khaliqIndustryMapping) {
		return _khaliqIndustryMappingLocalService.addKhaliqIndustryMapping(khaliqIndustryMapping);
	}

	/**
	* Creates a new khaliq industry mapping with the primary key. Does not add the khaliq industry mapping to the database.
	*
	* @param khaliqIndustryMappingId the primary key for the new khaliq industry mapping
	* @return the new khaliq industry mapping
	*/
	@Override
	public com.tahir.motogplusservice.model.KhaliqIndustryMapping createKhaliqIndustryMapping(
		long khaliqIndustryMappingId) {
		return _khaliqIndustryMappingLocalService.createKhaliqIndustryMapping(khaliqIndustryMappingId);
	}

	/**
	* Deletes the khaliq industry mapping from the database. Also notifies the appropriate model listeners.
	*
	* @param khaliqIndustryMapping the khaliq industry mapping
	* @return the khaliq industry mapping that was removed
	*/
	@Override
	public com.tahir.motogplusservice.model.KhaliqIndustryMapping deleteKhaliqIndustryMapping(
		com.tahir.motogplusservice.model.KhaliqIndustryMapping khaliqIndustryMapping) {
		return _khaliqIndustryMappingLocalService.deleteKhaliqIndustryMapping(khaliqIndustryMapping);
	}

	/**
	* Deletes the khaliq industry mapping with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param khaliqIndustryMappingId the primary key of the khaliq industry mapping
	* @return the khaliq industry mapping that was removed
	* @throws PortalException if a khaliq industry mapping with the primary key could not be found
	*/
	@Override
	public com.tahir.motogplusservice.model.KhaliqIndustryMapping deleteKhaliqIndustryMapping(
		long khaliqIndustryMappingId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _khaliqIndustryMappingLocalService.deleteKhaliqIndustryMapping(khaliqIndustryMappingId);
	}

	@Override
	public com.tahir.motogplusservice.model.KhaliqIndustryMapping fetchKhaliqIndustryMapping(
		long khaliqIndustryMappingId) {
		return _khaliqIndustryMappingLocalService.fetchKhaliqIndustryMapping(khaliqIndustryMappingId);
	}

	/**
	* Returns the khaliq industry mapping with the primary key.
	*
	* @param khaliqIndustryMappingId the primary key of the khaliq industry mapping
	* @return the khaliq industry mapping
	* @throws PortalException if a khaliq industry mapping with the primary key could not be found
	*/
	@Override
	public com.tahir.motogplusservice.model.KhaliqIndustryMapping getKhaliqIndustryMapping(
		long khaliqIndustryMappingId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _khaliqIndustryMappingLocalService.getKhaliqIndustryMapping(khaliqIndustryMappingId);
	}

	/**
	* Updates the khaliq industry mapping in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param khaliqIndustryMapping the khaliq industry mapping
	* @return the khaliq industry mapping that was updated
	*/
	@Override
	public com.tahir.motogplusservice.model.KhaliqIndustryMapping updateKhaliqIndustryMapping(
		com.tahir.motogplusservice.model.KhaliqIndustryMapping khaliqIndustryMapping) {
		return _khaliqIndustryMappingLocalService.updateKhaliqIndustryMapping(khaliqIndustryMapping);
	}

	/**
	* Returns the number of khaliq industry mappings.
	*
	* @return the number of khaliq industry mappings
	*/
	@Override
	public int getKhaliqIndustryMappingsCount() {
		return _khaliqIndustryMappingLocalService.getKhaliqIndustryMappingsCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _khaliqIndustryMappingLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _khaliqIndustryMappingLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.tahir.motogplusservice.model.impl.KhaliqIndustryMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _khaliqIndustryMappingLocalService.dynamicQuery(dynamicQuery,
			start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.tahir.motogplusservice.model.impl.KhaliqIndustryMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _khaliqIndustryMappingLocalService.dynamicQuery(dynamicQuery,
			start, end, orderByComparator);
	}

	/**
	* Returns a range of all the khaliq industry mappings.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.tahir.motogplusservice.model.impl.KhaliqIndustryMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of khaliq industry mappings
	* @param end the upper bound of the range of khaliq industry mappings (not inclusive)
	* @return the range of khaliq industry mappings
	*/
	@Override
	public java.util.List<com.tahir.motogplusservice.model.KhaliqIndustryMapping> getKhaliqIndustryMappings(
		int start, int end) {
		return _khaliqIndustryMappingLocalService.getKhaliqIndustryMappings(start,
			end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _khaliqIndustryMappingLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _khaliqIndustryMappingLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public KhaliqIndustryMappingLocalService getWrappedService() {
		return _khaliqIndustryMappingLocalService;
	}

	@Override
	public void setWrappedService(
		KhaliqIndustryMappingLocalService khaliqIndustryMappingLocalService) {
		_khaliqIndustryMappingLocalService = khaliqIndustryMappingLocalService;
	}

	private KhaliqIndustryMappingLocalService _khaliqIndustryMappingLocalService;
}