/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.tahir.motogplusservice.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import com.tahir.motogplusservice.model.KhaliqIndustryMapping;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing KhaliqIndustryMapping in entity cache.
 *
 * @author TahirNoor
 * @see KhaliqIndustryMapping
 * @generated
 */
@ProviderType
public class KhaliqIndustryMappingCacheModel implements CacheModel<KhaliqIndustryMapping>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof KhaliqIndustryMappingCacheModel)) {
			return false;
		}

		KhaliqIndustryMappingCacheModel khaliqIndustryMappingCacheModel = (KhaliqIndustryMappingCacheModel)obj;

		if (khaliqIndustryMappingId == khaliqIndustryMappingCacheModel.khaliqIndustryMappingId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, khaliqIndustryMappingId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", khaliqIndustryMappingId=");
		sb.append(khaliqIndustryMappingId);
		sb.append(", khaliqId=");
		sb.append(khaliqId);
		sb.append(", industryId=");
		sb.append(industryId);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public KhaliqIndustryMapping toEntityModel() {
		KhaliqIndustryMappingImpl khaliqIndustryMappingImpl = new KhaliqIndustryMappingImpl();

		if (uuid == null) {
			khaliqIndustryMappingImpl.setUuid(StringPool.BLANK);
		}
		else {
			khaliqIndustryMappingImpl.setUuid(uuid);
		}

		khaliqIndustryMappingImpl.setKhaliqIndustryMappingId(khaliqIndustryMappingId);
		khaliqIndustryMappingImpl.setKhaliqId(khaliqId);
		khaliqIndustryMappingImpl.setIndustryId(industryId);

		khaliqIndustryMappingImpl.resetOriginalValues();

		return khaliqIndustryMappingImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		khaliqIndustryMappingId = objectInput.readLong();

		khaliqId = objectInput.readLong();

		industryId = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(khaliqIndustryMappingId);

		objectOutput.writeLong(khaliqId);

		objectOutput.writeLong(industryId);
	}

	public String uuid;
	public long khaliqIndustryMappingId;
	public long khaliqId;
	public long industryId;
}