/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.tahir.motogplusservice.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.liferay.portal.spring.extender.service.ServiceReference;

import com.tahir.motogplusservice.exception.NoSuchKhaliqIndustryMappingException;
import com.tahir.motogplusservice.model.KhaliqIndustryMapping;
import com.tahir.motogplusservice.model.impl.KhaliqIndustryMappingImpl;
import com.tahir.motogplusservice.model.impl.KhaliqIndustryMappingModelImpl;
import com.tahir.motogplusservice.service.persistence.KhaliqIndustryMappingPersistence;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the khaliq industry mapping service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author TahirNoor
 * @see KhaliqIndustryMappingPersistence
 * @see com.tahir.motogplusservice.service.persistence.KhaliqIndustryMappingUtil
 * @generated
 */
@ProviderType
public class KhaliqIndustryMappingPersistenceImpl extends BasePersistenceImpl<KhaliqIndustryMapping>
	implements KhaliqIndustryMappingPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link KhaliqIndustryMappingUtil} to access the khaliq industry mapping persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = KhaliqIndustryMappingImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(KhaliqIndustryMappingModelImpl.ENTITY_CACHE_ENABLED,
			KhaliqIndustryMappingModelImpl.FINDER_CACHE_ENABLED,
			KhaliqIndustryMappingImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(KhaliqIndustryMappingModelImpl.ENTITY_CACHE_ENABLED,
			KhaliqIndustryMappingModelImpl.FINDER_CACHE_ENABLED,
			KhaliqIndustryMappingImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(KhaliqIndustryMappingModelImpl.ENTITY_CACHE_ENABLED,
			KhaliqIndustryMappingModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID = new FinderPath(KhaliqIndustryMappingModelImpl.ENTITY_CACHE_ENABLED,
			KhaliqIndustryMappingModelImpl.FINDER_CACHE_ENABLED,
			KhaliqIndustryMappingImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID = new FinderPath(KhaliqIndustryMappingModelImpl.ENTITY_CACHE_ENABLED,
			KhaliqIndustryMappingModelImpl.FINDER_CACHE_ENABLED,
			KhaliqIndustryMappingImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] { String.class.getName() },
			KhaliqIndustryMappingModelImpl.UUID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID = new FinderPath(KhaliqIndustryMappingModelImpl.ENTITY_CACHE_ENABLED,
			KhaliqIndustryMappingModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] { String.class.getName() });

	/**
	 * Returns all the khaliq industry mappings where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching khaliq industry mappings
	 */
	@Override
	public List<KhaliqIndustryMapping> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the khaliq industry mappings where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link KhaliqIndustryMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of khaliq industry mappings
	 * @param end the upper bound of the range of khaliq industry mappings (not inclusive)
	 * @return the range of matching khaliq industry mappings
	 */
	@Override
	public List<KhaliqIndustryMapping> findByUuid(String uuid, int start,
		int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the khaliq industry mappings where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link KhaliqIndustryMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of khaliq industry mappings
	 * @param end the upper bound of the range of khaliq industry mappings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching khaliq industry mappings
	 */
	@Override
	public List<KhaliqIndustryMapping> findByUuid(String uuid, int start,
		int end, OrderByComparator<KhaliqIndustryMapping> orderByComparator) {
		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the khaliq industry mappings where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link KhaliqIndustryMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of khaliq industry mappings
	 * @param end the upper bound of the range of khaliq industry mappings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching khaliq industry mappings
	 */
	@Override
	public List<KhaliqIndustryMapping> findByUuid(String uuid, int start,
		int end, OrderByComparator<KhaliqIndustryMapping> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid, start, end, orderByComparator };
		}

		List<KhaliqIndustryMapping> list = null;

		if (retrieveFromCache) {
			list = (List<KhaliqIndustryMapping>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (KhaliqIndustryMapping khaliqIndustryMapping : list) {
					if (!Objects.equals(uuid, khaliqIndustryMapping.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_KHALIQINDUSTRYMAPPING_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(KhaliqIndustryMappingModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				if (!pagination) {
					list = (List<KhaliqIndustryMapping>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<KhaliqIndustryMapping>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first khaliq industry mapping in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching khaliq industry mapping
	 * @throws NoSuchKhaliqIndustryMappingException if a matching khaliq industry mapping could not be found
	 */
	@Override
	public KhaliqIndustryMapping findByUuid_First(String uuid,
		OrderByComparator<KhaliqIndustryMapping> orderByComparator)
		throws NoSuchKhaliqIndustryMappingException {
		KhaliqIndustryMapping khaliqIndustryMapping = fetchByUuid_First(uuid,
				orderByComparator);

		if (khaliqIndustryMapping != null) {
			return khaliqIndustryMapping;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchKhaliqIndustryMappingException(msg.toString());
	}

	/**
	 * Returns the first khaliq industry mapping in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching khaliq industry mapping, or <code>null</code> if a matching khaliq industry mapping could not be found
	 */
	@Override
	public KhaliqIndustryMapping fetchByUuid_First(String uuid,
		OrderByComparator<KhaliqIndustryMapping> orderByComparator) {
		List<KhaliqIndustryMapping> list = findByUuid(uuid, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last khaliq industry mapping in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching khaliq industry mapping
	 * @throws NoSuchKhaliqIndustryMappingException if a matching khaliq industry mapping could not be found
	 */
	@Override
	public KhaliqIndustryMapping findByUuid_Last(String uuid,
		OrderByComparator<KhaliqIndustryMapping> orderByComparator)
		throws NoSuchKhaliqIndustryMappingException {
		KhaliqIndustryMapping khaliqIndustryMapping = fetchByUuid_Last(uuid,
				orderByComparator);

		if (khaliqIndustryMapping != null) {
			return khaliqIndustryMapping;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchKhaliqIndustryMappingException(msg.toString());
	}

	/**
	 * Returns the last khaliq industry mapping in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching khaliq industry mapping, or <code>null</code> if a matching khaliq industry mapping could not be found
	 */
	@Override
	public KhaliqIndustryMapping fetchByUuid_Last(String uuid,
		OrderByComparator<KhaliqIndustryMapping> orderByComparator) {
		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<KhaliqIndustryMapping> list = findByUuid(uuid, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the khaliq industry mappings before and after the current khaliq industry mapping in the ordered set where uuid = &#63;.
	 *
	 * @param khaliqIndustryMappingId the primary key of the current khaliq industry mapping
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next khaliq industry mapping
	 * @throws NoSuchKhaliqIndustryMappingException if a khaliq industry mapping with the primary key could not be found
	 */
	@Override
	public KhaliqIndustryMapping[] findByUuid_PrevAndNext(
		long khaliqIndustryMappingId, String uuid,
		OrderByComparator<KhaliqIndustryMapping> orderByComparator)
		throws NoSuchKhaliqIndustryMappingException {
		KhaliqIndustryMapping khaliqIndustryMapping = findByPrimaryKey(khaliqIndustryMappingId);

		Session session = null;

		try {
			session = openSession();

			KhaliqIndustryMapping[] array = new KhaliqIndustryMappingImpl[3];

			array[0] = getByUuid_PrevAndNext(session, khaliqIndustryMapping,
					uuid, orderByComparator, true);

			array[1] = khaliqIndustryMapping;

			array[2] = getByUuid_PrevAndNext(session, khaliqIndustryMapping,
					uuid, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected KhaliqIndustryMapping getByUuid_PrevAndNext(Session session,
		KhaliqIndustryMapping khaliqIndustryMapping, String uuid,
		OrderByComparator<KhaliqIndustryMapping> orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_KHALIQINDUSTRYMAPPING_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_UUID_1);
		}
		else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(KhaliqIndustryMappingModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(khaliqIndustryMapping);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<KhaliqIndustryMapping> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the khaliq industry mappings where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (KhaliqIndustryMapping khaliqIndustryMapping : findByUuid(uuid,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(khaliqIndustryMapping);
		}
	}

	/**
	 * Returns the number of khaliq industry mappings where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching khaliq industry mappings
	 */
	@Override
	public int countByUuid(String uuid) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID;

		Object[] finderArgs = new Object[] { uuid };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_KHALIQINDUSTRYMAPPING_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_1 = "khaliqIndustryMapping.uuid IS NULL";
	private static final String _FINDER_COLUMN_UUID_UUID_2 = "khaliqIndustryMapping.uuid = ?";
	private static final String _FINDER_COLUMN_UUID_UUID_3 = "(khaliqIndustryMapping.uuid IS NULL OR khaliqIndustryMapping.uuid = '')";

	public KhaliqIndustryMappingPersistenceImpl() {
		setModelClass(KhaliqIndustryMapping.class);
	}

	/**
	 * Caches the khaliq industry mapping in the entity cache if it is enabled.
	 *
	 * @param khaliqIndustryMapping the khaliq industry mapping
	 */
	@Override
	public void cacheResult(KhaliqIndustryMapping khaliqIndustryMapping) {
		entityCache.putResult(KhaliqIndustryMappingModelImpl.ENTITY_CACHE_ENABLED,
			KhaliqIndustryMappingImpl.class,
			khaliqIndustryMapping.getPrimaryKey(), khaliqIndustryMapping);

		khaliqIndustryMapping.resetOriginalValues();
	}

	/**
	 * Caches the khaliq industry mappings in the entity cache if it is enabled.
	 *
	 * @param khaliqIndustryMappings the khaliq industry mappings
	 */
	@Override
	public void cacheResult(List<KhaliqIndustryMapping> khaliqIndustryMappings) {
		for (KhaliqIndustryMapping khaliqIndustryMapping : khaliqIndustryMappings) {
			if (entityCache.getResult(
						KhaliqIndustryMappingModelImpl.ENTITY_CACHE_ENABLED,
						KhaliqIndustryMappingImpl.class,
						khaliqIndustryMapping.getPrimaryKey()) == null) {
				cacheResult(khaliqIndustryMapping);
			}
			else {
				khaliqIndustryMapping.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all khaliq industry mappings.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(KhaliqIndustryMappingImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the khaliq industry mapping.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(KhaliqIndustryMapping khaliqIndustryMapping) {
		entityCache.removeResult(KhaliqIndustryMappingModelImpl.ENTITY_CACHE_ENABLED,
			KhaliqIndustryMappingImpl.class,
			khaliqIndustryMapping.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<KhaliqIndustryMapping> khaliqIndustryMappings) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (KhaliqIndustryMapping khaliqIndustryMapping : khaliqIndustryMappings) {
			entityCache.removeResult(KhaliqIndustryMappingModelImpl.ENTITY_CACHE_ENABLED,
				KhaliqIndustryMappingImpl.class,
				khaliqIndustryMapping.getPrimaryKey());
		}
	}

	/**
	 * Creates a new khaliq industry mapping with the primary key. Does not add the khaliq industry mapping to the database.
	 *
	 * @param khaliqIndustryMappingId the primary key for the new khaliq industry mapping
	 * @return the new khaliq industry mapping
	 */
	@Override
	public KhaliqIndustryMapping create(long khaliqIndustryMappingId) {
		KhaliqIndustryMapping khaliqIndustryMapping = new KhaliqIndustryMappingImpl();

		khaliqIndustryMapping.setNew(true);
		khaliqIndustryMapping.setPrimaryKey(khaliqIndustryMappingId);

		String uuid = PortalUUIDUtil.generate();

		khaliqIndustryMapping.setUuid(uuid);

		return khaliqIndustryMapping;
	}

	/**
	 * Removes the khaliq industry mapping with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param khaliqIndustryMappingId the primary key of the khaliq industry mapping
	 * @return the khaliq industry mapping that was removed
	 * @throws NoSuchKhaliqIndustryMappingException if a khaliq industry mapping with the primary key could not be found
	 */
	@Override
	public KhaliqIndustryMapping remove(long khaliqIndustryMappingId)
		throws NoSuchKhaliqIndustryMappingException {
		return remove((Serializable)khaliqIndustryMappingId);
	}

	/**
	 * Removes the khaliq industry mapping with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the khaliq industry mapping
	 * @return the khaliq industry mapping that was removed
	 * @throws NoSuchKhaliqIndustryMappingException if a khaliq industry mapping with the primary key could not be found
	 */
	@Override
	public KhaliqIndustryMapping remove(Serializable primaryKey)
		throws NoSuchKhaliqIndustryMappingException {
		Session session = null;

		try {
			session = openSession();

			KhaliqIndustryMapping khaliqIndustryMapping = (KhaliqIndustryMapping)session.get(KhaliqIndustryMappingImpl.class,
					primaryKey);

			if (khaliqIndustryMapping == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchKhaliqIndustryMappingException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(khaliqIndustryMapping);
		}
		catch (NoSuchKhaliqIndustryMappingException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected KhaliqIndustryMapping removeImpl(
		KhaliqIndustryMapping khaliqIndustryMapping) {
		khaliqIndustryMapping = toUnwrappedModel(khaliqIndustryMapping);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(khaliqIndustryMapping)) {
				khaliqIndustryMapping = (KhaliqIndustryMapping)session.get(KhaliqIndustryMappingImpl.class,
						khaliqIndustryMapping.getPrimaryKeyObj());
			}

			if (khaliqIndustryMapping != null) {
				session.delete(khaliqIndustryMapping);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (khaliqIndustryMapping != null) {
			clearCache(khaliqIndustryMapping);
		}

		return khaliqIndustryMapping;
	}

	@Override
	public KhaliqIndustryMapping updateImpl(
		KhaliqIndustryMapping khaliqIndustryMapping) {
		khaliqIndustryMapping = toUnwrappedModel(khaliqIndustryMapping);

		boolean isNew = khaliqIndustryMapping.isNew();

		KhaliqIndustryMappingModelImpl khaliqIndustryMappingModelImpl = (KhaliqIndustryMappingModelImpl)khaliqIndustryMapping;

		if (Validator.isNull(khaliqIndustryMapping.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			khaliqIndustryMapping.setUuid(uuid);
		}

		Session session = null;

		try {
			session = openSession();

			if (khaliqIndustryMapping.isNew()) {
				session.save(khaliqIndustryMapping);

				khaliqIndustryMapping.setNew(false);
			}
			else {
				khaliqIndustryMapping = (KhaliqIndustryMapping)session.merge(khaliqIndustryMapping);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !KhaliqIndustryMappingModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((khaliqIndustryMappingModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						khaliqIndustryMappingModelImpl.getOriginalUuid()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);

				args = new Object[] { khaliqIndustryMappingModelImpl.getUuid() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);
			}
		}

		entityCache.putResult(KhaliqIndustryMappingModelImpl.ENTITY_CACHE_ENABLED,
			KhaliqIndustryMappingImpl.class,
			khaliqIndustryMapping.getPrimaryKey(), khaliqIndustryMapping, false);

		khaliqIndustryMapping.resetOriginalValues();

		return khaliqIndustryMapping;
	}

	protected KhaliqIndustryMapping toUnwrappedModel(
		KhaliqIndustryMapping khaliqIndustryMapping) {
		if (khaliqIndustryMapping instanceof KhaliqIndustryMappingImpl) {
			return khaliqIndustryMapping;
		}

		KhaliqIndustryMappingImpl khaliqIndustryMappingImpl = new KhaliqIndustryMappingImpl();

		khaliqIndustryMappingImpl.setNew(khaliqIndustryMapping.isNew());
		khaliqIndustryMappingImpl.setPrimaryKey(khaliqIndustryMapping.getPrimaryKey());

		khaliqIndustryMappingImpl.setUuid(khaliqIndustryMapping.getUuid());
		khaliqIndustryMappingImpl.setKhaliqIndustryMappingId(khaliqIndustryMapping.getKhaliqIndustryMappingId());
		khaliqIndustryMappingImpl.setKhaliqId(khaliqIndustryMapping.getKhaliqId());
		khaliqIndustryMappingImpl.setIndustryId(khaliqIndustryMapping.getIndustryId());

		return khaliqIndustryMappingImpl;
	}

	/**
	 * Returns the khaliq industry mapping with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the khaliq industry mapping
	 * @return the khaliq industry mapping
	 * @throws NoSuchKhaliqIndustryMappingException if a khaliq industry mapping with the primary key could not be found
	 */
	@Override
	public KhaliqIndustryMapping findByPrimaryKey(Serializable primaryKey)
		throws NoSuchKhaliqIndustryMappingException {
		KhaliqIndustryMapping khaliqIndustryMapping = fetchByPrimaryKey(primaryKey);

		if (khaliqIndustryMapping == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchKhaliqIndustryMappingException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return khaliqIndustryMapping;
	}

	/**
	 * Returns the khaliq industry mapping with the primary key or throws a {@link NoSuchKhaliqIndustryMappingException} if it could not be found.
	 *
	 * @param khaliqIndustryMappingId the primary key of the khaliq industry mapping
	 * @return the khaliq industry mapping
	 * @throws NoSuchKhaliqIndustryMappingException if a khaliq industry mapping with the primary key could not be found
	 */
	@Override
	public KhaliqIndustryMapping findByPrimaryKey(long khaliqIndustryMappingId)
		throws NoSuchKhaliqIndustryMappingException {
		return findByPrimaryKey((Serializable)khaliqIndustryMappingId);
	}

	/**
	 * Returns the khaliq industry mapping with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the khaliq industry mapping
	 * @return the khaliq industry mapping, or <code>null</code> if a khaliq industry mapping with the primary key could not be found
	 */
	@Override
	public KhaliqIndustryMapping fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(KhaliqIndustryMappingModelImpl.ENTITY_CACHE_ENABLED,
				KhaliqIndustryMappingImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		KhaliqIndustryMapping khaliqIndustryMapping = (KhaliqIndustryMapping)serializable;

		if (khaliqIndustryMapping == null) {
			Session session = null;

			try {
				session = openSession();

				khaliqIndustryMapping = (KhaliqIndustryMapping)session.get(KhaliqIndustryMappingImpl.class,
						primaryKey);

				if (khaliqIndustryMapping != null) {
					cacheResult(khaliqIndustryMapping);
				}
				else {
					entityCache.putResult(KhaliqIndustryMappingModelImpl.ENTITY_CACHE_ENABLED,
						KhaliqIndustryMappingImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(KhaliqIndustryMappingModelImpl.ENTITY_CACHE_ENABLED,
					KhaliqIndustryMappingImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return khaliqIndustryMapping;
	}

	/**
	 * Returns the khaliq industry mapping with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param khaliqIndustryMappingId the primary key of the khaliq industry mapping
	 * @return the khaliq industry mapping, or <code>null</code> if a khaliq industry mapping with the primary key could not be found
	 */
	@Override
	public KhaliqIndustryMapping fetchByPrimaryKey(long khaliqIndustryMappingId) {
		return fetchByPrimaryKey((Serializable)khaliqIndustryMappingId);
	}

	@Override
	public Map<Serializable, KhaliqIndustryMapping> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, KhaliqIndustryMapping> map = new HashMap<Serializable, KhaliqIndustryMapping>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			KhaliqIndustryMapping khaliqIndustryMapping = fetchByPrimaryKey(primaryKey);

			if (khaliqIndustryMapping != null) {
				map.put(primaryKey, khaliqIndustryMapping);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(KhaliqIndustryMappingModelImpl.ENTITY_CACHE_ENABLED,
					KhaliqIndustryMappingImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (KhaliqIndustryMapping)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_KHALIQINDUSTRYMAPPING_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append(String.valueOf(primaryKey));

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (KhaliqIndustryMapping khaliqIndustryMapping : (List<KhaliqIndustryMapping>)q.list()) {
				map.put(khaliqIndustryMapping.getPrimaryKeyObj(),
					khaliqIndustryMapping);

				cacheResult(khaliqIndustryMapping);

				uncachedPrimaryKeys.remove(khaliqIndustryMapping.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(KhaliqIndustryMappingModelImpl.ENTITY_CACHE_ENABLED,
					KhaliqIndustryMappingImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the khaliq industry mappings.
	 *
	 * @return the khaliq industry mappings
	 */
	@Override
	public List<KhaliqIndustryMapping> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the khaliq industry mappings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link KhaliqIndustryMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of khaliq industry mappings
	 * @param end the upper bound of the range of khaliq industry mappings (not inclusive)
	 * @return the range of khaliq industry mappings
	 */
	@Override
	public List<KhaliqIndustryMapping> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the khaliq industry mappings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link KhaliqIndustryMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of khaliq industry mappings
	 * @param end the upper bound of the range of khaliq industry mappings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of khaliq industry mappings
	 */
	@Override
	public List<KhaliqIndustryMapping> findAll(int start, int end,
		OrderByComparator<KhaliqIndustryMapping> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the khaliq industry mappings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link KhaliqIndustryMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of khaliq industry mappings
	 * @param end the upper bound of the range of khaliq industry mappings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of khaliq industry mappings
	 */
	@Override
	public List<KhaliqIndustryMapping> findAll(int start, int end,
		OrderByComparator<KhaliqIndustryMapping> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<KhaliqIndustryMapping> list = null;

		if (retrieveFromCache) {
			list = (List<KhaliqIndustryMapping>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_KHALIQINDUSTRYMAPPING);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_KHALIQINDUSTRYMAPPING;

				if (pagination) {
					sql = sql.concat(KhaliqIndustryMappingModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<KhaliqIndustryMapping>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<KhaliqIndustryMapping>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the khaliq industry mappings from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (KhaliqIndustryMapping khaliqIndustryMapping : findAll()) {
			remove(khaliqIndustryMapping);
		}
	}

	/**
	 * Returns the number of khaliq industry mappings.
	 *
	 * @return the number of khaliq industry mappings
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_KHALIQINDUSTRYMAPPING);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return KhaliqIndustryMappingModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the khaliq industry mapping persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(KhaliqIndustryMappingImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_KHALIQINDUSTRYMAPPING = "SELECT khaliqIndustryMapping FROM KhaliqIndustryMapping khaliqIndustryMapping";
	private static final String _SQL_SELECT_KHALIQINDUSTRYMAPPING_WHERE_PKS_IN = "SELECT khaliqIndustryMapping FROM KhaliqIndustryMapping khaliqIndustryMapping WHERE khaliqIndustryMappingId IN (";
	private static final String _SQL_SELECT_KHALIQINDUSTRYMAPPING_WHERE = "SELECT khaliqIndustryMapping FROM KhaliqIndustryMapping khaliqIndustryMapping WHERE ";
	private static final String _SQL_COUNT_KHALIQINDUSTRYMAPPING = "SELECT COUNT(khaliqIndustryMapping) FROM KhaliqIndustryMapping khaliqIndustryMapping";
	private static final String _SQL_COUNT_KHALIQINDUSTRYMAPPING_WHERE = "SELECT COUNT(khaliqIndustryMapping) FROM KhaliqIndustryMapping khaliqIndustryMapping WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "khaliqIndustryMapping.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No KhaliqIndustryMapping exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No KhaliqIndustryMapping exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(KhaliqIndustryMappingPersistenceImpl.class);
	private static final Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"uuid"
			});
}