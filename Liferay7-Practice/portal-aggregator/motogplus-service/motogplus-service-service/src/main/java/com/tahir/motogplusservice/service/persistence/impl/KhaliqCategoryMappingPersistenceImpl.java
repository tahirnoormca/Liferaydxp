/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.tahir.motogplusservice.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.liferay.portal.spring.extender.service.ServiceReference;

import com.tahir.motogplusservice.exception.NoSuchKhaliqCategoryMappingException;
import com.tahir.motogplusservice.model.KhaliqCategoryMapping;
import com.tahir.motogplusservice.model.impl.KhaliqCategoryMappingImpl;
import com.tahir.motogplusservice.model.impl.KhaliqCategoryMappingModelImpl;
import com.tahir.motogplusservice.service.persistence.KhaliqCategoryMappingPersistence;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the khaliq category mapping service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author TahirNoor
 * @see KhaliqCategoryMappingPersistence
 * @see com.tahir.motogplusservice.service.persistence.KhaliqCategoryMappingUtil
 * @generated
 */
@ProviderType
public class KhaliqCategoryMappingPersistenceImpl extends BasePersistenceImpl<KhaliqCategoryMapping>
	implements KhaliqCategoryMappingPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link KhaliqCategoryMappingUtil} to access the khaliq category mapping persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = KhaliqCategoryMappingImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(KhaliqCategoryMappingModelImpl.ENTITY_CACHE_ENABLED,
			KhaliqCategoryMappingModelImpl.FINDER_CACHE_ENABLED,
			KhaliqCategoryMappingImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(KhaliqCategoryMappingModelImpl.ENTITY_CACHE_ENABLED,
			KhaliqCategoryMappingModelImpl.FINDER_CACHE_ENABLED,
			KhaliqCategoryMappingImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(KhaliqCategoryMappingModelImpl.ENTITY_CACHE_ENABLED,
			KhaliqCategoryMappingModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID = new FinderPath(KhaliqCategoryMappingModelImpl.ENTITY_CACHE_ENABLED,
			KhaliqCategoryMappingModelImpl.FINDER_CACHE_ENABLED,
			KhaliqCategoryMappingImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID = new FinderPath(KhaliqCategoryMappingModelImpl.ENTITY_CACHE_ENABLED,
			KhaliqCategoryMappingModelImpl.FINDER_CACHE_ENABLED,
			KhaliqCategoryMappingImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] { String.class.getName() },
			KhaliqCategoryMappingModelImpl.UUID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID = new FinderPath(KhaliqCategoryMappingModelImpl.ENTITY_CACHE_ENABLED,
			KhaliqCategoryMappingModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] { String.class.getName() });

	/**
	 * Returns all the khaliq category mappings where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching khaliq category mappings
	 */
	@Override
	public List<KhaliqCategoryMapping> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the khaliq category mappings where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link KhaliqCategoryMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of khaliq category mappings
	 * @param end the upper bound of the range of khaliq category mappings (not inclusive)
	 * @return the range of matching khaliq category mappings
	 */
	@Override
	public List<KhaliqCategoryMapping> findByUuid(String uuid, int start,
		int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the khaliq category mappings where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link KhaliqCategoryMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of khaliq category mappings
	 * @param end the upper bound of the range of khaliq category mappings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching khaliq category mappings
	 */
	@Override
	public List<KhaliqCategoryMapping> findByUuid(String uuid, int start,
		int end, OrderByComparator<KhaliqCategoryMapping> orderByComparator) {
		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the khaliq category mappings where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link KhaliqCategoryMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of khaliq category mappings
	 * @param end the upper bound of the range of khaliq category mappings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching khaliq category mappings
	 */
	@Override
	public List<KhaliqCategoryMapping> findByUuid(String uuid, int start,
		int end, OrderByComparator<KhaliqCategoryMapping> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid, start, end, orderByComparator };
		}

		List<KhaliqCategoryMapping> list = null;

		if (retrieveFromCache) {
			list = (List<KhaliqCategoryMapping>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (KhaliqCategoryMapping khaliqCategoryMapping : list) {
					if (!Objects.equals(uuid, khaliqCategoryMapping.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_KHALIQCATEGORYMAPPING_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(KhaliqCategoryMappingModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				if (!pagination) {
					list = (List<KhaliqCategoryMapping>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<KhaliqCategoryMapping>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first khaliq category mapping in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching khaliq category mapping
	 * @throws NoSuchKhaliqCategoryMappingException if a matching khaliq category mapping could not be found
	 */
	@Override
	public KhaliqCategoryMapping findByUuid_First(String uuid,
		OrderByComparator<KhaliqCategoryMapping> orderByComparator)
		throws NoSuchKhaliqCategoryMappingException {
		KhaliqCategoryMapping khaliqCategoryMapping = fetchByUuid_First(uuid,
				orderByComparator);

		if (khaliqCategoryMapping != null) {
			return khaliqCategoryMapping;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchKhaliqCategoryMappingException(msg.toString());
	}

	/**
	 * Returns the first khaliq category mapping in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching khaliq category mapping, or <code>null</code> if a matching khaliq category mapping could not be found
	 */
	@Override
	public KhaliqCategoryMapping fetchByUuid_First(String uuid,
		OrderByComparator<KhaliqCategoryMapping> orderByComparator) {
		List<KhaliqCategoryMapping> list = findByUuid(uuid, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last khaliq category mapping in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching khaliq category mapping
	 * @throws NoSuchKhaliqCategoryMappingException if a matching khaliq category mapping could not be found
	 */
	@Override
	public KhaliqCategoryMapping findByUuid_Last(String uuid,
		OrderByComparator<KhaliqCategoryMapping> orderByComparator)
		throws NoSuchKhaliqCategoryMappingException {
		KhaliqCategoryMapping khaliqCategoryMapping = fetchByUuid_Last(uuid,
				orderByComparator);

		if (khaliqCategoryMapping != null) {
			return khaliqCategoryMapping;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchKhaliqCategoryMappingException(msg.toString());
	}

	/**
	 * Returns the last khaliq category mapping in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching khaliq category mapping, or <code>null</code> if a matching khaliq category mapping could not be found
	 */
	@Override
	public KhaliqCategoryMapping fetchByUuid_Last(String uuid,
		OrderByComparator<KhaliqCategoryMapping> orderByComparator) {
		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<KhaliqCategoryMapping> list = findByUuid(uuid, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the khaliq category mappings before and after the current khaliq category mapping in the ordered set where uuid = &#63;.
	 *
	 * @param khaliqCategoryMappingId the primary key of the current khaliq category mapping
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next khaliq category mapping
	 * @throws NoSuchKhaliqCategoryMappingException if a khaliq category mapping with the primary key could not be found
	 */
	@Override
	public KhaliqCategoryMapping[] findByUuid_PrevAndNext(
		long khaliqCategoryMappingId, String uuid,
		OrderByComparator<KhaliqCategoryMapping> orderByComparator)
		throws NoSuchKhaliqCategoryMappingException {
		KhaliqCategoryMapping khaliqCategoryMapping = findByPrimaryKey(khaliqCategoryMappingId);

		Session session = null;

		try {
			session = openSession();

			KhaliqCategoryMapping[] array = new KhaliqCategoryMappingImpl[3];

			array[0] = getByUuid_PrevAndNext(session, khaliqCategoryMapping,
					uuid, orderByComparator, true);

			array[1] = khaliqCategoryMapping;

			array[2] = getByUuid_PrevAndNext(session, khaliqCategoryMapping,
					uuid, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected KhaliqCategoryMapping getByUuid_PrevAndNext(Session session,
		KhaliqCategoryMapping khaliqCategoryMapping, String uuid,
		OrderByComparator<KhaliqCategoryMapping> orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_KHALIQCATEGORYMAPPING_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_UUID_1);
		}
		else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(KhaliqCategoryMappingModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(khaliqCategoryMapping);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<KhaliqCategoryMapping> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the khaliq category mappings where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (KhaliqCategoryMapping khaliqCategoryMapping : findByUuid(uuid,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(khaliqCategoryMapping);
		}
	}

	/**
	 * Returns the number of khaliq category mappings where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching khaliq category mappings
	 */
	@Override
	public int countByUuid(String uuid) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID;

		Object[] finderArgs = new Object[] { uuid };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_KHALIQCATEGORYMAPPING_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_1 = "khaliqCategoryMapping.uuid IS NULL";
	private static final String _FINDER_COLUMN_UUID_UUID_2 = "khaliqCategoryMapping.uuid = ?";
	private static final String _FINDER_COLUMN_UUID_UUID_3 = "(khaliqCategoryMapping.uuid IS NULL OR khaliqCategoryMapping.uuid = '')";

	public KhaliqCategoryMappingPersistenceImpl() {
		setModelClass(KhaliqCategoryMapping.class);
	}

	/**
	 * Caches the khaliq category mapping in the entity cache if it is enabled.
	 *
	 * @param khaliqCategoryMapping the khaliq category mapping
	 */
	@Override
	public void cacheResult(KhaliqCategoryMapping khaliqCategoryMapping) {
		entityCache.putResult(KhaliqCategoryMappingModelImpl.ENTITY_CACHE_ENABLED,
			KhaliqCategoryMappingImpl.class,
			khaliqCategoryMapping.getPrimaryKey(), khaliqCategoryMapping);

		khaliqCategoryMapping.resetOriginalValues();
	}

	/**
	 * Caches the khaliq category mappings in the entity cache if it is enabled.
	 *
	 * @param khaliqCategoryMappings the khaliq category mappings
	 */
	@Override
	public void cacheResult(List<KhaliqCategoryMapping> khaliqCategoryMappings) {
		for (KhaliqCategoryMapping khaliqCategoryMapping : khaliqCategoryMappings) {
			if (entityCache.getResult(
						KhaliqCategoryMappingModelImpl.ENTITY_CACHE_ENABLED,
						KhaliqCategoryMappingImpl.class,
						khaliqCategoryMapping.getPrimaryKey()) == null) {
				cacheResult(khaliqCategoryMapping);
			}
			else {
				khaliqCategoryMapping.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all khaliq category mappings.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(KhaliqCategoryMappingImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the khaliq category mapping.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(KhaliqCategoryMapping khaliqCategoryMapping) {
		entityCache.removeResult(KhaliqCategoryMappingModelImpl.ENTITY_CACHE_ENABLED,
			KhaliqCategoryMappingImpl.class,
			khaliqCategoryMapping.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<KhaliqCategoryMapping> khaliqCategoryMappings) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (KhaliqCategoryMapping khaliqCategoryMapping : khaliqCategoryMappings) {
			entityCache.removeResult(KhaliqCategoryMappingModelImpl.ENTITY_CACHE_ENABLED,
				KhaliqCategoryMappingImpl.class,
				khaliqCategoryMapping.getPrimaryKey());
		}
	}

	/**
	 * Creates a new khaliq category mapping with the primary key. Does not add the khaliq category mapping to the database.
	 *
	 * @param khaliqCategoryMappingId the primary key for the new khaliq category mapping
	 * @return the new khaliq category mapping
	 */
	@Override
	public KhaliqCategoryMapping create(long khaliqCategoryMappingId) {
		KhaliqCategoryMapping khaliqCategoryMapping = new KhaliqCategoryMappingImpl();

		khaliqCategoryMapping.setNew(true);
		khaliqCategoryMapping.setPrimaryKey(khaliqCategoryMappingId);

		String uuid = PortalUUIDUtil.generate();

		khaliqCategoryMapping.setUuid(uuid);

		return khaliqCategoryMapping;
	}

	/**
	 * Removes the khaliq category mapping with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param khaliqCategoryMappingId the primary key of the khaliq category mapping
	 * @return the khaliq category mapping that was removed
	 * @throws NoSuchKhaliqCategoryMappingException if a khaliq category mapping with the primary key could not be found
	 */
	@Override
	public KhaliqCategoryMapping remove(long khaliqCategoryMappingId)
		throws NoSuchKhaliqCategoryMappingException {
		return remove((Serializable)khaliqCategoryMappingId);
	}

	/**
	 * Removes the khaliq category mapping with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the khaliq category mapping
	 * @return the khaliq category mapping that was removed
	 * @throws NoSuchKhaliqCategoryMappingException if a khaliq category mapping with the primary key could not be found
	 */
	@Override
	public KhaliqCategoryMapping remove(Serializable primaryKey)
		throws NoSuchKhaliqCategoryMappingException {
		Session session = null;

		try {
			session = openSession();

			KhaliqCategoryMapping khaliqCategoryMapping = (KhaliqCategoryMapping)session.get(KhaliqCategoryMappingImpl.class,
					primaryKey);

			if (khaliqCategoryMapping == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchKhaliqCategoryMappingException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(khaliqCategoryMapping);
		}
		catch (NoSuchKhaliqCategoryMappingException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected KhaliqCategoryMapping removeImpl(
		KhaliqCategoryMapping khaliqCategoryMapping) {
		khaliqCategoryMapping = toUnwrappedModel(khaliqCategoryMapping);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(khaliqCategoryMapping)) {
				khaliqCategoryMapping = (KhaliqCategoryMapping)session.get(KhaliqCategoryMappingImpl.class,
						khaliqCategoryMapping.getPrimaryKeyObj());
			}

			if (khaliqCategoryMapping != null) {
				session.delete(khaliqCategoryMapping);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (khaliqCategoryMapping != null) {
			clearCache(khaliqCategoryMapping);
		}

		return khaliqCategoryMapping;
	}

	@Override
	public KhaliqCategoryMapping updateImpl(
		KhaliqCategoryMapping khaliqCategoryMapping) {
		khaliqCategoryMapping = toUnwrappedModel(khaliqCategoryMapping);

		boolean isNew = khaliqCategoryMapping.isNew();

		KhaliqCategoryMappingModelImpl khaliqCategoryMappingModelImpl = (KhaliqCategoryMappingModelImpl)khaliqCategoryMapping;

		if (Validator.isNull(khaliqCategoryMapping.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			khaliqCategoryMapping.setUuid(uuid);
		}

		Session session = null;

		try {
			session = openSession();

			if (khaliqCategoryMapping.isNew()) {
				session.save(khaliqCategoryMapping);

				khaliqCategoryMapping.setNew(false);
			}
			else {
				khaliqCategoryMapping = (KhaliqCategoryMapping)session.merge(khaliqCategoryMapping);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !KhaliqCategoryMappingModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((khaliqCategoryMappingModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						khaliqCategoryMappingModelImpl.getOriginalUuid()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);

				args = new Object[] { khaliqCategoryMappingModelImpl.getUuid() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);
			}
		}

		entityCache.putResult(KhaliqCategoryMappingModelImpl.ENTITY_CACHE_ENABLED,
			KhaliqCategoryMappingImpl.class,
			khaliqCategoryMapping.getPrimaryKey(), khaliqCategoryMapping, false);

		khaliqCategoryMapping.resetOriginalValues();

		return khaliqCategoryMapping;
	}

	protected KhaliqCategoryMapping toUnwrappedModel(
		KhaliqCategoryMapping khaliqCategoryMapping) {
		if (khaliqCategoryMapping instanceof KhaliqCategoryMappingImpl) {
			return khaliqCategoryMapping;
		}

		KhaliqCategoryMappingImpl khaliqCategoryMappingImpl = new KhaliqCategoryMappingImpl();

		khaliqCategoryMappingImpl.setNew(khaliqCategoryMapping.isNew());
		khaliqCategoryMappingImpl.setPrimaryKey(khaliqCategoryMapping.getPrimaryKey());

		khaliqCategoryMappingImpl.setUuid(khaliqCategoryMapping.getUuid());
		khaliqCategoryMappingImpl.setKhaliqCategoryMappingId(khaliqCategoryMapping.getKhaliqCategoryMappingId());
		khaliqCategoryMappingImpl.setKhaliqId(khaliqCategoryMapping.getKhaliqId());
		khaliqCategoryMappingImpl.setCategoryId(khaliqCategoryMapping.getCategoryId());

		return khaliqCategoryMappingImpl;
	}

	/**
	 * Returns the khaliq category mapping with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the khaliq category mapping
	 * @return the khaliq category mapping
	 * @throws NoSuchKhaliqCategoryMappingException if a khaliq category mapping with the primary key could not be found
	 */
	@Override
	public KhaliqCategoryMapping findByPrimaryKey(Serializable primaryKey)
		throws NoSuchKhaliqCategoryMappingException {
		KhaliqCategoryMapping khaliqCategoryMapping = fetchByPrimaryKey(primaryKey);

		if (khaliqCategoryMapping == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchKhaliqCategoryMappingException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return khaliqCategoryMapping;
	}

	/**
	 * Returns the khaliq category mapping with the primary key or throws a {@link NoSuchKhaliqCategoryMappingException} if it could not be found.
	 *
	 * @param khaliqCategoryMappingId the primary key of the khaliq category mapping
	 * @return the khaliq category mapping
	 * @throws NoSuchKhaliqCategoryMappingException if a khaliq category mapping with the primary key could not be found
	 */
	@Override
	public KhaliqCategoryMapping findByPrimaryKey(long khaliqCategoryMappingId)
		throws NoSuchKhaliqCategoryMappingException {
		return findByPrimaryKey((Serializable)khaliqCategoryMappingId);
	}

	/**
	 * Returns the khaliq category mapping with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the khaliq category mapping
	 * @return the khaliq category mapping, or <code>null</code> if a khaliq category mapping with the primary key could not be found
	 */
	@Override
	public KhaliqCategoryMapping fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(KhaliqCategoryMappingModelImpl.ENTITY_CACHE_ENABLED,
				KhaliqCategoryMappingImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		KhaliqCategoryMapping khaliqCategoryMapping = (KhaliqCategoryMapping)serializable;

		if (khaliqCategoryMapping == null) {
			Session session = null;

			try {
				session = openSession();

				khaliqCategoryMapping = (KhaliqCategoryMapping)session.get(KhaliqCategoryMappingImpl.class,
						primaryKey);

				if (khaliqCategoryMapping != null) {
					cacheResult(khaliqCategoryMapping);
				}
				else {
					entityCache.putResult(KhaliqCategoryMappingModelImpl.ENTITY_CACHE_ENABLED,
						KhaliqCategoryMappingImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(KhaliqCategoryMappingModelImpl.ENTITY_CACHE_ENABLED,
					KhaliqCategoryMappingImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return khaliqCategoryMapping;
	}

	/**
	 * Returns the khaliq category mapping with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param khaliqCategoryMappingId the primary key of the khaliq category mapping
	 * @return the khaliq category mapping, or <code>null</code> if a khaliq category mapping with the primary key could not be found
	 */
	@Override
	public KhaliqCategoryMapping fetchByPrimaryKey(long khaliqCategoryMappingId) {
		return fetchByPrimaryKey((Serializable)khaliqCategoryMappingId);
	}

	@Override
	public Map<Serializable, KhaliqCategoryMapping> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, KhaliqCategoryMapping> map = new HashMap<Serializable, KhaliqCategoryMapping>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			KhaliqCategoryMapping khaliqCategoryMapping = fetchByPrimaryKey(primaryKey);

			if (khaliqCategoryMapping != null) {
				map.put(primaryKey, khaliqCategoryMapping);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(KhaliqCategoryMappingModelImpl.ENTITY_CACHE_ENABLED,
					KhaliqCategoryMappingImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (KhaliqCategoryMapping)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_KHALIQCATEGORYMAPPING_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append(String.valueOf(primaryKey));

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (KhaliqCategoryMapping khaliqCategoryMapping : (List<KhaliqCategoryMapping>)q.list()) {
				map.put(khaliqCategoryMapping.getPrimaryKeyObj(),
					khaliqCategoryMapping);

				cacheResult(khaliqCategoryMapping);

				uncachedPrimaryKeys.remove(khaliqCategoryMapping.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(KhaliqCategoryMappingModelImpl.ENTITY_CACHE_ENABLED,
					KhaliqCategoryMappingImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the khaliq category mappings.
	 *
	 * @return the khaliq category mappings
	 */
	@Override
	public List<KhaliqCategoryMapping> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the khaliq category mappings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link KhaliqCategoryMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of khaliq category mappings
	 * @param end the upper bound of the range of khaliq category mappings (not inclusive)
	 * @return the range of khaliq category mappings
	 */
	@Override
	public List<KhaliqCategoryMapping> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the khaliq category mappings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link KhaliqCategoryMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of khaliq category mappings
	 * @param end the upper bound of the range of khaliq category mappings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of khaliq category mappings
	 */
	@Override
	public List<KhaliqCategoryMapping> findAll(int start, int end,
		OrderByComparator<KhaliqCategoryMapping> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the khaliq category mappings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link KhaliqCategoryMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of khaliq category mappings
	 * @param end the upper bound of the range of khaliq category mappings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of khaliq category mappings
	 */
	@Override
	public List<KhaliqCategoryMapping> findAll(int start, int end,
		OrderByComparator<KhaliqCategoryMapping> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<KhaliqCategoryMapping> list = null;

		if (retrieveFromCache) {
			list = (List<KhaliqCategoryMapping>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_KHALIQCATEGORYMAPPING);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_KHALIQCATEGORYMAPPING;

				if (pagination) {
					sql = sql.concat(KhaliqCategoryMappingModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<KhaliqCategoryMapping>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<KhaliqCategoryMapping>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the khaliq category mappings from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (KhaliqCategoryMapping khaliqCategoryMapping : findAll()) {
			remove(khaliqCategoryMapping);
		}
	}

	/**
	 * Returns the number of khaliq category mappings.
	 *
	 * @return the number of khaliq category mappings
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_KHALIQCATEGORYMAPPING);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return KhaliqCategoryMappingModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the khaliq category mapping persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(KhaliqCategoryMappingImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_KHALIQCATEGORYMAPPING = "SELECT khaliqCategoryMapping FROM KhaliqCategoryMapping khaliqCategoryMapping";
	private static final String _SQL_SELECT_KHALIQCATEGORYMAPPING_WHERE_PKS_IN = "SELECT khaliqCategoryMapping FROM KhaliqCategoryMapping khaliqCategoryMapping WHERE khaliqCategoryMappingId IN (";
	private static final String _SQL_SELECT_KHALIQCATEGORYMAPPING_WHERE = "SELECT khaliqCategoryMapping FROM KhaliqCategoryMapping khaliqCategoryMapping WHERE ";
	private static final String _SQL_COUNT_KHALIQCATEGORYMAPPING = "SELECT COUNT(khaliqCategoryMapping) FROM KhaliqCategoryMapping khaliqCategoryMapping";
	private static final String _SQL_COUNT_KHALIQCATEGORYMAPPING_WHERE = "SELECT COUNT(khaliqCategoryMapping) FROM KhaliqCategoryMapping khaliqCategoryMapping WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "khaliqCategoryMapping.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No KhaliqCategoryMapping exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No KhaliqCategoryMapping exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(KhaliqCategoryMappingPersistenceImpl.class);
	private static final Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"uuid"
			});
}