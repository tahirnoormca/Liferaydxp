/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.tahir.motogplusservice.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import com.tahir.motogplusservice.model.KhaliqCategoryMapping;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing KhaliqCategoryMapping in entity cache.
 *
 * @author TahirNoor
 * @see KhaliqCategoryMapping
 * @generated
 */
@ProviderType
public class KhaliqCategoryMappingCacheModel implements CacheModel<KhaliqCategoryMapping>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof KhaliqCategoryMappingCacheModel)) {
			return false;
		}

		KhaliqCategoryMappingCacheModel khaliqCategoryMappingCacheModel = (KhaliqCategoryMappingCacheModel)obj;

		if (khaliqCategoryMappingId == khaliqCategoryMappingCacheModel.khaliqCategoryMappingId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, khaliqCategoryMappingId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", khaliqCategoryMappingId=");
		sb.append(khaliqCategoryMappingId);
		sb.append(", khaliqId=");
		sb.append(khaliqId);
		sb.append(", categoryId=");
		sb.append(categoryId);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public KhaliqCategoryMapping toEntityModel() {
		KhaliqCategoryMappingImpl khaliqCategoryMappingImpl = new KhaliqCategoryMappingImpl();

		if (uuid == null) {
			khaliqCategoryMappingImpl.setUuid(StringPool.BLANK);
		}
		else {
			khaliqCategoryMappingImpl.setUuid(uuid);
		}

		khaliqCategoryMappingImpl.setKhaliqCategoryMappingId(khaliqCategoryMappingId);
		khaliqCategoryMappingImpl.setKhaliqId(khaliqId);
		khaliqCategoryMappingImpl.setCategoryId(categoryId);

		khaliqCategoryMappingImpl.resetOriginalValues();

		return khaliqCategoryMappingImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		khaliqCategoryMappingId = objectInput.readLong();

		khaliqId = objectInput.readLong();

		categoryId = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(khaliqCategoryMappingId);

		objectOutput.writeLong(khaliqId);

		objectOutput.writeLong(categoryId);
	}

	public String uuid;
	public long khaliqCategoryMappingId;
	public long khaliqId;
	public long categoryId;
}