create index IX_A2213AE on moto_Category (uuid_[$COLUMN_LENGTH:75$]);

create index IX_37480ACE on moto_Industry (uuid_[$COLUMN_LENGTH:75$]);

create index IX_F222F977 on moto_Khaliq (name[$COLUMN_LENGTH:75$]);
create index IX_6F7A57AC on moto_Khaliq (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_A8CCD92E on moto_Khaliq (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_E408C1D0 on moto_KhaliqCategoryMapping (uuid_[$COLUMN_LENGTH:75$]);

create index IX_D1B486B0 on moto_KhaliqIndustryMapping (uuid_[$COLUMN_LENGTH:75$]);