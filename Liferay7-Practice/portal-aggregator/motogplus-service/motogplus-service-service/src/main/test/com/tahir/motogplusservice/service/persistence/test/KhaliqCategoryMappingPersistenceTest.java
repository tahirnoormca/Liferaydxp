/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.tahir.motogplusservice.service.persistence.test;

import com.liferay.arquillian.extension.junit.bridge.junit.Arquillian;

import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.test.rule.AggregateTestRule;
import com.liferay.portal.kernel.test.rule.TransactionalTestRule;
import com.liferay.portal.kernel.test.util.RandomTestUtil;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.util.IntegerWrapper;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.OrderByComparatorFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.test.rule.LiferayIntegrationTestRule;
import com.liferay.portal.test.rule.PersistenceTestRule;

import com.tahir.motogplusservice.exception.NoSuchKhaliqCategoryMappingException;
import com.tahir.motogplusservice.model.KhaliqCategoryMapping;
import com.tahir.motogplusservice.service.KhaliqCategoryMappingLocalServiceUtil;
import com.tahir.motogplusservice.service.persistence.KhaliqCategoryMappingPersistence;
import com.tahir.motogplusservice.service.persistence.KhaliqCategoryMappingUtil;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

import org.junit.runner.RunWith;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @generated
 */
@RunWith(Arquillian.class)
public class KhaliqCategoryMappingPersistenceTest {
	@ClassRule
	@Rule
	public static final AggregateTestRule aggregateTestRule = new AggregateTestRule(new LiferayIntegrationTestRule(),
			PersistenceTestRule.INSTANCE,
			new TransactionalTestRule(Propagation.REQUIRED));

	@Before
	public void setUp() {
		_persistence = KhaliqCategoryMappingUtil.getPersistence();

		Class<?> clazz = _persistence.getClass();

		_dynamicQueryClassLoader = clazz.getClassLoader();
	}

	@After
	public void tearDown() throws Exception {
		Iterator<KhaliqCategoryMapping> iterator = _khaliqCategoryMappings.iterator();

		while (iterator.hasNext()) {
			_persistence.remove(iterator.next());

			iterator.remove();
		}
	}

	@Test
	public void testCreate() throws Exception {
		long pk = RandomTestUtil.nextLong();

		KhaliqCategoryMapping khaliqCategoryMapping = _persistence.create(pk);

		Assert.assertNotNull(khaliqCategoryMapping);

		Assert.assertEquals(khaliqCategoryMapping.getPrimaryKey(), pk);
	}

	@Test
	public void testRemove() throws Exception {
		KhaliqCategoryMapping newKhaliqCategoryMapping = addKhaliqCategoryMapping();

		_persistence.remove(newKhaliqCategoryMapping);

		KhaliqCategoryMapping existingKhaliqCategoryMapping = _persistence.fetchByPrimaryKey(newKhaliqCategoryMapping.getPrimaryKey());

		Assert.assertNull(existingKhaliqCategoryMapping);
	}

	@Test
	public void testUpdateNew() throws Exception {
		addKhaliqCategoryMapping();
	}

	@Test
	public void testUpdateExisting() throws Exception {
		long pk = RandomTestUtil.nextLong();

		KhaliqCategoryMapping newKhaliqCategoryMapping = _persistence.create(pk);

		newKhaliqCategoryMapping.setUuid(RandomTestUtil.randomString());

		newKhaliqCategoryMapping.setKhaliqId(RandomTestUtil.nextLong());

		newKhaliqCategoryMapping.setCategoryId(RandomTestUtil.nextLong());

		_khaliqCategoryMappings.add(_persistence.update(
				newKhaliqCategoryMapping));

		KhaliqCategoryMapping existingKhaliqCategoryMapping = _persistence.findByPrimaryKey(newKhaliqCategoryMapping.getPrimaryKey());

		Assert.assertEquals(existingKhaliqCategoryMapping.getUuid(),
			newKhaliqCategoryMapping.getUuid());
		Assert.assertEquals(existingKhaliqCategoryMapping.getKhaliqCategoryMappingId(),
			newKhaliqCategoryMapping.getKhaliqCategoryMappingId());
		Assert.assertEquals(existingKhaliqCategoryMapping.getKhaliqId(),
			newKhaliqCategoryMapping.getKhaliqId());
		Assert.assertEquals(existingKhaliqCategoryMapping.getCategoryId(),
			newKhaliqCategoryMapping.getCategoryId());
	}

	@Test
	public void testCountByUuid() throws Exception {
		_persistence.countByUuid(StringPool.BLANK);

		_persistence.countByUuid(StringPool.NULL);

		_persistence.countByUuid((String)null);
	}

	@Test
	public void testFindByPrimaryKeyExisting() throws Exception {
		KhaliqCategoryMapping newKhaliqCategoryMapping = addKhaliqCategoryMapping();

		KhaliqCategoryMapping existingKhaliqCategoryMapping = _persistence.findByPrimaryKey(newKhaliqCategoryMapping.getPrimaryKey());

		Assert.assertEquals(existingKhaliqCategoryMapping,
			newKhaliqCategoryMapping);
	}

	@Test(expected = NoSuchKhaliqCategoryMappingException.class)
	public void testFindByPrimaryKeyMissing() throws Exception {
		long pk = RandomTestUtil.nextLong();

		_persistence.findByPrimaryKey(pk);
	}

	@Test
	public void testFindAll() throws Exception {
		_persistence.findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			getOrderByComparator());
	}

	protected OrderByComparator<KhaliqCategoryMapping> getOrderByComparator() {
		return OrderByComparatorFactoryUtil.create("moto_KhaliqCategoryMapping",
			"uuid", true, "khaliqCategoryMappingId", true, "khaliqId", true,
			"categoryId", true);
	}

	@Test
	public void testFetchByPrimaryKeyExisting() throws Exception {
		KhaliqCategoryMapping newKhaliqCategoryMapping = addKhaliqCategoryMapping();

		KhaliqCategoryMapping existingKhaliqCategoryMapping = _persistence.fetchByPrimaryKey(newKhaliqCategoryMapping.getPrimaryKey());

		Assert.assertEquals(existingKhaliqCategoryMapping,
			newKhaliqCategoryMapping);
	}

	@Test
	public void testFetchByPrimaryKeyMissing() throws Exception {
		long pk = RandomTestUtil.nextLong();

		KhaliqCategoryMapping missingKhaliqCategoryMapping = _persistence.fetchByPrimaryKey(pk);

		Assert.assertNull(missingKhaliqCategoryMapping);
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereAllPrimaryKeysExist()
		throws Exception {
		KhaliqCategoryMapping newKhaliqCategoryMapping1 = addKhaliqCategoryMapping();
		KhaliqCategoryMapping newKhaliqCategoryMapping2 = addKhaliqCategoryMapping();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newKhaliqCategoryMapping1.getPrimaryKey());
		primaryKeys.add(newKhaliqCategoryMapping2.getPrimaryKey());

		Map<Serializable, KhaliqCategoryMapping> khaliqCategoryMappings = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(2, khaliqCategoryMappings.size());
		Assert.assertEquals(newKhaliqCategoryMapping1,
			khaliqCategoryMappings.get(
				newKhaliqCategoryMapping1.getPrimaryKey()));
		Assert.assertEquals(newKhaliqCategoryMapping2,
			khaliqCategoryMappings.get(
				newKhaliqCategoryMapping2.getPrimaryKey()));
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereNoPrimaryKeysExist()
		throws Exception {
		long pk1 = RandomTestUtil.nextLong();

		long pk2 = RandomTestUtil.nextLong();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(pk1);
		primaryKeys.add(pk2);

		Map<Serializable, KhaliqCategoryMapping> khaliqCategoryMappings = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertTrue(khaliqCategoryMappings.isEmpty());
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereSomePrimaryKeysExist()
		throws Exception {
		KhaliqCategoryMapping newKhaliqCategoryMapping = addKhaliqCategoryMapping();

		long pk = RandomTestUtil.nextLong();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newKhaliqCategoryMapping.getPrimaryKey());
		primaryKeys.add(pk);

		Map<Serializable, KhaliqCategoryMapping> khaliqCategoryMappings = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(1, khaliqCategoryMappings.size());
		Assert.assertEquals(newKhaliqCategoryMapping,
			khaliqCategoryMappings.get(newKhaliqCategoryMapping.getPrimaryKey()));
	}

	@Test
	public void testFetchByPrimaryKeysWithNoPrimaryKeys()
		throws Exception {
		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		Map<Serializable, KhaliqCategoryMapping> khaliqCategoryMappings = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertTrue(khaliqCategoryMappings.isEmpty());
	}

	@Test
	public void testFetchByPrimaryKeysWithOnePrimaryKey()
		throws Exception {
		KhaliqCategoryMapping newKhaliqCategoryMapping = addKhaliqCategoryMapping();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newKhaliqCategoryMapping.getPrimaryKey());

		Map<Serializable, KhaliqCategoryMapping> khaliqCategoryMappings = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(1, khaliqCategoryMappings.size());
		Assert.assertEquals(newKhaliqCategoryMapping,
			khaliqCategoryMappings.get(newKhaliqCategoryMapping.getPrimaryKey()));
	}

	@Test
	public void testActionableDynamicQuery() throws Exception {
		final IntegerWrapper count = new IntegerWrapper();

		ActionableDynamicQuery actionableDynamicQuery = KhaliqCategoryMappingLocalServiceUtil.getActionableDynamicQuery();

		actionableDynamicQuery.setPerformActionMethod(new ActionableDynamicQuery.PerformActionMethod<KhaliqCategoryMapping>() {
				@Override
				public void performAction(
					KhaliqCategoryMapping khaliqCategoryMapping) {
					Assert.assertNotNull(khaliqCategoryMapping);

					count.increment();
				}
			});

		actionableDynamicQuery.performActions();

		Assert.assertEquals(count.getValue(), _persistence.countAll());
	}

	@Test
	public void testDynamicQueryByPrimaryKeyExisting()
		throws Exception {
		KhaliqCategoryMapping newKhaliqCategoryMapping = addKhaliqCategoryMapping();

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(KhaliqCategoryMapping.class,
				_dynamicQueryClassLoader);

		dynamicQuery.add(RestrictionsFactoryUtil.eq("khaliqCategoryMappingId",
				newKhaliqCategoryMapping.getKhaliqCategoryMappingId()));

		List<KhaliqCategoryMapping> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(1, result.size());

		KhaliqCategoryMapping existingKhaliqCategoryMapping = result.get(0);

		Assert.assertEquals(existingKhaliqCategoryMapping,
			newKhaliqCategoryMapping);
	}

	@Test
	public void testDynamicQueryByPrimaryKeyMissing() throws Exception {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(KhaliqCategoryMapping.class,
				_dynamicQueryClassLoader);

		dynamicQuery.add(RestrictionsFactoryUtil.eq("khaliqCategoryMappingId",
				RandomTestUtil.nextLong()));

		List<KhaliqCategoryMapping> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(0, result.size());
	}

	@Test
	public void testDynamicQueryByProjectionExisting()
		throws Exception {
		KhaliqCategoryMapping newKhaliqCategoryMapping = addKhaliqCategoryMapping();

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(KhaliqCategoryMapping.class,
				_dynamicQueryClassLoader);

		dynamicQuery.setProjection(ProjectionFactoryUtil.property(
				"khaliqCategoryMappingId"));

		Object newKhaliqCategoryMappingId = newKhaliqCategoryMapping.getKhaliqCategoryMappingId();

		dynamicQuery.add(RestrictionsFactoryUtil.in("khaliqCategoryMappingId",
				new Object[] { newKhaliqCategoryMappingId }));

		List<Object> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(1, result.size());

		Object existingKhaliqCategoryMappingId = result.get(0);

		Assert.assertEquals(existingKhaliqCategoryMappingId,
			newKhaliqCategoryMappingId);
	}

	@Test
	public void testDynamicQueryByProjectionMissing() throws Exception {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(KhaliqCategoryMapping.class,
				_dynamicQueryClassLoader);

		dynamicQuery.setProjection(ProjectionFactoryUtil.property(
				"khaliqCategoryMappingId"));

		dynamicQuery.add(RestrictionsFactoryUtil.in("khaliqCategoryMappingId",
				new Object[] { RandomTestUtil.nextLong() }));

		List<Object> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(0, result.size());
	}

	protected KhaliqCategoryMapping addKhaliqCategoryMapping()
		throws Exception {
		long pk = RandomTestUtil.nextLong();

		KhaliqCategoryMapping khaliqCategoryMapping = _persistence.create(pk);

		khaliqCategoryMapping.setUuid(RandomTestUtil.randomString());

		khaliqCategoryMapping.setKhaliqId(RandomTestUtil.nextLong());

		khaliqCategoryMapping.setCategoryId(RandomTestUtil.nextLong());

		_khaliqCategoryMappings.add(_persistence.update(khaliqCategoryMapping));

		return khaliqCategoryMapping;
	}

	private List<KhaliqCategoryMapping> _khaliqCategoryMappings = new ArrayList<KhaliqCategoryMapping>();
	private KhaliqCategoryMappingPersistence _persistence;
	private ClassLoader _dynamicQueryClassLoader;
}