/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.tahir.motogplusservice.service.persistence.test;

import com.liferay.arquillian.extension.junit.bridge.junit.Arquillian;

import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.test.rule.AggregateTestRule;
import com.liferay.portal.kernel.test.rule.TransactionalTestRule;
import com.liferay.portal.kernel.test.util.RandomTestUtil;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.util.IntegerWrapper;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.OrderByComparatorFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.test.rule.LiferayIntegrationTestRule;
import com.liferay.portal.test.rule.PersistenceTestRule;

import com.tahir.motogplusservice.exception.NoSuchIndustryException;
import com.tahir.motogplusservice.model.Industry;
import com.tahir.motogplusservice.service.IndustryLocalServiceUtil;
import com.tahir.motogplusservice.service.persistence.IndustryPersistence;
import com.tahir.motogplusservice.service.persistence.IndustryUtil;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

import org.junit.runner.RunWith;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @generated
 */
@RunWith(Arquillian.class)
public class IndustryPersistenceTest {
	@ClassRule
	@Rule
	public static final AggregateTestRule aggregateTestRule = new AggregateTestRule(new LiferayIntegrationTestRule(),
			PersistenceTestRule.INSTANCE,
			new TransactionalTestRule(Propagation.REQUIRED));

	@Before
	public void setUp() {
		_persistence = IndustryUtil.getPersistence();

		Class<?> clazz = _persistence.getClass();

		_dynamicQueryClassLoader = clazz.getClassLoader();
	}

	@After
	public void tearDown() throws Exception {
		Iterator<Industry> iterator = _industries.iterator();

		while (iterator.hasNext()) {
			_persistence.remove(iterator.next());

			iterator.remove();
		}
	}

	@Test
	public void testCreate() throws Exception {
		long pk = RandomTestUtil.nextLong();

		Industry industry = _persistence.create(pk);

		Assert.assertNotNull(industry);

		Assert.assertEquals(industry.getPrimaryKey(), pk);
	}

	@Test
	public void testRemove() throws Exception {
		Industry newIndustry = addIndustry();

		_persistence.remove(newIndustry);

		Industry existingIndustry = _persistence.fetchByPrimaryKey(newIndustry.getPrimaryKey());

		Assert.assertNull(existingIndustry);
	}

	@Test
	public void testUpdateNew() throws Exception {
		addIndustry();
	}

	@Test
	public void testUpdateExisting() throws Exception {
		long pk = RandomTestUtil.nextLong();

		Industry newIndustry = _persistence.create(pk);

		newIndustry.setUuid(RandomTestUtil.randomString());

		newIndustry.setIndustryName(RandomTestUtil.randomString());

		_industries.add(_persistence.update(newIndustry));

		Industry existingIndustry = _persistence.findByPrimaryKey(newIndustry.getPrimaryKey());

		Assert.assertEquals(existingIndustry.getUuid(), newIndustry.getUuid());
		Assert.assertEquals(existingIndustry.getIndustryId(),
			newIndustry.getIndustryId());
		Assert.assertEquals(existingIndustry.getIndustryName(),
			newIndustry.getIndustryName());
	}

	@Test
	public void testCountByUuid() throws Exception {
		_persistence.countByUuid(StringPool.BLANK);

		_persistence.countByUuid(StringPool.NULL);

		_persistence.countByUuid((String)null);
	}

	@Test
	public void testFindByPrimaryKeyExisting() throws Exception {
		Industry newIndustry = addIndustry();

		Industry existingIndustry = _persistence.findByPrimaryKey(newIndustry.getPrimaryKey());

		Assert.assertEquals(existingIndustry, newIndustry);
	}

	@Test(expected = NoSuchIndustryException.class)
	public void testFindByPrimaryKeyMissing() throws Exception {
		long pk = RandomTestUtil.nextLong();

		_persistence.findByPrimaryKey(pk);
	}

	@Test
	public void testFindAll() throws Exception {
		_persistence.findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			getOrderByComparator());
	}

	protected OrderByComparator<Industry> getOrderByComparator() {
		return OrderByComparatorFactoryUtil.create("moto_Industry", "uuid",
			true, "industryId", true, "industryName", true);
	}

	@Test
	public void testFetchByPrimaryKeyExisting() throws Exception {
		Industry newIndustry = addIndustry();

		Industry existingIndustry = _persistence.fetchByPrimaryKey(newIndustry.getPrimaryKey());

		Assert.assertEquals(existingIndustry, newIndustry);
	}

	@Test
	public void testFetchByPrimaryKeyMissing() throws Exception {
		long pk = RandomTestUtil.nextLong();

		Industry missingIndustry = _persistence.fetchByPrimaryKey(pk);

		Assert.assertNull(missingIndustry);
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereAllPrimaryKeysExist()
		throws Exception {
		Industry newIndustry1 = addIndustry();
		Industry newIndustry2 = addIndustry();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newIndustry1.getPrimaryKey());
		primaryKeys.add(newIndustry2.getPrimaryKey());

		Map<Serializable, Industry> industries = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(2, industries.size());
		Assert.assertEquals(newIndustry1,
			industries.get(newIndustry1.getPrimaryKey()));
		Assert.assertEquals(newIndustry2,
			industries.get(newIndustry2.getPrimaryKey()));
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereNoPrimaryKeysExist()
		throws Exception {
		long pk1 = RandomTestUtil.nextLong();

		long pk2 = RandomTestUtil.nextLong();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(pk1);
		primaryKeys.add(pk2);

		Map<Serializable, Industry> industries = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertTrue(industries.isEmpty());
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereSomePrimaryKeysExist()
		throws Exception {
		Industry newIndustry = addIndustry();

		long pk = RandomTestUtil.nextLong();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newIndustry.getPrimaryKey());
		primaryKeys.add(pk);

		Map<Serializable, Industry> industries = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(1, industries.size());
		Assert.assertEquals(newIndustry,
			industries.get(newIndustry.getPrimaryKey()));
	}

	@Test
	public void testFetchByPrimaryKeysWithNoPrimaryKeys()
		throws Exception {
		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		Map<Serializable, Industry> industries = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertTrue(industries.isEmpty());
	}

	@Test
	public void testFetchByPrimaryKeysWithOnePrimaryKey()
		throws Exception {
		Industry newIndustry = addIndustry();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newIndustry.getPrimaryKey());

		Map<Serializable, Industry> industries = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(1, industries.size());
		Assert.assertEquals(newIndustry,
			industries.get(newIndustry.getPrimaryKey()));
	}

	@Test
	public void testActionableDynamicQuery() throws Exception {
		final IntegerWrapper count = new IntegerWrapper();

		ActionableDynamicQuery actionableDynamicQuery = IndustryLocalServiceUtil.getActionableDynamicQuery();

		actionableDynamicQuery.setPerformActionMethod(new ActionableDynamicQuery.PerformActionMethod<Industry>() {
				@Override
				public void performAction(Industry industry) {
					Assert.assertNotNull(industry);

					count.increment();
				}
			});

		actionableDynamicQuery.performActions();

		Assert.assertEquals(count.getValue(), _persistence.countAll());
	}

	@Test
	public void testDynamicQueryByPrimaryKeyExisting()
		throws Exception {
		Industry newIndustry = addIndustry();

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Industry.class,
				_dynamicQueryClassLoader);

		dynamicQuery.add(RestrictionsFactoryUtil.eq("industryId",
				newIndustry.getIndustryId()));

		List<Industry> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(1, result.size());

		Industry existingIndustry = result.get(0);

		Assert.assertEquals(existingIndustry, newIndustry);
	}

	@Test
	public void testDynamicQueryByPrimaryKeyMissing() throws Exception {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Industry.class,
				_dynamicQueryClassLoader);

		dynamicQuery.add(RestrictionsFactoryUtil.eq("industryId",
				RandomTestUtil.nextLong()));

		List<Industry> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(0, result.size());
	}

	@Test
	public void testDynamicQueryByProjectionExisting()
		throws Exception {
		Industry newIndustry = addIndustry();

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Industry.class,
				_dynamicQueryClassLoader);

		dynamicQuery.setProjection(ProjectionFactoryUtil.property("industryId"));

		Object newIndustryId = newIndustry.getIndustryId();

		dynamicQuery.add(RestrictionsFactoryUtil.in("industryId",
				new Object[] { newIndustryId }));

		List<Object> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(1, result.size());

		Object existingIndustryId = result.get(0);

		Assert.assertEquals(existingIndustryId, newIndustryId);
	}

	@Test
	public void testDynamicQueryByProjectionMissing() throws Exception {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Industry.class,
				_dynamicQueryClassLoader);

		dynamicQuery.setProjection(ProjectionFactoryUtil.property("industryId"));

		dynamicQuery.add(RestrictionsFactoryUtil.in("industryId",
				new Object[] { RandomTestUtil.nextLong() }));

		List<Object> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(0, result.size());
	}

	protected Industry addIndustry() throws Exception {
		long pk = RandomTestUtil.nextLong();

		Industry industry = _persistence.create(pk);

		industry.setUuid(RandomTestUtil.randomString());

		industry.setIndustryName(RandomTestUtil.randomString());

		_industries.add(_persistence.update(industry));

		return industry;
	}

	private List<Industry> _industries = new ArrayList<Industry>();
	private IndustryPersistence _persistence;
	private ClassLoader _dynamicQueryClassLoader;
}