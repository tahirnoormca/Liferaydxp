/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.tahir.motogplusservice.service.persistence.test;

import com.liferay.arquillian.extension.junit.bridge.junit.Arquillian;

import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.test.rule.AggregateTestRule;
import com.liferay.portal.kernel.test.rule.TransactionalTestRule;
import com.liferay.portal.kernel.test.util.RandomTestUtil;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.util.IntegerWrapper;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.OrderByComparatorFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.test.rule.LiferayIntegrationTestRule;
import com.liferay.portal.test.rule.PersistenceTestRule;

import com.tahir.motogplusservice.exception.NoSuchKhaliqIndustryMappingException;
import com.tahir.motogplusservice.model.KhaliqIndustryMapping;
import com.tahir.motogplusservice.service.KhaliqIndustryMappingLocalServiceUtil;
import com.tahir.motogplusservice.service.persistence.KhaliqIndustryMappingPersistence;
import com.tahir.motogplusservice.service.persistence.KhaliqIndustryMappingUtil;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

import org.junit.runner.RunWith;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @generated
 */
@RunWith(Arquillian.class)
public class KhaliqIndustryMappingPersistenceTest {
	@ClassRule
	@Rule
	public static final AggregateTestRule aggregateTestRule = new AggregateTestRule(new LiferayIntegrationTestRule(),
			PersistenceTestRule.INSTANCE,
			new TransactionalTestRule(Propagation.REQUIRED));

	@Before
	public void setUp() {
		_persistence = KhaliqIndustryMappingUtil.getPersistence();

		Class<?> clazz = _persistence.getClass();

		_dynamicQueryClassLoader = clazz.getClassLoader();
	}

	@After
	public void tearDown() throws Exception {
		Iterator<KhaliqIndustryMapping> iterator = _khaliqIndustryMappings.iterator();

		while (iterator.hasNext()) {
			_persistence.remove(iterator.next());

			iterator.remove();
		}
	}

	@Test
	public void testCreate() throws Exception {
		long pk = RandomTestUtil.nextLong();

		KhaliqIndustryMapping khaliqIndustryMapping = _persistence.create(pk);

		Assert.assertNotNull(khaliqIndustryMapping);

		Assert.assertEquals(khaliqIndustryMapping.getPrimaryKey(), pk);
	}

	@Test
	public void testRemove() throws Exception {
		KhaliqIndustryMapping newKhaliqIndustryMapping = addKhaliqIndustryMapping();

		_persistence.remove(newKhaliqIndustryMapping);

		KhaliqIndustryMapping existingKhaliqIndustryMapping = _persistence.fetchByPrimaryKey(newKhaliqIndustryMapping.getPrimaryKey());

		Assert.assertNull(existingKhaliqIndustryMapping);
	}

	@Test
	public void testUpdateNew() throws Exception {
		addKhaliqIndustryMapping();
	}

	@Test
	public void testUpdateExisting() throws Exception {
		long pk = RandomTestUtil.nextLong();

		KhaliqIndustryMapping newKhaliqIndustryMapping = _persistence.create(pk);

		newKhaliqIndustryMapping.setUuid(RandomTestUtil.randomString());

		newKhaliqIndustryMapping.setKhaliqId(RandomTestUtil.nextLong());

		newKhaliqIndustryMapping.setIndustryId(RandomTestUtil.nextLong());

		_khaliqIndustryMappings.add(_persistence.update(
				newKhaliqIndustryMapping));

		KhaliqIndustryMapping existingKhaliqIndustryMapping = _persistence.findByPrimaryKey(newKhaliqIndustryMapping.getPrimaryKey());

		Assert.assertEquals(existingKhaliqIndustryMapping.getUuid(),
			newKhaliqIndustryMapping.getUuid());
		Assert.assertEquals(existingKhaliqIndustryMapping.getKhaliqIndustryMappingId(),
			newKhaliqIndustryMapping.getKhaliqIndustryMappingId());
		Assert.assertEquals(existingKhaliqIndustryMapping.getKhaliqId(),
			newKhaliqIndustryMapping.getKhaliqId());
		Assert.assertEquals(existingKhaliqIndustryMapping.getIndustryId(),
			newKhaliqIndustryMapping.getIndustryId());
	}

	@Test
	public void testCountByUuid() throws Exception {
		_persistence.countByUuid(StringPool.BLANK);

		_persistence.countByUuid(StringPool.NULL);

		_persistence.countByUuid((String)null);
	}

	@Test
	public void testFindByPrimaryKeyExisting() throws Exception {
		KhaliqIndustryMapping newKhaliqIndustryMapping = addKhaliqIndustryMapping();

		KhaliqIndustryMapping existingKhaliqIndustryMapping = _persistence.findByPrimaryKey(newKhaliqIndustryMapping.getPrimaryKey());

		Assert.assertEquals(existingKhaliqIndustryMapping,
			newKhaliqIndustryMapping);
	}

	@Test(expected = NoSuchKhaliqIndustryMappingException.class)
	public void testFindByPrimaryKeyMissing() throws Exception {
		long pk = RandomTestUtil.nextLong();

		_persistence.findByPrimaryKey(pk);
	}

	@Test
	public void testFindAll() throws Exception {
		_persistence.findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			getOrderByComparator());
	}

	protected OrderByComparator<KhaliqIndustryMapping> getOrderByComparator() {
		return OrderByComparatorFactoryUtil.create("moto_KhaliqIndustryMapping",
			"uuid", true, "khaliqIndustryMappingId", true, "khaliqId", true,
			"industryId", true);
	}

	@Test
	public void testFetchByPrimaryKeyExisting() throws Exception {
		KhaliqIndustryMapping newKhaliqIndustryMapping = addKhaliqIndustryMapping();

		KhaliqIndustryMapping existingKhaliqIndustryMapping = _persistence.fetchByPrimaryKey(newKhaliqIndustryMapping.getPrimaryKey());

		Assert.assertEquals(existingKhaliqIndustryMapping,
			newKhaliqIndustryMapping);
	}

	@Test
	public void testFetchByPrimaryKeyMissing() throws Exception {
		long pk = RandomTestUtil.nextLong();

		KhaliqIndustryMapping missingKhaliqIndustryMapping = _persistence.fetchByPrimaryKey(pk);

		Assert.assertNull(missingKhaliqIndustryMapping);
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereAllPrimaryKeysExist()
		throws Exception {
		KhaliqIndustryMapping newKhaliqIndustryMapping1 = addKhaliqIndustryMapping();
		KhaliqIndustryMapping newKhaliqIndustryMapping2 = addKhaliqIndustryMapping();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newKhaliqIndustryMapping1.getPrimaryKey());
		primaryKeys.add(newKhaliqIndustryMapping2.getPrimaryKey());

		Map<Serializable, KhaliqIndustryMapping> khaliqIndustryMappings = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(2, khaliqIndustryMappings.size());
		Assert.assertEquals(newKhaliqIndustryMapping1,
			khaliqIndustryMappings.get(
				newKhaliqIndustryMapping1.getPrimaryKey()));
		Assert.assertEquals(newKhaliqIndustryMapping2,
			khaliqIndustryMappings.get(
				newKhaliqIndustryMapping2.getPrimaryKey()));
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereNoPrimaryKeysExist()
		throws Exception {
		long pk1 = RandomTestUtil.nextLong();

		long pk2 = RandomTestUtil.nextLong();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(pk1);
		primaryKeys.add(pk2);

		Map<Serializable, KhaliqIndustryMapping> khaliqIndustryMappings = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertTrue(khaliqIndustryMappings.isEmpty());
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereSomePrimaryKeysExist()
		throws Exception {
		KhaliqIndustryMapping newKhaliqIndustryMapping = addKhaliqIndustryMapping();

		long pk = RandomTestUtil.nextLong();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newKhaliqIndustryMapping.getPrimaryKey());
		primaryKeys.add(pk);

		Map<Serializable, KhaliqIndustryMapping> khaliqIndustryMappings = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(1, khaliqIndustryMappings.size());
		Assert.assertEquals(newKhaliqIndustryMapping,
			khaliqIndustryMappings.get(newKhaliqIndustryMapping.getPrimaryKey()));
	}

	@Test
	public void testFetchByPrimaryKeysWithNoPrimaryKeys()
		throws Exception {
		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		Map<Serializable, KhaliqIndustryMapping> khaliqIndustryMappings = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertTrue(khaliqIndustryMappings.isEmpty());
	}

	@Test
	public void testFetchByPrimaryKeysWithOnePrimaryKey()
		throws Exception {
		KhaliqIndustryMapping newKhaliqIndustryMapping = addKhaliqIndustryMapping();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newKhaliqIndustryMapping.getPrimaryKey());

		Map<Serializable, KhaliqIndustryMapping> khaliqIndustryMappings = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(1, khaliqIndustryMappings.size());
		Assert.assertEquals(newKhaliqIndustryMapping,
			khaliqIndustryMappings.get(newKhaliqIndustryMapping.getPrimaryKey()));
	}

	@Test
	public void testActionableDynamicQuery() throws Exception {
		final IntegerWrapper count = new IntegerWrapper();

		ActionableDynamicQuery actionableDynamicQuery = KhaliqIndustryMappingLocalServiceUtil.getActionableDynamicQuery();

		actionableDynamicQuery.setPerformActionMethod(new ActionableDynamicQuery.PerformActionMethod<KhaliqIndustryMapping>() {
				@Override
				public void performAction(
					KhaliqIndustryMapping khaliqIndustryMapping) {
					Assert.assertNotNull(khaliqIndustryMapping);

					count.increment();
				}
			});

		actionableDynamicQuery.performActions();

		Assert.assertEquals(count.getValue(), _persistence.countAll());
	}

	@Test
	public void testDynamicQueryByPrimaryKeyExisting()
		throws Exception {
		KhaliqIndustryMapping newKhaliqIndustryMapping = addKhaliqIndustryMapping();

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(KhaliqIndustryMapping.class,
				_dynamicQueryClassLoader);

		dynamicQuery.add(RestrictionsFactoryUtil.eq("khaliqIndustryMappingId",
				newKhaliqIndustryMapping.getKhaliqIndustryMappingId()));

		List<KhaliqIndustryMapping> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(1, result.size());

		KhaliqIndustryMapping existingKhaliqIndustryMapping = result.get(0);

		Assert.assertEquals(existingKhaliqIndustryMapping,
			newKhaliqIndustryMapping);
	}

	@Test
	public void testDynamicQueryByPrimaryKeyMissing() throws Exception {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(KhaliqIndustryMapping.class,
				_dynamicQueryClassLoader);

		dynamicQuery.add(RestrictionsFactoryUtil.eq("khaliqIndustryMappingId",
				RandomTestUtil.nextLong()));

		List<KhaliqIndustryMapping> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(0, result.size());
	}

	@Test
	public void testDynamicQueryByProjectionExisting()
		throws Exception {
		KhaliqIndustryMapping newKhaliqIndustryMapping = addKhaliqIndustryMapping();

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(KhaliqIndustryMapping.class,
				_dynamicQueryClassLoader);

		dynamicQuery.setProjection(ProjectionFactoryUtil.property(
				"khaliqIndustryMappingId"));

		Object newKhaliqIndustryMappingId = newKhaliqIndustryMapping.getKhaliqIndustryMappingId();

		dynamicQuery.add(RestrictionsFactoryUtil.in("khaliqIndustryMappingId",
				new Object[] { newKhaliqIndustryMappingId }));

		List<Object> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(1, result.size());

		Object existingKhaliqIndustryMappingId = result.get(0);

		Assert.assertEquals(existingKhaliqIndustryMappingId,
			newKhaliqIndustryMappingId);
	}

	@Test
	public void testDynamicQueryByProjectionMissing() throws Exception {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(KhaliqIndustryMapping.class,
				_dynamicQueryClassLoader);

		dynamicQuery.setProjection(ProjectionFactoryUtil.property(
				"khaliqIndustryMappingId"));

		dynamicQuery.add(RestrictionsFactoryUtil.in("khaliqIndustryMappingId",
				new Object[] { RandomTestUtil.nextLong() }));

		List<Object> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(0, result.size());
	}

	protected KhaliqIndustryMapping addKhaliqIndustryMapping()
		throws Exception {
		long pk = RandomTestUtil.nextLong();

		KhaliqIndustryMapping khaliqIndustryMapping = _persistence.create(pk);

		khaliqIndustryMapping.setUuid(RandomTestUtil.randomString());

		khaliqIndustryMapping.setKhaliqId(RandomTestUtil.nextLong());

		khaliqIndustryMapping.setIndustryId(RandomTestUtil.nextLong());

		_khaliqIndustryMappings.add(_persistence.update(khaliqIndustryMapping));

		return khaliqIndustryMapping;
	}

	private List<KhaliqIndustryMapping> _khaliqIndustryMappings = new ArrayList<KhaliqIndustryMapping>();
	private KhaliqIndustryMappingPersistence _persistence;
	private ClassLoader _dynamicQueryClassLoader;
}