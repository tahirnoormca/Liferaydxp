package com.tahir.listener;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.exception.ModelListenerException;
import com.liferay.portal.kernel.model.BaseModelListener;
import com.liferay.portal.kernel.model.ModelListener;
import com.liferay.portal.kernel.model.User;

@Component(
        immediate = true,
        service = ModelListener.class
)
class CustomUserModelListener extends BaseModelListener<User> {
    @Override
    public void onAfterCreate(User model) throws ModelListenerException {
        System.out.println("After User Creation");
        super.onAfterCreate(model);
    }

    @Override
    public void onBeforeCreate(User model) throws ModelListenerException {
        System.out.println("Before User Creation");
        super.onBeforeCreate(model);
    }
}