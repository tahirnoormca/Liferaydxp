/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.tahir.customsql.service.persistence.test;

import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.test.ReflectionTestUtil;
import com.liferay.portal.kernel.test.rule.AggregateTestRule;
import com.liferay.portal.kernel.test.rule.TransactionalTestRule;
import com.liferay.portal.kernel.test.util.RandomTestUtil;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.util.IntegerWrapper;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.OrderByComparatorFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Time;
import com.liferay.portal.test.rule.LiferayIntegrationTestRule;
import com.liferay.portal.test.rule.PersistenceTestRule;

import com.tahir.customsql.exception.NoSuchTahirNoorKhaliqException;
import com.tahir.customsql.model.TahirNoorKhaliq;
import com.tahir.customsql.service.TahirNoorKhaliqLocalServiceUtil;
import com.tahir.customsql.service.persistence.TahirNoorKhaliqPersistence;
import com.tahir.customsql.service.persistence.TahirNoorKhaliqUtil;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * @generated
 */
public class TahirNoorKhaliqPersistenceTest {
	@ClassRule
	@Rule
	public static final AggregateTestRule aggregateTestRule = new AggregateTestRule(new LiferayIntegrationTestRule(),
			PersistenceTestRule.INSTANCE,
			new TransactionalTestRule(Propagation.REQUIRED));

	@Before
	public void setUp() {
		_persistence = TahirNoorKhaliqUtil.getPersistence();

		Class<?> clazz = _persistence.getClass();

		_dynamicQueryClassLoader = clazz.getClassLoader();
	}

	@After
	public void tearDown() throws Exception {
		Iterator<TahirNoorKhaliq> iterator = _tahirNoorKhaliqs.iterator();

		while (iterator.hasNext()) {
			_persistence.remove(iterator.next());

			iterator.remove();
		}
	}

	@Test
	public void testCreate() throws Exception {
		long pk = RandomTestUtil.nextLong();

		TahirNoorKhaliq tahirNoorKhaliq = _persistence.create(pk);

		Assert.assertNotNull(tahirNoorKhaliq);

		Assert.assertEquals(tahirNoorKhaliq.getPrimaryKey(), pk);
	}

	@Test
	public void testRemove() throws Exception {
		TahirNoorKhaliq newTahirNoorKhaliq = addTahirNoorKhaliq();

		_persistence.remove(newTahirNoorKhaliq);

		TahirNoorKhaliq existingTahirNoorKhaliq = _persistence.fetchByPrimaryKey(newTahirNoorKhaliq.getPrimaryKey());

		Assert.assertNull(existingTahirNoorKhaliq);
	}

	@Test
	public void testUpdateNew() throws Exception {
		addTahirNoorKhaliq();
	}

	@Test
	public void testUpdateExisting() throws Exception {
		long pk = RandomTestUtil.nextLong();

		TahirNoorKhaliq newTahirNoorKhaliq = _persistence.create(pk);

		newTahirNoorKhaliq.setUuid(RandomTestUtil.randomString());

		newTahirNoorKhaliq.setGroupId(RandomTestUtil.nextLong());

		newTahirNoorKhaliq.setCompanyId(RandomTestUtil.nextLong());

		newTahirNoorKhaliq.setUserId(RandomTestUtil.nextLong());

		newTahirNoorKhaliq.setUserName(RandomTestUtil.randomString());

		newTahirNoorKhaliq.setCreateDate(RandomTestUtil.nextDate());

		newTahirNoorKhaliq.setModifiedDate(RandomTestUtil.nextDate());

		newTahirNoorKhaliq.setName(RandomTestUtil.randomString());

		newTahirNoorKhaliq.setDob(RandomTestUtil.nextDate());

		_tahirNoorKhaliqs.add(_persistence.update(newTahirNoorKhaliq));

		TahirNoorKhaliq existingTahirNoorKhaliq = _persistence.findByPrimaryKey(newTahirNoorKhaliq.getPrimaryKey());

		Assert.assertEquals(existingTahirNoorKhaliq.getUuid(),
			newTahirNoorKhaliq.getUuid());
		Assert.assertEquals(existingTahirNoorKhaliq.getId(),
			newTahirNoorKhaliq.getId());
		Assert.assertEquals(existingTahirNoorKhaliq.getGroupId(),
			newTahirNoorKhaliq.getGroupId());
		Assert.assertEquals(existingTahirNoorKhaliq.getCompanyId(),
			newTahirNoorKhaliq.getCompanyId());
		Assert.assertEquals(existingTahirNoorKhaliq.getUserId(),
			newTahirNoorKhaliq.getUserId());
		Assert.assertEquals(existingTahirNoorKhaliq.getUserName(),
			newTahirNoorKhaliq.getUserName());
		Assert.assertEquals(Time.getShortTimestamp(
				existingTahirNoorKhaliq.getCreateDate()),
			Time.getShortTimestamp(newTahirNoorKhaliq.getCreateDate()));
		Assert.assertEquals(Time.getShortTimestamp(
				existingTahirNoorKhaliq.getModifiedDate()),
			Time.getShortTimestamp(newTahirNoorKhaliq.getModifiedDate()));
		Assert.assertEquals(existingTahirNoorKhaliq.getName(),
			newTahirNoorKhaliq.getName());
		Assert.assertEquals(Time.getShortTimestamp(
				existingTahirNoorKhaliq.getDob()),
			Time.getShortTimestamp(newTahirNoorKhaliq.getDob()));
	}

	@Test
	public void testCountByUuid() throws Exception {
		_persistence.countByUuid(StringPool.BLANK);

		_persistence.countByUuid(StringPool.NULL);

		_persistence.countByUuid((String)null);
	}

	@Test
	public void testCountByUUID_G() throws Exception {
		_persistence.countByUUID_G(StringPool.BLANK, RandomTestUtil.nextLong());

		_persistence.countByUUID_G(StringPool.NULL, 0L);

		_persistence.countByUUID_G((String)null, 0L);
	}

	@Test
	public void testCountByUuid_C() throws Exception {
		_persistence.countByUuid_C(StringPool.BLANK, RandomTestUtil.nextLong());

		_persistence.countByUuid_C(StringPool.NULL, 0L);

		_persistence.countByUuid_C((String)null, 0L);
	}

	@Test
	public void testFindByPrimaryKeyExisting() throws Exception {
		TahirNoorKhaliq newTahirNoorKhaliq = addTahirNoorKhaliq();

		TahirNoorKhaliq existingTahirNoorKhaliq = _persistence.findByPrimaryKey(newTahirNoorKhaliq.getPrimaryKey());

		Assert.assertEquals(existingTahirNoorKhaliq, newTahirNoorKhaliq);
	}

	@Test(expected = NoSuchTahirNoorKhaliqException.class)
	public void testFindByPrimaryKeyMissing() throws Exception {
		long pk = RandomTestUtil.nextLong();

		_persistence.findByPrimaryKey(pk);
	}

	@Test
	public void testFindAll() throws Exception {
		_persistence.findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			getOrderByComparator());
	}

	protected OrderByComparator<TahirNoorKhaliq> getOrderByComparator() {
		return OrderByComparatorFactoryUtil.create("AT_TahirNoorKhaliq",
			"uuid", true, "id", true, "groupId", true, "companyId", true,
			"userId", true, "userName", true, "createDate", true,
			"modifiedDate", true, "name", true, "dob", true);
	}

	@Test
	public void testFetchByPrimaryKeyExisting() throws Exception {
		TahirNoorKhaliq newTahirNoorKhaliq = addTahirNoorKhaliq();

		TahirNoorKhaliq existingTahirNoorKhaliq = _persistence.fetchByPrimaryKey(newTahirNoorKhaliq.getPrimaryKey());

		Assert.assertEquals(existingTahirNoorKhaliq, newTahirNoorKhaliq);
	}

	@Test
	public void testFetchByPrimaryKeyMissing() throws Exception {
		long pk = RandomTestUtil.nextLong();

		TahirNoorKhaliq missingTahirNoorKhaliq = _persistence.fetchByPrimaryKey(pk);

		Assert.assertNull(missingTahirNoorKhaliq);
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereAllPrimaryKeysExist()
		throws Exception {
		TahirNoorKhaliq newTahirNoorKhaliq1 = addTahirNoorKhaliq();
		TahirNoorKhaliq newTahirNoorKhaliq2 = addTahirNoorKhaliq();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newTahirNoorKhaliq1.getPrimaryKey());
		primaryKeys.add(newTahirNoorKhaliq2.getPrimaryKey());

		Map<Serializable, TahirNoorKhaliq> tahirNoorKhaliqs = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(2, tahirNoorKhaliqs.size());
		Assert.assertEquals(newTahirNoorKhaliq1,
			tahirNoorKhaliqs.get(newTahirNoorKhaliq1.getPrimaryKey()));
		Assert.assertEquals(newTahirNoorKhaliq2,
			tahirNoorKhaliqs.get(newTahirNoorKhaliq2.getPrimaryKey()));
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereNoPrimaryKeysExist()
		throws Exception {
		long pk1 = RandomTestUtil.nextLong();

		long pk2 = RandomTestUtil.nextLong();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(pk1);
		primaryKeys.add(pk2);

		Map<Serializable, TahirNoorKhaliq> tahirNoorKhaliqs = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertTrue(tahirNoorKhaliqs.isEmpty());
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereSomePrimaryKeysExist()
		throws Exception {
		TahirNoorKhaliq newTahirNoorKhaliq = addTahirNoorKhaliq();

		long pk = RandomTestUtil.nextLong();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newTahirNoorKhaliq.getPrimaryKey());
		primaryKeys.add(pk);

		Map<Serializable, TahirNoorKhaliq> tahirNoorKhaliqs = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(1, tahirNoorKhaliqs.size());
		Assert.assertEquals(newTahirNoorKhaliq,
			tahirNoorKhaliqs.get(newTahirNoorKhaliq.getPrimaryKey()));
	}

	@Test
	public void testFetchByPrimaryKeysWithNoPrimaryKeys()
		throws Exception {
		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		Map<Serializable, TahirNoorKhaliq> tahirNoorKhaliqs = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertTrue(tahirNoorKhaliqs.isEmpty());
	}

	@Test
	public void testFetchByPrimaryKeysWithOnePrimaryKey()
		throws Exception {
		TahirNoorKhaliq newTahirNoorKhaliq = addTahirNoorKhaliq();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newTahirNoorKhaliq.getPrimaryKey());

		Map<Serializable, TahirNoorKhaliq> tahirNoorKhaliqs = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(1, tahirNoorKhaliqs.size());
		Assert.assertEquals(newTahirNoorKhaliq,
			tahirNoorKhaliqs.get(newTahirNoorKhaliq.getPrimaryKey()));
	}

	@Test
	public void testActionableDynamicQuery() throws Exception {
		final IntegerWrapper count = new IntegerWrapper();

		ActionableDynamicQuery actionableDynamicQuery = TahirNoorKhaliqLocalServiceUtil.getActionableDynamicQuery();

		actionableDynamicQuery.setPerformActionMethod(new ActionableDynamicQuery.PerformActionMethod<TahirNoorKhaliq>() {
				@Override
				public void performAction(TahirNoorKhaliq tahirNoorKhaliq) {
					Assert.assertNotNull(tahirNoorKhaliq);

					count.increment();
				}
			});

		actionableDynamicQuery.performActions();

		Assert.assertEquals(count.getValue(), _persistence.countAll());
	}

	@Test
	public void testDynamicQueryByPrimaryKeyExisting()
		throws Exception {
		TahirNoorKhaliq newTahirNoorKhaliq = addTahirNoorKhaliq();

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(TahirNoorKhaliq.class,
				_dynamicQueryClassLoader);

		dynamicQuery.add(RestrictionsFactoryUtil.eq("id",
				newTahirNoorKhaliq.getId()));

		List<TahirNoorKhaliq> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(1, result.size());

		TahirNoorKhaliq existingTahirNoorKhaliq = result.get(0);

		Assert.assertEquals(existingTahirNoorKhaliq, newTahirNoorKhaliq);
	}

	@Test
	public void testDynamicQueryByPrimaryKeyMissing() throws Exception {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(TahirNoorKhaliq.class,
				_dynamicQueryClassLoader);

		dynamicQuery.add(RestrictionsFactoryUtil.eq("id",
				RandomTestUtil.nextLong()));

		List<TahirNoorKhaliq> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(0, result.size());
	}

	@Test
	public void testDynamicQueryByProjectionExisting()
		throws Exception {
		TahirNoorKhaliq newTahirNoorKhaliq = addTahirNoorKhaliq();

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(TahirNoorKhaliq.class,
				_dynamicQueryClassLoader);

		dynamicQuery.setProjection(ProjectionFactoryUtil.property("id"));

		Object newId = newTahirNoorKhaliq.getId();

		dynamicQuery.add(RestrictionsFactoryUtil.in("id", new Object[] { newId }));

		List<Object> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(1, result.size());

		Object existingId = result.get(0);

		Assert.assertEquals(existingId, newId);
	}

	@Test
	public void testDynamicQueryByProjectionMissing() throws Exception {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(TahirNoorKhaliq.class,
				_dynamicQueryClassLoader);

		dynamicQuery.setProjection(ProjectionFactoryUtil.property("id"));

		dynamicQuery.add(RestrictionsFactoryUtil.in("id",
				new Object[] { RandomTestUtil.nextLong() }));

		List<Object> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(0, result.size());
	}

	@Test
	public void testResetOriginalValues() throws Exception {
		TahirNoorKhaliq newTahirNoorKhaliq = addTahirNoorKhaliq();

		_persistence.clearCache();

		TahirNoorKhaliq existingTahirNoorKhaliq = _persistence.findByPrimaryKey(newTahirNoorKhaliq.getPrimaryKey());

		Assert.assertTrue(Objects.equals(existingTahirNoorKhaliq.getUuid(),
				ReflectionTestUtil.invoke(existingTahirNoorKhaliq,
					"getOriginalUuid", new Class<?>[0])));
		Assert.assertEquals(Long.valueOf(existingTahirNoorKhaliq.getGroupId()),
			ReflectionTestUtil.<Long>invoke(existingTahirNoorKhaliq,
				"getOriginalGroupId", new Class<?>[0]));
	}

	protected TahirNoorKhaliq addTahirNoorKhaliq() throws Exception {
		long pk = RandomTestUtil.nextLong();

		TahirNoorKhaliq tahirNoorKhaliq = _persistence.create(pk);

		tahirNoorKhaliq.setUuid(RandomTestUtil.randomString());

		tahirNoorKhaliq.setGroupId(RandomTestUtil.nextLong());

		tahirNoorKhaliq.setCompanyId(RandomTestUtil.nextLong());

		tahirNoorKhaliq.setUserId(RandomTestUtil.nextLong());

		tahirNoorKhaliq.setUserName(RandomTestUtil.randomString());

		tahirNoorKhaliq.setCreateDate(RandomTestUtil.nextDate());

		tahirNoorKhaliq.setModifiedDate(RandomTestUtil.nextDate());

		tahirNoorKhaliq.setName(RandomTestUtil.randomString());

		tahirNoorKhaliq.setDob(RandomTestUtil.nextDate());

		_tahirNoorKhaliqs.add(_persistence.update(tahirNoorKhaliq));

		return tahirNoorKhaliq;
	}

	private List<TahirNoorKhaliq> _tahirNoorKhaliqs = new ArrayList<TahirNoorKhaliq>();
	private TahirNoorKhaliqPersistence _persistence;
	private ClassLoader _dynamicQueryClassLoader;
}