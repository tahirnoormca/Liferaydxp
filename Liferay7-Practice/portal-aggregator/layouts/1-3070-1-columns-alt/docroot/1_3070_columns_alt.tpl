 
<div class="1_3070_1_columns_alt" id="main-content" role="main">
    <div class="portlet-layout row">
        <div id="column-1" class="portlet-column-only col-md-12 portlet-column">
		    $processor.processColumn("column-1", "portlet-column-content portlet-column-content-only")
        </div>
    </div>
    <div class="portlet-layout row">
        <div id="column-2" class="portlet-column-first col-md-2 portlet-column" style=" width: 296px !important;" >
		    $processor.processColumn("column-2", "portlet-column-content portlet-column-content-first")
        </div>
        <div id="column-3" class="portlet-column-last col-md-10 portlet-column" style="width: calc(100% - 296px);">
		    $processor.processColumn("column-3", "portlet-column-content portlet-column-content-last")
        </div>
    </div>
    <div class="portlet-layout row">
        <div id="column-4" class="portlet-column-only col-md-12 portlet-column">
		    $processor.processColumn("column-4", "portlet-column-content portlet-column-content-only")
        </div>
    </div>
</div>
